package com.cargill.util.test;

import org.joda.time.DateTime;
import org.joda.time.DateTimeUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.cargill.util.CodeUtil;
import com.cargill.util.UUIDUtil;

@RunWith(PowerMockRunner.class)
public class CodeUtilTest {
	
	@Before
	public void setUp() {
		PowerMockito.mockStatic(UUIDUtil.class);
		DateTime expectedDateTime = new DateTime(2019, 5, 27, 8, 27, 6, 380);
		DateTimeUtils.setCurrentMillisFixed(expectedDateTime.getMillis());
	}
	
	@After
	public void tearDown() {
		DateTimeUtils.setCurrentMillisSystem();
	}
	
	@Test
	@PrepareForTest({ UUIDUtil.class })
	public void testGenerateOrderNo() {
		String expectedUuid = "711f450f-e89b-12d3-a456-556642440000";
		PowerMockito.when(UUIDUtil.generateNewUUID()).thenReturn(expectedUuid);
		String expectedOrderNo = "2019052708270638_711f450f";
		String actualOrderNo = CodeUtil.generateOrderNo();
		Assert.assertEquals(expectedOrderNo, actualOrderNo);
	}
	
}
