package com.cargill.service;


import com.cargill.Constants;
import com.cargill.base.SpringTXBaseTest;
import com.cargill.dto.CustomerDto;
import com.cargill.dto.request.SearchCustomerRequest;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.web.PageableDefault;

import java.util.List;

public class CustomerServiceTest extends SpringTXBaseTest {

    @Autowired
    private CustomerService customerService;


    @Test
    public void findBySubIdTest(){

        String cusSubId = "54090018";
        List<CustomerDto>  cList = customerService.findBySubId(cusSubId);

        System.out.println(cList.size());
        for(CustomerDto cd :  cList){

            System.out.println(cd.getCustomerMainName());

        }

        Assert.assertTrue(cList.size()>0);
    }

    @Test
    public void findByConditionwWithoutCustomerMainIdTest(){


        SearchCustomerRequest sCustRequest = new SearchCustomerRequest();
        sCustRequest.setCustomerId("50320003");



        //SearchCustomerRequest condition, Pageable pageable

        Page<CustomerDto>  cService = customerService.findByConditionwWithoutCustomerMainId(sCustRequest,
               null );

        System.out.println("ss"+cService.getSize());


    }

    @Test
    public void findFeedingScaleBySubIdTest(){



    }



}
