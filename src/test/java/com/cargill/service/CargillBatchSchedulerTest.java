package com.cargill.service;

import java.io.UnsupportedEncodingException;
import java.time.LocalDate;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.json.JSONException;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import com.cargill.dto.A2CellDto;
import com.cargill.dto.A2Dto;
import com.cargill.dto.A2RowDto;
import com.cargill.enums.OwnerType;
import com.cargill.model.CustomerOrder;
import com.cargill.model.SchedulerNotification;
import com.cargill.model.parameter.order.OrderDetail;
import com.cargill.properties.BatchProperties;
import com.cargill.repository.CustomerOrderRepository;
import com.cargill.repository.SchedulerNotificationRepository;
import com.cargill.util.DateUtil;
import com.google.common.collect.Maps;

import groovy.util.logging.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@Slf4j
public class CargillBatchSchedulerTest {

	private String executeTestMethodName;
	private long start;

	@Rule
	public TestName testName = new TestName();
	@Autowired
	private CustomerOrderRepository repository;
	@Autowired
	private SchedulerNotificationRepository schedulerNotificationRepository;
	@Autowired
	private BatchProperties batchProperties;
	@Autowired
    private RestTemplate restTemplate;
	@Autowired
	private SchedulerManagerService schedulerManagerService;
	@Before
	public void setUp() {
		start = System.currentTimeMillis();
		System.out.println();
	}

	@After
	public void tearDown() {
		long end = System.currentTimeMillis();
		StringBuilder builder = new StringBuilder();
		builder.append("方法: ").append(executeTestMethodName);
		builder.append(", 總花費時間: ").append(String.valueOf(end - start));
		System.out.println(builder.toString());
	}

	@Test
	@Ignore
	public void testFindNextPurchaseDate() {
		executeTestMethodName = testName.getMethodName();
		Calendar start = Calendar.getInstance();
		start.set(2019, 0, 1, 0, 0, 0);
		Calendar end = Calendar.getInstance();
		end.set(2019, 0, 31, 23, 59, 59);
		long s, e;
		s = System.currentTimeMillis();
		List<CustomerOrder> totalDatas = this.repository.findByDeliveryDateBetween(start, end);
		e = System.currentTimeMillis();
		System.out.println("findByDeliveryDateBetween, Cost: " + (e - s));
		s = System.currentTimeMillis();
		List<A2Dto> pile = 
				totalDatas
				.stream().collect(Collectors.groupingBy(CustomerOrder::getCustomerSubId))
                .entrySet()
                .stream()
                .map(data -> {
                    A2Dto a2 = new A2Dto();
                    data.getValue().stream().findFirst().ifPresent(order -> {
                        a2.setCustomerSubId(order.getCustomerSubId());
                        a2.setCustomerSubName(order.getCustomerSubName());
                        a2.setTmcode(order.getTmcode());
                        a2.setDeliveryDate(order.getDeliveryDate());
                    });
                    data.getValue().stream()
                            .peek(order -> {
                                data.getValue().stream()
                                        .peek((orderForDetail) -> {
                                            int groupingDay = new DateTime(orderForDetail.getDeliveryDate()).dayOfMonth().get();
                                            orderForDetail.getOrderDetailList()
                                                    .stream()
                                                    .collect(Collectors.groupingBy(OrderDetail::getCode))
                                                    .entrySet()
                                                    .stream()
                                                    .peek((detail) -> {
                                                        A2RowDto currentRow = a2.getRowList().stream()
                                                        		.filter(it -> detail.getKey().equals(it.getCode()))
                                                                .findFirst().orElseGet(() -> {
                                                                    A2RowDto temp = new A2RowDto();
                                                                    IntStream.range(0, temp.getCellList().size())
                                                                            .mapToObj(i -> temp.getCellList().set(i, new A2CellDto("")))
                                                                            .collect(Collectors.toList());
                                                                    a2.getRowList().add(temp);
                                                                    return temp;
                                                                });
                                                        currentRow.setCode(detail.getKey());
                                                        currentRow.getCellList().set(groupingDay, new A2CellDto("O"));
                                                    }).collect(Collectors.toList());
                                        }).collect(Collectors.toList());
                            }).collect(Collectors.toList());
                    return a2;
                }).collect(Collectors.toList());
		List<SchedulerNotification> notifies = new ArrayList<>();
		pile.forEach(dto -> {
			dto.getRowList().forEach(row -> {
				if (row.getCellList().size() <= 2) {
					return ;
				}
				List<A2CellDto> cells = row.getCellList();
				List<Integer> days = IntStream.range(0, cells.size())
				.filter(idx -> StringUtils.isNotBlank(cells.get(idx).getValue()))
				.boxed()
				.sorted(Collections.reverseOrder())
				.collect(Collectors.toList());
				if (days.size() <= 2) { 
					return ;
				}
				Integer first = days.get(2);
				Integer second = days.get(1);
				Integer third = days.get(0);
				Integer firstBetween =  second - first;
				Integer secondBetween = third - second;
				if (firstBetween == 0 || secondBetween == 0 || (firstBetween != secondBetween)) {
					return;
				}
				LocalDate notificationDate = 
						YearMonth.of(start.get(Calendar.YEAR), start.get(Calendar.MONTH) + 1)
						.atDay(third).plusDays(secondBetween);
				SchedulerNotification sn = new SchedulerNotification();
				sn.setNotificationDate(DateUtil.convert(notificationDate).getTime());
				sn.setTmcode(dto.getTmcode());
				sn.setCustomerSubName(dto.getCustomerSubName());
				sn.setCustomerSubId(dto.getCustomerSubId());
				sn.setCode(row.getCode());
				sn.setMessage(StringUtils.EMPTY);
				sn.setOwner(OwnerType.SALES.getCode());
				notifies.add(sn);
			});
		});
		e = System.currentTimeMillis();
		System.out.println("data process , Cost: " + (e - s));
		Calendar preStart = Calendar.getInstance();
		preStart.set(2019, 0, 1, 0, 0, 0);
		Calendar preEnd = Calendar.getInstance();
		preEnd.set(2019, 0, 1, 0, 0, 0);
		preEnd.add(Calendar.MONTH, 1);
		preEnd.set(Calendar.DAY_OF_MONTH, preEnd.getActualMaximum(Calendar.DAY_OF_MONTH));
		List<SchedulerNotification> sns = 
				this.schedulerNotificationRepository.findByNotificationDateBetween(preStart.getTime(), preEnd.getTime());
		// 差集(欲新增扣除已存在的資料)
        List<SchedulerNotification> reduceNeedInsertResult = 
        		notifies.stream()
        		.filter(item -> !isContains(sns, item))
        		.collect(Collectors.toList());
		e = System.currentTimeMillis();
		System.out.println("exclude exist datas , Cost: " + (e - s));
		this.schedulerNotificationRepository.save(reduceNeedInsertResult);
	}
	
	private boolean isContains(List<SchedulerNotification> sns, SchedulerNotification sn) {
		if (CollectionUtils.isEmpty(sns) || sn == null) {
			return Boolean.FALSE;
		}
		return sns.stream().filter(s -> {
			return s.getNotificationDate().compareTo(sn.getNotificationDate()) == 0
					&& s.getTmcode().equals(sn.getTmcode())
					&& s.getCustomerSubId().equals(sn.getCustomerSubId());
		})
		.findFirst().isPresent();
	}

	@Test
	public void testNotifyApp() throws UnsupportedEncodingException, JSONException {
		executeTestMethodName = testName.getMethodName();
		System.out.println("=========================[開始]每日固定查詢推播對象主動推播=========================");
    	long start, end;
    	start = System.currentTimeMillis();
    	Calendar calendar = Calendar.getInstance();
		calendar.set(2019, 1, 3);
    	List<SchedulerNotification> sns = this.schedulerNotificationRepository
				.findByNotificationDateAndFinishedAndCancelled(calendar.getTime(), Boolean.FALSE, Boolean.FALSE);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		Boolean result = Boolean.TRUE;
		List<String> controlList = Arrays.asList("611");
		for (SchedulerNotification sn : sns) {
			if (controlList.contains(sn.getTmcode()) == false) {
				continue ;
			}
			Map<String, Object> map = Maps.newHashMap();
			map.put("userIds", Arrays.asList(sn.getTmcode()));
			map.put("title", batchProperties.getNotificationTitle());
			map.put("message", sn.getMessage());
			HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);
			ResponseEntity<Boolean> response = null;
			response = restTemplate.postForEntity(batchProperties.getNotificationUrl(), entity, Boolean.class);
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {}
			Boolean temp = response.getBody() == null? Boolean.FALSE : response.getBody();
			result &= temp;
		}
		if (result.booleanValue()) {
			sns.stream()
			.peek(s -> {
				s.setFinished(Boolean.TRUE);
			});
			this.schedulerNotificationRepository.save(sns);
		}
		end = System.currentTimeMillis();
		System.out.println("每日固定查詢推播對象主動推播, 花費時間: " + (end - start));
		System.out.println("=========================[結束]每日固定查詢推播對象主動推播=========================");
	}

	@Test
	public void testNotificationCustomer() throws UnsupportedEncodingException, JSONException {
		System.out.println("=========================[開始]每日固定查詢推播對象主動推播=========================");
		long start, end;
		start = System.currentTimeMillis();

		//取得資料
		List<SchedulerNotification> sns = this.schedulerManagerService.getNotifictions();

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		Boolean result = Boolean.TRUE;
		List<String> controlList = Arrays.asList(
				"610", "630", "611", "612", "613", "615", "616", "617", "618", "621", "622", "623", "631", "632", "633", "635", "636",
				"710", "720", "730", "750", "770", "780",
				"200", "211", "212", "213", "215", "216", "217"
		);

		for (SchedulerNotification sn : sns) {
			if (batchProperties.getControlList()
					&& controlList.contains(sn.getTmcode()) == false) {
				continue ;
			}


			Map<String, Object> map = Maps.newHashMap();
			map.put("userIds", Arrays.asList(sn.getTmcode()));
			map.put("title", batchProperties.getNotificationTitle());
			map.put("message", sn.getMessage());
			HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);
			ResponseEntity<Boolean> response = null;
			if (batchProperties.getDisablePush().booleanValue()) {
				response = new ResponseEntity<Boolean>(HttpStatus.OK);
				result = Boolean.FALSE;
			}
			else {
				response = restTemplate.postForEntity(batchProperties.getNotificationUrl(), entity, Boolean.class);
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {}
			}
			Boolean temp = response.getBody() == null? Boolean.FALSE : response.getBody();
			result &= temp;
		}
		if (result.booleanValue()) {
			sns.stream()
					.forEach(s -> {
						s.setFinished(Boolean.TRUE);
					});
			this.schedulerNotificationRepository.save(sns);
		}
		end = System.currentTimeMillis();
		System.out.println("每日固定查詢推播對象主動推播, 花費時間: " + (end - start));
		System.out.println("=========================[結束]每日固定查詢推播對象主動推播=========================");
	}

	@Test
	public void init() {}
	
}
