package com.cargill.service;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import groovy.util.logging.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@ActiveProfiles({"test"})
@Slf4j
public class SmsServiceTest {

    @Autowired
    private SmsService smsService;

    @Before
    public void setUp() {

    }

    @Test
    @Ignore
    public void shouldAbleToSendSms() {
        String messageId = smsService.sendSms("0958668947", "hello world");
        assertNotNull(messageId);
    }
}
