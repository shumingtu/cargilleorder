package com.cargill.test;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.cargill.Constants;
import com.cargill.util.DateUtil;
import com.google.common.base.Splitter;

public class SimpleTest {

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}

	@Test
	@Ignore
	public void testUUID() {
		Splitter splitter = Splitter.on("-");
		System.out.println(splitter.splitToList(UUID.randomUUID().toString()).get(0));
	}

	@Test
	@Ignore
	public void testList() {
		List<String> demo = Stream.of("a", "b", "c", "d").collect(Collectors.toList());
		IntStream.range(0, demo.size()).filter(i -> i == (Integer.parseInt("3") - 1)).mapToObj(demo::get).findFirst()
				.ifPresent(System.out::println);
	}

	@Test
	public void testDate() {
//		System.out.println(LocalDate.now().with(TemporalAdjusters.firstDayOfMonth()).atTime(LocalTime.MIN));
//		System.out.println(LocalDate.now().with(TemporalAdjusters.lastDayOfMonth()).atTime(LocalTime.MAX));
		Calendar c = Calendar.getInstance();
		c.set(2019, 11, 31);
		Date d = c.getTime();
		System.out.println(d.toString());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		System.out.println(Constants.SDF_DATE_ONLY.format(d));
		System.out.println(sdf.format(d));
	}

	@Test
	@Ignore
	public void testBetweenDate() {
		LocalDate today = LocalDate.now();
		LocalDate yesterday = today.minusDays(1);
		System.out.println(Duration.between(yesterday.atStartOfDay(), today.atStartOfDay()).toDays());
	}

	@Test
	@Ignore
	public void testEnumValueOf() {
//		System.out.println(OrderTypeCode.valueOf("APP"));
		System.out.println(DateUtil.convert(LocalDate.now()).getTime());
	}
	
	@Test
	@Ignore
	public void testMap() {
		Map<String, String> map = new HashMap<>();
		map.put("test", "testing");
		System.out.println(map.getOrDefault("a", ""));
	}
	
}
