package com.cargill.base;


import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@WebAppConfiguration
public abstract class SpringTXBaseTest {



    @Before
    public void setUp(){

        System.out.println("test start!");
    }

    @After
    public void dearDown(){

        System.out.println("test end!");
    }

}
