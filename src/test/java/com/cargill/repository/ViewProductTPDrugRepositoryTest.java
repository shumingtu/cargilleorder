package com.cargill.repository;

import com.cargill.base.SpringTXBaseTest;
import com.cargill.model.AppUser;
import com.cargill.model.parameter.TPAreaType;
import com.cargill.model.parameter.TPDrugType;
import com.cargill.model.view.ViewProductTPDrug;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigInteger;
import java.util.Collections;
import java.util.List;

public class ViewProductTPDrugRepositoryTest extends SpringTXBaseTest {



    @Autowired
    private ViewProductTPDrugRepository viewProductTPDrugRepository;


    @Test
    public void findByCodeAndDrugTypeTest(){

        String  code = "11852";
        ViewProductTPDrug drug = viewProductTPDrugRepository.findByCodeAndDrugTypeAndAreaType(
                code, TPDrugType.COMPANY, TPAreaType.KS
        );

        System.out.println("drug"+drug.getCode());
//        Assert.assertTrue(  drug.);
    }



    @Test
    public void findByCodeStartingWithAndDrugTypeTest(){

        List<ViewProductTPDrug> drugList = Collections.emptyList();
        String  code = "11852";
        drugList = viewProductTPDrugRepository.findByCodeStartingWithAndDrugType(
                code, TPDrugType.COMPANY
        );


        for( ViewProductTPDrug  viewProductTPDrug : drugList){

            System.out.println(viewProductTPDrug.getCode()+ viewProductTPDrug.getName());

        }



    }




}
