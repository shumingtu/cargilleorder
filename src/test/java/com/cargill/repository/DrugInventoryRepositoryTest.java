package com.cargill.repository;

import com.cargill.base.SpringTXBaseTest;

import com.cargill.model.DrugInventory;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;


public class DrugInventoryRepositoryTest extends SpringTXBaseTest {


    @Autowired
    private DrugInventoryRepository drugInventoryRepository;

    @Test
    public void findByCustomerSubIdAndCodeAndStatusTest(){

        String customerSubId = "70700073333";
        String code = "20005";
        String status ="2";
        DrugInventory drugInventory =  drugInventoryRepository
                .findByCustomerSubIdAndCodeAndStatus(customerSubId,code, status);

        System.out.println(drugInventory);

        if(null == drugInventory){
            System.out.println("test");
        }


        Assert.assertTrue( drugInventory.getCode().equals(code) );
    }




    @Test
    public void updateCreateDateByStatusOneTest(){
        drugInventoryRepository.updateCreateDateByStatusOne();
    }



}
