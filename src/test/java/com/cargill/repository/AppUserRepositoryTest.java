package com.cargill.repository;


import com.cargill.base.SpringTXBaseTest;
import com.cargill.model.AppUser;
import groovy.util.logging.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.math.BigInteger;


public class AppUserRepositoryTest extends SpringTXBaseTest {

    @Autowired
    private AppUserRepository appUserRepository;


    @Test
    public void appUserRepositoryTest(){
        AppUser apUser = appUserRepository.findById(new BigInteger("62"));
        System.out.println("sss"+apUser.getUserName());

        Assert.assertTrue(  apUser.getUserName().equals("610Even"));
    }





}
