function resetDateTimePickers() {
    // 特配訂單日曆
    $("#spSpDeliveryDate").flatpickr({
        defaultDate: 'today',
    });
    // 特配訂單時間
    $("#spSpDeliveryTime").flatpickr({
        enableTime: true,
        noCalendar: true,
        dateFormat: "H:i",
        time_24hr: true,
        defaultDate: new Date().getHours()+1 + ':00'
    });
}

function emptyDetailsTP(type) {
    var eType = ('company' === type) ? 'TPC' : 'TPS';
    $('#searchProductCode' + eType).val('');
    $('#searchProductName' + eType).val('');
    $('#selectProductAmount' + eType).val('');
    $('#amount2kg' + eType).text('');
    $('#recommendedAmount' + eType).text('');
    $('#maxAmount' + eType).text('');
    $('#drugWithdrawal' + eType).text('');
    $('#note' + eType).text('');
    $('#totalAmountTPC').text('');
    $('#totalAmountTPS').text('');
}

// 顯示特配小單 from 原本未填特配
function showDialogSP_patched_fapp(td) {
    var spValue = $(td).val();
    if(spValue && '' !== spValue && '特配單' !== spValue) {
        $(td).closest('td').find('span').text(spValue);
        var spDetail = JSON.parse($(td).closest('tr').find('input[name=spDetail]').val());
        spDetail.value = spValue;
        $(td).closest('tr').find('input[name=spDetail]').val(JSON.stringify(spDetail));
    }
}

// 顯示特配小單
function showDialogSP(row, descApp, rowid) {
    var spDetail = JSON.parse($(row).closest('tr').find('input[name=spDetail]').val());
    if(!spDetail.value || '特配單' === spDetail.value) {
        return;
    }

    $('.A7').addClass('show-dialog');
    $('body').addClass('sw_active');
    $('.dialog-modal.subDialog .tableContent .content').scrollTop('0');

    resetDateTimePickers();

    emptyDetailsTP('company');
    emptyDetailsTP('self');
    $('input[name=symptom]').prop('checked', false);

    editIndex = $(row).closest('tr').index();

    $("#spDeliveryDate").text($("#deliveryDate").val());
    $("#spDeliveryTime").text($("#deliveryTime").val());

    $('#spValue').text($(row).text());
    $('#spCode').text($(row).closest('tr').find('td:eq(0)').text());
    $('#spName').text($(row).closest('tr').find('td:eq(1)').text());
    $('#spPhase').text($(row).closest('tr').find('input[name=phase]').val());

    $('#vehicleType').text($(row).closest('tr').find('select[name="vehicle"]  :selected').text());

    $('#spPackage').text($(row).closest('tr').find('td:eq(2)').text());
    $('#spPackageValue').val($(row).closest('tr').find('input[name=package]').val());
//    $('#spPackagePrice').val($(row).closest('tr').find('input[name=packagePrice]').val());
//    $('#spPackageValue').val($().val());
    $('#spPackagePrice').val($("#selectPackagePrice"+rowid).val());


    // <20201102> FIXME 取得噸數/包數數值異常 START
//    $('#spAmount').text($(row).closest('tr').find('td:eq(3)').text());
    var spAmt = $(row).closest('tr').find('td:eq(3) > input[name=count]').val();
    $('#spAmount').text(spAmt);
    console.log("SET 噸數/包數: " + spAmt);
    // <20201102> FIXME 取得噸數/包數數值異常 END

    if(spDetail.id) {
        $('#spDetailNo').html('特配單編號：SP' + spDetail.id + '<span>建單日期　' + spDetail.createDate + '</span>');
    } else {
        $('#spDetailNo').html('特配單編號：<span>建單日期　</span>');
    }
    if(spDetail.id) {
        $('#btnPrintPageSP').show();
    } else {
        $('#btnPrintPageSP').hide();
    }

    $('#spValue').text(spDetail.value);

    if(spDetail.symptom) {
        spDetail.symptom.map(function (s) {
            $('#symptom'+s).prop('checked', true);
        });
    }

    if(spDetail.deliveryDate) {
        if(spDetail.deliveryDateDisplay && spDetail.deliveryTimeDisplay) {
            $('#spSpDeliveryDate').val(spDetail.deliveryDateDisplay);
            $('#spSpDeliveryTime').val(spDetail.deliveryTimeDisplay);
        } else {
            var temp = spDetail.deliveryDate.split(' ');
            $('#spSpDeliveryDate').val(temp[0]);
            $('#spSpDeliveryTime').val(temp[1]);
        }
    }

    $('#productSpec').val(spDetail.productSpec);
    
    var selectProductSpec = $('#selectProductSpec'+rowid).val();
    
    console.log("生產規格 : "+selectProductSpec);
    
    if($('#productSpec').val() === ''){
    	console.log("TP 包裝規格註記 NULL");
    	
    	var fproductSpec = productSpecMapping(selectProductSpec);
    	
    	console.log("SET 包裝規格註記  : "+fproductSpec);
    	
    	$('#productSpec').val(fproductSpec);
    	
    	console.log("設定完成  : "+$('#productSpec').val());
    	
    }
    
    $('#productLineNote').val(spDetail.productLineNote);

    // <20190821> FIXED: 調整為外部主檔備註帶入
//    $('#productNote').val(spDetail.productNote);
    $('#productNote').val(descApp);
    $('#noteA').val(spDetail.noteA);
    $('#noteB').val(spDetail.noteB);
    $('#noteC').val(spDetail.noteC);

    $('#totalPriceTP').text(spDetail.totalPrice ? spDetail.totalPrice : '0');
    $('#codeWeightTP').text(spDetail.codeWeight ? spDetail.codeWeight : '0');
    $('#spPriceTP').text(spDetail.spPrice ? spDetail.spPrice : '0');

    generateDrugsTP(spDetail);

    queryCucy($(row).closest('tr').find('td:eq(0)').text());

    // 修改紀錄
    if(spDetail.modifyLogs) {
        var li = '';
        spDetail.modifyLogs.map(function(log) {
            li += '<li>' +  log + '</li>';
        });
        $('#spModifyLogs').append('<ul>' + li + '</ul>');
    }

    updateTotalPriceTP();
}

//生產規格to特配單包裝規格註記
function productSpecMapping(productSpec){
	//default
	var fproductSpec = 'PL';
	
	switch(productSpec) {
    
	    case 'PL'://粒料"PL"
	    	fproductSpec = '1';
           break;
        case 'ML':
        	fproductSpec = '2';//粉料"ML"
           break;
        case 'CR'://碎料"CR"
        	fproductSpec = '0';
            break;
     } 
	
	return fproductSpec;
}

function closeDialogSP(do_save) {
    $('.A7').removeClass('show-dialog');
    $('body').removeClass('sw_active');

    if(do_save && 0 < editIndex) {
        var drugCompanyList = [];
        var drugSelfList = [];
        $('#orderDetailTPC tr:gt(2)').each(function() {
            drugCompanyList.push({
                code:  $(this).find('td:eq(0)').text().trim(),
                name:  $(this).find('td:eq(1)').text().trim(),
                count1kg:  $(this).find('td:eq(2)').text().trim(),
                count2kg: $(this).find('td:eq(3)').text().trim(),
                recommend:  $(this).find('td:eq(4)').text().trim(),
                totalCount:$(this).find('td:eq(5)').text().trim(),
                days:$(this).find('td:eq(6)').text().trim(),
                note:$(this).find('td:eq(7)').text().trim(),
                price:$(this).find('td:eq(7) input[name=priceTPC]').val().trim()*1,
            });
        });
        $('#orderDetailTPS tr:gt(2)').each(function() {
            drugSelfList.push({
                code:  $(this).find('td:eq(0)').text().trim(),
                name:  $(this).find('td:eq(1)').text().trim(),
                count1kg:  $(this).find('td:eq(2)').text().trim(),
                count2kg: $(this).find('td:eq(3)').text().trim(),
                recommend:  $(this).find('td:eq(4)').text().trim(),
                totalCount:$(this).find('td:eq(5)').text().trim(),
                days:$(this).find('td:eq(6)').text().trim(),
                note:$(this).find('td:eq(7)').text().trim(),
                price:$(this).find('td:eq(7) input[name=priceTPS]').val().trim()*1,
            });
        });

        var spDetail = JSON.parse($('#orderDetail tr:eq('+editIndex+') input[name=spDetail]').val());
        spDetail.value = $('#spValue').text();
        spDetail.symptom = $.map($('input[name=symptom]:checked'), function(c){return c.value; });
        spDetail.deliveryDate = $('#spSpDeliveryDate').val() + ' ' + $('#spSpDeliveryTime').val(),
        spDetail.productSpec = $('#productSpec').val();
        spDetail.productLineNote = $('#productLineNote').val();
        spDetail.productNote = $('#productNote').val();
        spDetail.noteA = $('#noteA').val();
        spDetail.noteB = $('#noteB').val();
        spDetail.noteC = $('#noteC').val();
        spDetail.drugCompanyList = drugCompanyList;
        spDetail.drugSelfList = drugSelfList;
        spDetail.update = true;
        spDetail.totalPrice = $('#totalPriceTP').text().trim();
        spDetail.codeWeight = $('#codeWeightTP').text().trim();
        spDetail.spPrice = $('#spPriceTP').text().trim();
        // <20190821> FIXED: 調整特配單備註需與主檔備註同步
        try {
        	updateDescApp(spDetail.productNote);
        } catch (e) {}

        $('#orderDetail tr:eq('+editIndex+') input[name=spDetail]').val(JSON.stringify(spDetail));
    }
}

// 計算 TPC 每1噸用量(KG)
function calculateWeightTPC() {
    var spAmount = new Decimal($('#spAmount').text().trim()*1);
    var amount = new Decimal($('#selectProductAmountTPC').val()*1);
    var price = new Decimal($('#priceTPC').val()*1);
    $('#amount2kgTPC').text(amount.mul(new Decimal(2)).toFixed());
    $('#totalAmountTPC').text(spAmount.mul(amount).mul(getCodeWeight($('#spPackageValue').val())).toFixed());
}

// 計算 TPS 每1噸用量(KG)
function calculateWeightTPS() {
    var spAmount = new Decimal($('#spAmount').text().trim()*1);
    var amount = new Decimal($('#selectProductAmountTPS').val()*1);
    var price = new Decimal($('#priceTPS').val()*1);
    $('#amount2kgTPS').text(amount.mul(new Decimal(2)).toFixed());
    $('#totalAmountTPS').text(spAmount.mul(amount).mul(getCodeWeight($('#spPackageValue').val())).toFixed());
}

// 查詢常用抽藥
function queryCucy(code) {
    $('#spCUCY').find('tr:gt(0)').remove();
    axios.post('/api/product/cucy?code='+code).then(function (response) {

        if(response.data && 200 === response.status) {
            $('#spCUCY').find('tr:gt(0)').remove();

            response.data.map(function (s, idx) {
                var sub = '';
                if(0 === idx) {
                    sub = '<td>' + s.code + '</td><td>' + s.name + '</td><td>' + s.drugCode + '</td><td>' + s.drugName + '</td><td>' + s.amount + '</td>';
                } else {
                    sub = '<td></td><td></td><td>' + s.drugCode + '</td><td>' + s.drugName + '</td><td>' + s.amount + '</td>';
                }

                $('#spCUCY').append('<tr>' + sub + '</tr>');
            });
        }
    });
}

// 更新特配 散裝/包裝資訊
function updateSelectedValueTPC(suggestion) {
    $('#searchProductCodeTPC').val(suggestion.data.code);
    $('#searchProductNameTPC').val(suggestion.data.name);

    $('#selectProductAmountTPC').val(1);
    $('#amount2kgTPC').text(2);
    $('#recommendedAmountTPC').text(suggestion.data.recommendedAmount + '/' + suggestion.data.maxAmount);
    $('#totalAmountTPC').text($('#spAmount').text().trim()*1);
    $('#drugWithdrawalTPC').text(suggestion.data.drugWithdrawal);
    $('#noteTPC').text(suggestion.data.note);
    $('#priceTPC').val(suggestion.data.price);

    calculateWeightTPC();
}

// 更新特配 TPS 散裝/包裝資訊
function updateSelectedValueTPS(suggestion) {
    $('#searchProductCodeTPS').val(suggestion.data.code);
    $('#searchProductNameTPS').val(suggestion.data.name);

    $('#selectProductAmountTPS').val(1);
    $('#amount2kgTPS').text(2);
    $('#recommendedAmountTPS').text(suggestion.data.recommendedAmount + '/' + suggestion.data.maxAmount);
    $('#totalAmountTPS').text($('#spAmount').text().trim()*1);
    $('#drugWithdrawalTPS').text(suggestion.data.drugWithdrawal);
    $('#noteTPS').text(suggestion.data.note);
    $('#priceTPS').val(suggestion.data.price);

    calculateWeightTPS();
}

/**
 * 重算特配單
 * @param detail 特配單
 * @returns
 */
function recalculateDrugsTP(detail) {
	// 飼料出貨量
	var spAmount = new Decimal($('#spAmount').text().trim()*1);
    AppDebugger.info('[重算特配單] 飼料出貨量(spAmount)', spAmount.toString());
    // 散裝/包裝重量
    var weight = getCodeWeight($('#spPackageValue').val());
    AppDebugger.info('[重算特配單] 散裝/包裝重量(spPackageValue)', weight.toString());
	// 重算計算公司藥
	if(detail.drugCompanyList) {
        detail.drugCompanyList.map(function(drug, idx) {
        	AppDebugger.info('[重算特配單-公司藥] ===== 第' + (idx + 1) + '筆 =====');
        	AppDebugger.info('[重算特配單-公司藥] 特配每1噸用量(KG)', drug.count1kg);
        	// 總量
        	drug.totalCount = spAmount.mul(drug.count1kg).mul(weight).toFixed()
        });
    }
	// 重新計算自備藥
    if(detail.drugSelfList) {
        detail.drugSelfList.map(function(drug, idx) {
        	AppDebugger.info('[重算特配單-自備藥] ===== 第' + (idx + 1) + '筆 =====');
        	AppDebugger.info('[重算特配單-自備藥] 特配每1噸用量(KG)', drug.count1kg);
        	// 總量
        	drug.totalCount = spAmount.mul(drug.count1kg).mul(weight).toFixed()
        });
    }
}

function generateDrugsTP(detail) {
    $('#orderDetailTPC tr:gt(2)').remove();
    $('#orderDetailTPS tr:gt(2)').remove();
    
    /* <20210714> Mike: 修改特配單應重算 START */
    recalculateDrugsTP(detail);
    /* <20210714> Mike: 修改特配單應重算 END */

    if(detail.drugCompanyList) {
        detail.drugCompanyList.map(function(drug) {
            var newDetail = '';
            newDetail += '<td>' +  drug.code + '</td>';
            newDetail += '<td>' + drug.name + '</td>';
            newDetail += '<td>' +  drug.count1kg + '</td>';
            newDetail += '<td>' +  drug.count2kg + '</td>';
            newDetail += '<td>' +  drug.recommend + '</td>';
            newDetail += '<td>' +  drug.totalCount + '</td>';
            newDetail += '<td>' +  drug.days + '</td>';
            newDetail += '<td>' +  drug.note + '<input type="hidden" name="priceTPC" value="' + drug.price + '" /></td>';

            newDetail += '<td><img onclick="removeDetailTP(this)" src="../img/ico_delete.png"></td>';
            $('#orderDetailTPC').append('<tr>' + newDetail + '</tr>');
        });
    }
    if(detail.drugSelfList) {
        detail.drugSelfList.map(function(drug) {
            var newDetail = '';
            newDetail += '<td>' +  drug.code + '</td>';
            newDetail += '<td>' + drug.name + '</td>';
            newDetail += '<td>' +  drug.count1kg + '</td>';
            newDetail += '<td>' +  drug.count2kg + '</td>';
            newDetail += '<td>' +  drug.recommend + '</td>';
            newDetail += '<td>' +  drug.totalCount + '</td>';
            newDetail += '<td>' +  drug.days + '</td>';
            newDetail += '<td>' +  drug.note + '<input type="hidden" name="priceTPS" value="' + drug.price + '" /></td>';

            newDetail += '<td><img onclick="removeDetailTP(this)" src="../img/ico_delete.png"></td>';
            $('#orderDetailTPS').append('<tr>' + newDetail + '</tr>');
        });
    }

    detail.totalPrice = 0;
    detail.codeWeight = 0;
    detail.spPrice = 0;

    return detail;
}

function getCodeWeight(pkg_code) {
	try {
		AppDebugger.info("===== [計算散裝/包裝重量] 開始 =====");
		var result = new Decimal('1');
		pkgCode = pkg_code;
		if(1 < pkg_code.split('/').length) {
			pkgCode = pkg_code.split('/')[1];
		}
		AppDebugger.info("[計算散裝/包裝重量]代碼", pkgCode);
		switch(pkgCode) {
		case "1":
			result = new Decimal('0.01');
			break;
		case "2":
			result = new Decimal('0.02');
			break;
		case "3K":
			result = new Decimal('0.003');
			break;
		case "2.5":
			result = new Decimal('0.025');
			break;
		case "3":
			result = new Decimal('0.03');
			break;
		case "3C":
			result = new Decimal('0.03');
			break;
		case "5":
			result = new Decimal('0.05');
			break;
		case "J":
			result = new Decimal('1');
			break;
		case "J5":
			result = new Decimal('0.5');
			break;
		}
		AppDebugger.info("[計算散裝/包裝重量]重量係數", result.toString());
		return result;
	} finally {
		AppDebugger.info("===== [計算散裝/包裝重量] 結束 =====");
	}
}

// 更新特配總價
function updateTotalPriceTP() {
	AppDebugger.info("[特配總價] 開始更新");
    // 特配總價
    var totalPriceTP = new Decimal(0);
    // 飼料扣藥重
    var codeWeightTP = new Decimal(0);
    // 特配金額
    var spPriceTP = new Decimal(0);

    // 飼料出貨量
    var oAmount = new Decimal($('#spAmount').text().trim()*1);
    AppDebugger.info('[特配總量] 飼料出貨量[噸數/包數](spAmount)', oAmount.toString());
    // 飼料價格
    var oPrice = new Decimal($('#spPackagePrice').val()*1);
    AppDebugger.info('[特配總量] 飼料價格(spPackagePrice)', oPrice.toString());
    // 散裝/包裝重量
    var weight = getCodeWeight($('#spPackageValue').val());
    AppDebugger.info('[特配總量] 散裝/包裝重量(spPackageValue)', weight.toString());

    AppDebugger.info('[特配總量] 開始計算公司藥');
    $('#orderDetailTPC tr:gt(2)').each(function(idx) {
    	AppDebugger.info('[特配總量-公司藥] ===== 第' + (idx + 1) + '筆 =====');
        // 每個特配的量
        var p1 = new Decimal($(this).find('td:eq(2)').text().trim()*1);
        AppDebugger.info('[特配總量-公司藥] 每個特配的量', p1.toString());
        // 每個特配的價格
        var p2 = new Decimal($(this).find('input[name=priceTPC]').val()*1);
        AppDebugger.info('[特配總量-公司藥] 每個特配的價格', p2.toString());
        // 總量
        var p3 = new Decimal($(this).find('td:eq(5)').text().trim()*1);
        AppDebugger.info('[特配總量-公司藥] 總量', p3.toString());

        // 特配總價 = 特配每公斤3元*特佩4公斤
        totalPriceTP = totalPriceTP.plus( p2.mul(p3) );
        AppDebugger.info('[特配總量-公司藥] 特配總價(每個特配的價格 * 總量)', totalPriceTP.toString());
        // 飼料扣藥重 = 飼料每公斤4元*特佩4公斤
        codeWeightTP = codeWeightTP.plus( oPrice.mul(p3) );
        AppDebugger.info('[特配總量-公司藥] 飼料扣藥重(飼料價格 * 總量)', codeWeightTP.toString());
    });
    AppDebugger.info('[特配總量] 結束計算公司藥');

    AppDebugger.info('[特配總量] 開始計算自備藥');
    $('#orderDetailTPS tr:gt(2)').each(function(idx) {
    	AppDebugger.info('[特配總量-自備藥] ===== 第' + (idx + 1) + '筆 =====');
        // 每個特配的量
        var p1 = new Decimal($(this).find('td:eq(2)').text().trim()*1);
        AppDebugger.info('[特配總量-自備藥] 每個特配的量', p1.toString());
        // 每個特配的價格
        var p2 = new Decimal($(this).find('input[name=priceTPS]').val()*1);
        AppDebugger.info('[特配總量-自備藥] 每個特配的價格', p2.toString());
        // 總量
        var p3 = new Decimal($(this).find('td:eq(5)').text().trim()*1);
        AppDebugger.info('[特配總量-自備藥] 總量', p3.toString());

       // 特配總價 = 特配每公斤3元*特佩4公斤
       totalPriceTP = totalPriceTP.plus( p2.mul(p3) );
       AppDebugger.info('[特配總量-自備藥] 特配總價(每個特配的價格 * 總量)', totalPriceTP.toString());
       // 飼料扣藥重 = 飼料每公斤4元*特佩4公斤
       codeWeightTP = codeWeightTP.plus( oPrice.mul(p3) );
       AppDebugger.info('[特配總量-自備藥] 飼料扣藥重(飼料價格 * 總量)', codeWeightTP.toString());
    });
    AppDebugger.info('[特配總量] 結束計算自備藥');

    AppDebugger.info('[特配總量] 總計特配總價(公司藥特配總價 + 自備藥特配總價)', totalPriceTP.toString());
    $('#totalPriceTP').text(totalPriceTP ? Math.round(totalPriceTP.toFixed()) : '0');
    AppDebugger.info('[特配總量] 總計飼料扣藥重', codeWeightTP.toString());
    $('#codeWeightTP').text(codeWeightTP ? Math.round(codeWeightTP.toFixed()) : '0');

     // 特配金額 = -(飼料扣藥重) + 特配總價
    spPriceTP = new Decimal($('#totalPriceTP').text()*1).sub(new Decimal($('#codeWeightTP').text()*1));
    AppDebugger.info('[特配總量] 特配金額 = -(飼料扣藥重) + 特配總價', spPriceTP.toString());
    $('#spPriceTP').text(spPriceTP ? Math.round(spPriceTP.toFixed()) : '0');
    
    AppDebugger.info("[特配總價] 結束更新");
}

// 刪除一筆明細列
function removeDetailTP(row) {
    $(row).closest('tr').remove();
    updateTotalPriceTP();
}

// 新增一筆明細列
function addNewDetailTP(type) {
    var eType = ('company' === type) ? 'TPC' : 'TPS';
    if('' === $('#searchProductCode' + eType).val() || '' === $('#searchProductName' + eType).val() || '' ===  $('#selectProductAmount' + eType).val() ) {
        return;
    }

    var newDetail = '';
    newDetail += '<td>' +  $('#searchProductCode' + eType).val() + '</td>';
    newDetail += '<td>' +  $('#searchProductName' + eType).val() + '</td>';
    newDetail += '<td>' +  $('#selectProductAmount' + eType).val() + '</td>';
    newDetail += '<td>' +  $('#amount2kg' + eType).text() + '</td>';
    newDetail += '<td>' +  $('#recommendedAmount' + eType).text() + '</td>';
    newDetail += '<td>' +  $('#totalAmount' + eType).text() + '</td>';
    newDetail += '<td>' +  $('#drugWithdrawal' + eType).text() + '</td>';
    newDetail += '<td>' +  $('#note' + eType).text() + '<input type="hidden" name="price' + eType+ '" value="' + $('#price' + eType).val() + '" /></td>';

    newDetail += '<td><img onclick="removeDetailTP(this)" src="../img/ico_delete.png"></td>';

    $('#orderDetail' + eType).append('<tr>' + newDetail + '</tr>');

    emptyDetailsTP(type);
    updateTotalPriceTP();
}

// 查詢 auto complete
$('#searchProductCodeTPC').autocomplete({
    serviceUrl: '/api/product/query/drugs/company',
    paramName: 'code',
    dataType: 'json',
    width: 'flex',
    noCache: true,
    zIndex: 99999,
    transformResult: function(response) {
        return {
            suggestions: $.map(response, function(data) {
                return { value: data.code, data: data };
            })
        };
    },
    formatResult: function (suggestion, currentValue) {
        return $.Autocomplete.defaults.formatResult(suggestion, currentValue) + ' ' + suggestion.data.name;
    },
    onSearchStart: function (params) {
        var sp = $('#spValue').text();
        params.name = '';
        params.areaCode = sp.match('[1-9].')[0];
        params.areaType = ('台中' == sp.substring(0,2)) ? 'TC' : 'KS';
    },
    onSelect: updateSelectedValueTPC
});
$('#searchProductNameTPC').autocomplete({
    serviceUrl: '/api/product/query/drugs/company',
    paramName: 'name',
    dataType: 'json',
    width: 'flex',
    noCache: true,
    zIndex: 99999,
    transformResult: function(response) {
        return {
            suggestions: $.map(response, function(data) {
                return { value: data.name, data: data };
            })
        };
    },
    formatResult: function (suggestion, currentValue) {
        return suggestion.data.code + ' ' + $.Autocomplete.defaults.formatResult(suggestion, currentValue);
    },
    onSearchStart: function (params) {
        params.code = '';
        var sp = $('#spValue').text();
        params.areaCode = sp.match('[1-9].')[0];
        params.areaType = ('台中' == sp.substring(0,2)) ? 'TC' : 'KS';
    },
    onSelect: updateSelectedValueTPC
});

// 查詢 auto complete
$('#searchProductCodeTPS').autocomplete({
    serviceUrl: '/api/product/query/drugs/self',
    paramName: 'code',
    dataType: 'json',
    width: 'flex',
    noCache: true,
    zIndex: 99999,
    transformResult: function(response) {
        return {
            suggestions: $.map(response, function(data) {
                return { value: data.code, data: data };
            })
        };
    },
    formatResult: function (suggestion, currentValue) {
        return $.Autocomplete.defaults.formatResult(suggestion, currentValue) + ' ' + suggestion.data.name;
    },
    onSearchStart: function (params) {
        params.name = '';
        var sp = $('#spValue').text();
        params.areaCode = sp.match('[1-9].')[0];
        params.areaType = ('台中' == sp.substring(0,2)) ? 'TC' : 'KS';
    },
    onSelect: updateSelectedValueTPS
});
$('#searchProductNameTPS').autocomplete({
    serviceUrl: '/api/product/query/drugs/self',
    paramName: 'name',
    dataType: 'json',
    width: 'flex',
    noCache: true,
    zIndex: 99999,
    transformResult: function(response) {
        return {
            suggestions: $.map(response, function(data) {
                return { value: data.name, data: data };
            })
        };
    },
    formatResult: function (suggestion, currentValue) {
        return suggestion.data.code + ' ' + $.Autocomplete.defaults.formatResult(suggestion, currentValue);
    },
    onSearchStart: function (params) {
        params.code = '';
        var sp = $('#spValue').text();
        params.areaCode = sp.match('[1-9].')[0];
        params.areaType = ('台中' == sp.substring(0,2)) ? 'TC' : 'KS';
    },
    onSelect: updateSelectedValueTPS
});
