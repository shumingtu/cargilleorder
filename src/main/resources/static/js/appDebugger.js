(function(w) {
	
	var appDebugger = {
		type: {
			INFO: 'INFO',
			ERROR: 'ERROR'
		},
		print : function(type, msg) {
			switch (type) {
			case 'INFO':
				console.log('%c INFO: ', 'background: green; color: white;', msg);
				break;
			case 'ERROR':
				console.log('%c ERROR: ', 'background: red; color: white;', msg);
				
			}
		},
		info : function(msg, data) {
			this.print(this.type.INFO, msg);
			if (arguments.length > 1) {
				console.log(data);
			}
		},
		error: function(msg, data) {
			this.print(this.type.ERROR, msg);
			if (arguments.length > 1) {
				console.log(data);
			}
		}
	};

	w.AppDebugger = appDebugger;

})(window);