function initPage() {
    // 初始化設定
}

// 頁面連結
function link(url, tabNum) {
    if(!tabNum){
        return location.assign(url);
    }else{
        return location.assign(url+"?tab="+tabNum);
	}
}

// 頁面另開連結
function open_blank(url) {
        window.open(url, '_blank'); 
}

// 防止冒泡事件
function stopPropagation() {
    event.stopPropagation();
}

// 特配單/工廠小單 dialog顯示控制
function showDialog(name) {
	$('.'+name).addClass('show-dialog');
	$('body').addClass('sw_active');
}
// 特配單/工廠小單 dialog隱藏控制
function closeDialog(name) {
	$('.'+name).removeClass('show-dialog');
	$('body').removeClass('sw_active');
}
