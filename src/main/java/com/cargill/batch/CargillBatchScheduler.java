package com.cargill.batch;

import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import com.cargill.dto.CustomerDto;
import com.cargill.service.SchedulerManagerService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import com.cargill.dto.BatchDto;
import com.cargill.dto.BatchRowDto;
import com.cargill.enums.OwnerType;
import com.cargill.model.Customer;
import com.cargill.model.CustomerOrder;
import com.cargill.model.SchedulerNotification;
import com.cargill.model.TmcodeGroup;
import com.cargill.model.parameter.order.OrderDetail;
import com.cargill.properties.BatchProperties;
import com.cargill.repository.CustomerOrderRepository;
import com.cargill.repository.CustomerRepository;
import com.cargill.repository.SchedulerNotificationRepository;
import com.cargill.repository.TmcodeGroupRepository;
import com.cargill.util.DateUtil;
import com.google.common.collect.Maps;

import lombok.extern.slf4j.Slf4j;

/**
 * 批次排程作業
 * 
 * @author Mike
 *
 */
@Slf4j
@Component
public class CargillBatchScheduler {



	private CustomerOrderRepository customerOrderRepository;
	private CustomerRepository customerRepository;
	private SchedulerNotificationRepository schedulerNotificationRepository;
	private BatchProperties batchProperties;
	private RestTemplate restTemplate;
	private SchedulerManagerService schedulerManagerService;
	@Autowired
	private TmcodeGroupRepository tmcodeGroupRepository;
	@Value("${cargill.batch.spcific.notify.date:}")
	private String notifyDate;

	@Autowired
	public void setCustomerOrderRepository(CustomerOrderRepository customerOrderRepository) {
		this.customerOrderRepository = customerOrderRepository;
	}

	@Autowired
	public void setSchedulerNotificationRepository(SchedulerNotificationRepository schedulerNotificationRepository) {
		this.schedulerNotificationRepository = schedulerNotificationRepository;
	}

	@Autowired
	public void setSchedulerManagerService(SchedulerManagerService schedulerManagerService) {
		this.schedulerManagerService = schedulerManagerService;
	}
	@Autowired
	public void setBatchProperties(BatchProperties batchProperties) {
		this.batchProperties = batchProperties;
	}

	@Autowired
	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	@Autowired
	public void setCustomerRepository(CustomerRepository customerRepository) {
		this.customerRepository = customerRepository;
	}
	
    /**
     * 每日計算欲推播對象
     * @author Mike
     */
    @Scheduled(cron = "${cargill.batch.calculate-notification-date}")
    public void calculateNotificationDate() {
    	log.info("=========================[開始]進行每日計算欲推播對象=========================");
    	if (batchProperties.getBatchEnabled()) {
    		Calendar start = Calendar.getInstance();
    		start.setTime(DateUtil.getBeforeOneMonthByNow());
    		Calendar end = Calendar.getInstance();
    		end.setTime(DateUtil.now());
    		if (batchProperties.getRealExecute() == false) {
    			start.set(2020, 7, 1, 0, 0, 0);
    			end.set(2020, 7, 31, 23, 59, 59);
    		}
    		long s, e;
    		s = System.currentTimeMillis();
    		// 查詢本月欲推播名單
    		List<CustomerOrder> totalDatas = this.customerOrderRepository.findByDeliveryDateBetween(start, end);
    		e = System.currentTimeMillis();
    		log.debug("findByDeliveryDateBetween, Cost: ", (e - s));
    		log.debug("Data Count: ", totalDatas.size());
    		s = System.currentTimeMillis();
    		List<String> sub_ids = totalDatas.stream()
    				.map(CustomerOrder::getCustomerSubId)
    				.collect(Collectors.toList());
    		List<Customer> totalCustomers = this.customerRepository.findByCustomerPk_CustomerSubIdIn(sub_ids);
    		Map<String, String> realTmCode = totalCustomers.stream()
    				.collect(Collectors.toMap(CargillBatchScheduler::getSubId, Customer::getTmcode, 
    						(existing, replacement) -> existing));
    		// 依客戶分堆 -> 依料別分堆 -> 依發送日分堆, 參考:{@code com.cargill.service.impl.CustomerOrderService.buildA2} 邏輯
    		List<BatchDto> pile = 
    				totalDatas
    				// 依客戶分堆
    				.stream().collect(Collectors.groupingBy(CustomerOrder::getCustomerSubId))
    				.entrySet()
    				.stream()
    				.map(data -> {
    					BatchDto a2 = new BatchDto();
    					data.getValue().stream().findFirst().ifPresent(order -> {
    						a2.setCustomerSubId(order.getCustomerSubId());
    						a2.setCustomerSubName(order.getCustomerSubName());
    						// START <20200831> FIXBUG: 客戶表示Tmcode應該從Customer表取得
//    						a2.setTmcode(order.getTmcode());
    						String shipTo = Optional.of(order.getShipTo()).orElse(StringUtils.EMPTY);
    						String csub_id = Optional.of(order.getCustomerSubId()).orElse(StringUtils.EMPTY);
    						String key = shipTo.concat(csub_id);
    						a2.setTmcode(realTmCode.get(key));
    						// END <20200831> FIXBUG: 客戶表示Tmcode應該從Customer表取得
    						a2.setDeliveryDate(order.getDeliveryDate());
    					});
    					data.getValue().stream()
    					.peek(order -> {
    						data.getValue().stream()
    						.peek((orderForDetail) -> {
    							orderForDetail.getOrderDetailList()
    							.stream()
    							// 依料號分堆
    							.collect(Collectors.groupingBy(OrderDetail::getCode))
    							.entrySet()
    							.stream()
    							.peek((detail) -> {
    								BatchRowDto currentRow = a2.getRowList().stream()
    										.filter(it -> detail.getKey().equals(it.getCode()))
    										.findFirst().orElseGet(() -> {
    											BatchRowDto temp = new BatchRowDto();
    											a2.getRowList().add(temp);
    											return temp;
    										});
    								OrderDetail cellOrder = detail.getValue().get(0);
    								currentRow.setCode(detail.getKey());
    								currentRow.setName(cellOrder.getName());
    								// 依日期陣列分堆
    								currentRow.getCellList().add(orderForDetail.getDeliveryDate());
    							}).collect(Collectors.toList());
    						}).collect(Collectors.toList());
    					}).collect(Collectors.toList());
    					return a2;
    				}).collect(Collectors.toList());
    		List<SchedulerNotification> notifies = new ArrayList<>();
    		pile.forEach(dto -> {
    			dto.getRowList().forEach(row -> { 
    				List<Calendar> days = row.getCellList();
    				if (days.size() <= 2) {
    					return ;
    				}
    				Collections.reverse(days);
    				LocalDate first = DateUtil.convertToLocalDate(days.get(2));
    				LocalDate second = DateUtil.convertToLocalDate(days.get(1));
    				LocalDate third = DateUtil.convertToLocalDate(days.get(0));
    				long firstBetween =  ChronoUnit.DAYS.between(first, second);
    				long secondBetween = ChronoUnit.DAYS.between(second, third);
    				if (firstBetween == 0 || secondBetween == 0 || (firstBetween != secondBetween)) {
    					return;
    				}
    				LocalDate notificationDate = 
    						third.plusDays(secondBetween)
    						// <20190801> 客戶希望提前一天推播
    						.minusDays(1)
    						;
    				SchedulerNotification sn = new SchedulerNotification();
    				Date nextDeliveryDate = DateUtil.convert(notificationDate).getTime();
    				String code = row.getCode();
    				sn.setNotificationDate(nextDeliveryDate);
    				sn.setTmcode(dto.getTmcode());
    				sn.setCustomerSubName(dto.getCustomerSubName());
    				sn.setCustomerSubId(dto.getCustomerSubId());
    				sn.setCode(code);
    				sn.setMessage(
    						MessageFormat.format(
    								batchProperties.getNotificationMessage(),
    								dto.getCustomerSubName(),
    								// <20190805> User說只要料名就好
//								row.getCode().concat("-").concat(row.getName()),
    								row.getName(),
    								DateUtil.DashDateFormatter.format(notificationDate)
    								)
    						);
    				sn.setOwner(OwnerType.SALES.getCode());
    				notifies.add(sn);
    			});
    		});
    		log.debug("data process , Cost: " + (e - s));
    		s = System.currentTimeMillis();
    		Calendar preStart = Calendar.getInstance();
    		preStart.setTime(DateUtil.getBeforeOneMonthByNow());
    		Calendar preEnd = Calendar.getInstance();
    		preEnd.setTime(DateUtil.getAfterOneMonthByNow());
    		if (batchProperties.getRealExecute() == false) {
    			preStart = start;
    			preEnd = end;
    		}
    		List<SchedulerNotification> sns = 
    				this.schedulerNotificationRepository.findByNotificationDateBetween(preStart.getTime(), preEnd.getTime());
    		// 差集(欲新增扣除已存在的資料)
    		List<SchedulerNotification> reduceNeedInsertResult = Collections.emptyList();
    		if (batchProperties.getRealExecute()) {
    			reduceNeedInsertResult = notifies.stream()
    					// <20190805> User說如果不是隔天空桶，今日推播結果將不儲存
//					.filter(item -> LocalDate.now().isBefore(DateUtil.convertToLocalDate(item.getNotificationDate())))
    					.filter(item -> LocalDate.now().isEqual(DateUtil.convertToLocalDate(item.getNotificationDate())))
    					.filter(item -> !isContains(sns, item))
    					.collect(Collectors.toList());
    		}
    		else {
    			reduceNeedInsertResult = notifies.stream()
    					.filter(item -> !isContains(sns, item))
    					.collect(Collectors.toList());
    		}
    		e = System.currentTimeMillis();
    		log.debug("exclude exist datas , Cost: " + (e - s));
    		this.schedulerNotificationRepository.save(reduceNeedInsertResult);
    	}
		log.info("=========================[結束]進行每日計算欲推播對象=========================");
    }
    
    private boolean isContains(List<SchedulerNotification> sns, SchedulerNotification sn) {
		if (CollectionUtils.isEmpty(sns) || sn == null) {
			return Boolean.FALSE;
		}
		boolean isContains = sns.stream().filter(s -> {
			return s.getNotificationDate().compareTo(sn.getNotificationDate()) == 0
					&& s.getTmcode().equals(sn.getTmcode())
					&& s.getCustomerSubId().equals(sn.getCustomerSubId());
		})
		.findFirst().isPresent();
		return isContains;
	}

    /**
     * 每日固定查詢推播對象主動推播
     * @author Mike
     */
    @Scheduled(cron = "${cargill.batch.notification-customer}")
    public void notificationCustomer() {
    	log.debug("=========================[開始]每日固定查詢推播對象主動推播=========================");
    	long start, end;
    	start = System.currentTimeMillis();

    	//取得資料
		List<SchedulerNotification> sns = this.schedulerManagerService.getNotifictions();

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		Boolean result = Boolean.TRUE;
		// 取得名單列表
		List<String> controlList = this.getTmcodeList();
		for (SchedulerNotification sn : sns) {
			if (batchProperties.getControlList()
					&& controlList.contains(sn.getTmcode()) == false) {
				continue ;
			}


			Map<String, Object> map = Maps.newHashMap();
			map.put("userIds", Arrays.asList(sn.getTmcode()));
			map.put("title", batchProperties.getNotificationTitle());
			map.put("message", sn.getMessage());
			HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);
			ResponseEntity<Boolean> response = null;
			if (batchProperties.getDisablePush().booleanValue()) {
				response = new ResponseEntity<Boolean>(HttpStatus.OK);
				result = Boolean.FALSE;
			} else {
				response = restTemplate.postForEntity(batchProperties.getNotificationUrl(), entity, Boolean.class);
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {
				}
			}
			Boolean temp = response.getBody() == null ? Boolean.FALSE : response.getBody();
			result &= temp;
		}
		if (result.booleanValue()) {
			sns.stream()
			.forEach(s -> {
				s.setFinished(Boolean.TRUE);
			});
			this.schedulerNotificationRepository.save(sns);
		}
		end = System.currentTimeMillis();
		log.debug("每日固定查詢推播對象主動推播, 花費時間: " + (end - start));
		log.debug("=========================[結束]每日固定查詢推播對象主動推播=========================");
	}

	private static String getSubId(Customer c) {
		if (c == null) {
			return StringUtils.EMPTY;
		}
		return c.getCustomerPk().getAddressShipTo().concat(c.getCustomerPk().getCustomerSubId());
	}

	/**
	 * 取得業務員名單列表
	 * 
	 * @author Mike
	 *
	 * @return
	 */
	private List<String> getTmcodeList() {
		List<TmcodeGroup> tmcodes = this.tmcodeGroupRepository.findAll();
		return tmcodes.stream()
				.map(TmcodeGroup::getTmcodeId)
				.collect(Collectors.toList());
	}
	
}
