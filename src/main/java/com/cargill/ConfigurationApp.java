package com.cargill;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import com.cargill.properties.AppProperties;

@Configuration
@EnableConfigurationProperties(AppProperties.class)
public class ConfigurationApp {
	
}
