package com.cargill.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
/***
 * C5 特配原料 品項價格
 */
public class ProductTP extends BaseEntity {
    @Id
    @Column(length = 10)
    private String code;
    private String name;
    @Column(columnDefinition = "DECIMAL(10,3)")
    private BigDecimal price = BigDecimal.ZERO;
}
