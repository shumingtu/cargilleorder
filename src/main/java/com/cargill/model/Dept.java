package com.cargill.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
public class Dept extends BaseEntity {
    @Id
    private String code;
    private String name;
}
