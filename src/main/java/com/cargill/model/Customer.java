package com.cargill.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Lob;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
public class Customer extends BaseEntity {

    @EmbeddedId
    private CustomerPK customerPk;
    private String type;
    private String customerSubName;
    private String customerMainName;
    private String phone;
    private String tmcode;
    private String address;
    private String sscode;
    private String livestock;


    // 20210628 增加memo欄位及功能
    @Column
    @Lob
    private String memo;
}
