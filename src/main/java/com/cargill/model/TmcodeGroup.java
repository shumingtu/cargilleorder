package com.cargill.model;


import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
public class TmcodeGroup extends BaseEntity{

    @Id
    @Column(length = 5)
    private String tmcodeId;

    @Column(length = 20)
    private String groupName;
}
