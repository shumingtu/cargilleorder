package com.cargill.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
public class AppUserType extends BaseEntity{

    @Id
    private String code;
    private String name;
}
