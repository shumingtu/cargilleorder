package com.cargill.model;

import com.cargill.model.parameter.TPAreaCode;
import com.cargill.model.parameter.TPAreaType;
import com.cargill.model.parameter.TPDrugType;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
/***
 * C3~4 特配原料 資料 [台中/高雄][20/40/60 [公司/自備]藥
 */
public class ProductTPDrug extends BaseEntity {
    //area_code + '_' +  area_type + '_' + drug_type + '_' + code
    @Id
    @Column(length = 255)
    private String complexCode;

    @Column(length = 10)
    private String code;
    private String name;

    @Column(length = 20)
    @Enumerated(EnumType.STRING)
    private TPAreaCode areaCode;

    @Column(length = 20, nullable = false)
    @Enumerated(EnumType.STRING)
    private TPAreaType areaType;

    @Column(length = 20, nullable = false)
    @Enumerated(EnumType.STRING)
    private TPDrugType drugType;

    private String recommendedAmount;
    private String maxAmount;
    private String drugWithdrawal;
    private String note;
}
