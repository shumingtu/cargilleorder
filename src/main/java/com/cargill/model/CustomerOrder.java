package com.cargill.model;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
/***
 *A1 訂單資料
 */
public class CustomerOrder extends CustomerOrderBaseEntity {

    // 流水號
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "BIGINT")
    private BigInteger id;

    private String tmcode;

    // 訂單編號
    private String orderNo;

    // 訂單出貨日期
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar deliveryDate;

    private String customerSubId;
    private String customerSubName;
    private String customerPhone;

    // 送貨地點
    private String shipTo;
    private String descWeb;
    private String descApp;
    // 訂單狀態
    @Column(length = 20, nullable = false)
    private String type;
    private String poCode;
    
    private String orderSys;

    @Fetch(FetchMode.SELECT)
    @OneToMany(mappedBy="customerPk.customerSubId")
    private List<Customer> customer;
}
