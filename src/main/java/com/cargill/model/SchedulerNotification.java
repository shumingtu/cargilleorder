package com.cargill.model;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
public class SchedulerNotification extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "BIGINT")
    private BigInteger id;
    @Temporal(TemporalType.DATE)
    private Date notificationDate;
    private String tmcode;
    private String customerSubName;
    private String customerSubId;
    private String code;
    private String message;
    private String owner;
    private boolean finished;
    private boolean cancelled;
    private String remark;
    
}
