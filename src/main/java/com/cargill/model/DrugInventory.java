package com.cargill.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
public class DrugInventory  {

    // 流水號
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "BIGINT")
    private BigInteger id;

    private String customerSubId;
    private String customerSubName;
    private String code;
    private String name;
    private String unit;
    private String amount;
    private String status;
    private String createUser;
    private String createUserName;
    private String type;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    protected Calendar createTime;

    @PrePersist
    public void prePersist() {
        this.createTime = Calendar.getInstance();
    }
}
