package com.cargill.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.Calendar;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
public class AppUser extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "BIGINT")
    private BigInteger id;
    private String type;
    private String unicode;
    private String userName;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;
    private String email;
    private String phone;
    @Column(length = 1)
    private String canAct;
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar lastLoginTime;
    private String verifyCode;
    private String verifyResult;
    private Integer verifyCounts;
    private Integer verifySendCounts;
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar verifyLasttime;
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar verifyCreatetime;

}
