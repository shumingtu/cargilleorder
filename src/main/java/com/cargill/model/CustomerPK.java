package com.cargill.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class CustomerPK implements Serializable {

    @Column(length = 10)
    private String customerSubId;

    @Column(length = 10)
    private String customerMainId;

    private String addressShipTo;
}
