package com.cargill.model;

import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Calendar;

@Data
@MappedSuperclass
public abstract class BaseEntity {

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    protected Calendar createTime;

    @Temporal(TemporalType.TIMESTAMP)
    protected Calendar modifyTime;

    @PrePersist
    public void prePersist() {
        this.createTime = Calendar.getInstance();
    }

    @PreUpdate
    public void preUpdate() {
        this.modifyTime = Calendar.getInstance();
    }

}
