package com.cargill.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
public class CustomerQuota extends BaseEntity{
    @Id
    private String customerId;
    private String customerName;
    private String note;
}
