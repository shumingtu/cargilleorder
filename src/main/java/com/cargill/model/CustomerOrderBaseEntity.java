package com.cargill.model;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.Lob;
import javax.persistence.MappedSuperclass;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Transient;

import com.cargill.dto.WebAccountDto;
import com.cargill.model.parameter.ContextWrapper;
import com.cargill.model.parameter.order.OrderDetail;
import com.cargill.util.UserUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@MappedSuperclass
public abstract class CustomerOrderBaseEntity extends BaseEntity {
    private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyyMMdd HH:mm");

    // 訂單人
    private String orderPerson;

    @Column
    @Lob
    private String details;

    @Column
    @Lob
    private String modifyLogs = "";

    @Transient
    private List<OrderDetail> orderDetailList = Collections.emptyList();

    @Transient
    private List<String> modifyLogList = Collections.emptyList();

    @PostLoad
    public void postLoad() {
        try {
            ObjectMapper objectMapper = ContextWrapper.getContext().getBean("objectMapper", ObjectMapper.class);
            orderDetailList = objectMapper.readValue(details, new TypeReference<List<OrderDetail>>() {
            });

            modifyLogList = Optional.ofNullable(modifyLogs)
                    .map(it -> Arrays.stream(it.split(",")).collect(Collectors.toList()))
                    .orElse(Collections.emptyList());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @PrePersist
    public void prePersist() {
        this.createTime = Calendar.getInstance();

        updateDetails();

        modifyLogs += SDF.format(new Date()) + " WEB " + orderPerson + " " + "新增,";
    }

    @PreUpdate
    public void preUpdate() {
        this.modifyTime = Calendar.getInstance();

        updateDetails();
        
        WebAccountDto userInfo = UserUtils.getUserInfo();
        String modifyUserName = Optional.ofNullable(userInfo).map(WebAccountDto::getUserName).orElse("");
        
        // <20190802> BugFixed: 修正修改者為登入者
//        modifyLogs += SDF.format(new Date()) + " WEB " + orderPerson + " " + "儲存,";
        modifyLogs += SDF.format(new Date()) + " WEB " + modifyUserName + " " + "儲存,";
    }

    public void updateDetails() {
        try {
            ObjectMapper objectMapper = ContextWrapper.getContext().getBean("objectMapper", ObjectMapper.class);
            this.details = objectMapper.writeValueAsString(orderDetailList);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
