package com.cargill.model.view;

import com.cargill.model.parameter.TPAreaCode;
import com.cargill.model.parameter.TPAreaType;
import com.cargill.model.parameter.TPDrugType;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import java.math.BigDecimal;

@Data
@Entity
/***
 * C3~4 特配原料與價格 View
 */
public class ViewProductTPDrug {
    @Id
    private String complex_code;

    private String code;
    private String name;

    @Enumerated(EnumType.STRING)
    private TPAreaCode areaCode;

    @Enumerated(EnumType.STRING)
    private TPAreaType areaType;

    @Enumerated(EnumType.STRING)
    private TPDrugType drugType;

    private String recommendedAmount;
    private String maxAmount;
    private String drugWithdrawal;
    private String note;

    private String tpCode;
    private String tpName;

    private BigDecimal price = BigDecimal.ZERO;
}
