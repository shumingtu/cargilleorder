package com.cargill.model.parameter;

/***
 * 特配原料 - 公司/自備藥
 */
public enum TPDrugType {
    COMPANY, SELF
}
