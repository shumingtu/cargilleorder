package com.cargill.model.parameter;

/***
 * 特配原料 - 地區
 */
public enum TPAreaType {
    TC, KS
}
