package com.cargill.model.parameter;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JacksonXmlRootElement(localName = "SubmitRes")
public class SubmitRes {
    @JacksonXmlProperty(localName = "ResultCode")
    private String resultCode;

    @JacksonXmlProperty(localName = "ResultText")
    private String resultText;

    @JacksonXmlProperty(localName = "MessageId")
    private String messageId;
}
