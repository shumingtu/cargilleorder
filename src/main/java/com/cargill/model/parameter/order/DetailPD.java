package com.cargill.model.parameter.order;

import lombok.Data;

/***
 * 訂單 - 訂單內容 - 生產小單
 */
@Data
public class DetailPD {
    // 生產小單流水號
    private String id;

    // 建單日期
    private String createDate;

    private String note;
}
