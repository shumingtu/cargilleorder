package com.cargill.model.parameter.order;

import lombok.Data;

/***
 * 訂單 - 訂單內容
 */
@Data
public class OrderDetail {

    // 飼料編號
    private String code;
    // 飼料名稱
    private String name;
    // 散裝/包裝
    private String unit;
    // 噸數/包數
    private Double count;
    // 生產規格
    private String spec;
    // 總重
    private Double weight;
    // 使用階段
    private String phase;
    // 噸數/包數 的價格
    private Double price;
    //車種
    private String vehicleType;
    //運輸車種名
    private String vehicleName;
    // 特配單/工廠
    private DetailSP detailSP;

    // 生產小單
    private DetailPD detailPD;
}
