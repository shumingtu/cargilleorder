package com.cargill.model.parameter.order;

import lombok.Data;

import java.math.BigDecimal;

/***
 * 訂單 - 訂單內容 - 特配單/工廠 - 公司/自備藥
 */
@Data
public class DetailDrugSP {

    // 編號
    private String code;
    // 名稱
    private String name;
    // 每1噸用量(KG)
    private Double count1kg;
    // 每2噸用量(KG)
    private Double count2kg;
    // 建議/最高劑量
    private String recommend;
    // 總量
    private Double totalCount;
    // 停藥期天數
    private String days;
    // 特配備註
    private String note;

    // 飼料扣藥重的錢
    private BigDecimal price;
}
