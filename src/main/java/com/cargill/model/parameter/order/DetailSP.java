package com.cargill.model.parameter.order;

import lombok.Data;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

/***
 * 訂單 - 訂單內容 - 特配單/工廠
 */
@Data
public class DetailSP {
    // 特配單流水號
    private String id;

    // 特配工廠別
    private String value;

    // 建單日期
    private String createDate;

    // 症狀
    private List<String> symptom;

    // 特配出貨日期
    private String deliveryDate;
    private String deliveryDateDisplay;
    private String deliveryTimeDisplay;

    // 包裝規格註記
    private String productSpec;
    // 生產線註記
    private String productLineNote;
    // 生產備註
    private String productNote;

    // 編制
    private String noteA;
    // 審核
    private String noteB;
    // 傳真編號
    private String noteC;

    // 公司藥
    List<DetailDrugSP> drugCompanyList = Collections.emptyList();
    // 自備藥
    List<DetailDrugSP> drugSelfList = Collections.emptyList();

    // 特配總價
    private Integer totalPrice;
    // 飼料扣藥重
    private Double codeWeight;
    // 特配金額
    private Double spPrice;

    // 修改紀錄
    private List<String> modifyLogs = Collections.emptyList();
}
