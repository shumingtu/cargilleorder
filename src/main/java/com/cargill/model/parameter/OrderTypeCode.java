package com.cargill.model.parameter;

/***
 * 訂單狀態
 */
public enum OrderTypeCode {
    WEB("CS 新單"), APP("APP 新單"), WEB_END("CS 已結單"), APP_END("APP 已結單"),DEL("已刪除");

    private String desc;

    OrderTypeCode(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

}
