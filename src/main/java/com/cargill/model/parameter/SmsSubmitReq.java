package com.cargill.model.parameter;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "SmsSubmitReq")
@XmlAccessorType(XmlAccessType.NONE)
public class SmsSubmitReq {
    @XmlElement(name = "SysId")
    private String sysId;

    @XmlElement(name = "SrcAddress")
    private String srcAddress;

    @XmlElement(name = "DestAddress")
    private String destAddress;

    @XmlElement(name = "SmsBody")
    private String smsBody;

    @XmlElement(name = "DrFlag")
    private boolean drFlag;
    @XmlElement(name = "FirstFailFlag")
    private boolean firstFailFlag;
}
