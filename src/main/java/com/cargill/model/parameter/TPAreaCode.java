package com.cargill.model.parameter;

/***
 * 特配原料 - 20/40/50/60/NULL
 */
public enum TPAreaCode {
    TWENTY, FOURTY, FIFTY, SIXTY, NULL
}
