package com.cargill.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
/***
 * C1 飼料產品 常用抽藥
 */
public class ProductCUCY extends BaseEntity {

    @EmbeddedId
    private ProductCUCYPK productCUCYPK;
    private String name;
    private String drugName;
    @Column(columnDefinition = "DECIMAL(10,2)")
    private BigDecimal amount = BigDecimal.ZERO;
}
