package com.cargill.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.math.BigInteger;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
public class CustomerFeedingscale extends BaseEntity {

    @Id
    private String customerId;
    private String customerName;
    private String feedingScale;
    private String size;
    private String note;

}
