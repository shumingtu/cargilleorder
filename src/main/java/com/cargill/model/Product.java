package com.cargill.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
/***
 *C1 飼料產品 品項價格
 */
public class Product extends BaseEntity {
    @Id
    @Column(length = 20)
    private String code;
    private String name;
    private String phase;

    @Column(columnDefinition = "DECIMAL(10,2)")
    private BigDecimal priceX = BigDecimal.ZERO;
    @Column(columnDefinition = "DECIMAL(10,2)")
    private BigDecimal price3kg3K = BigDecimal.ZERO;
    @Column(columnDefinition = "DECIMAL(10,2)")
    private BigDecimal price10kg1 = BigDecimal.ZERO;
    @Column(columnDefinition = "DECIMAL(10,2)")
    private BigDecimal price20kg2 = BigDecimal.ZERO;
    @Column(columnDefinition = "DECIMAL(10,2)")
    private BigDecimal price25kg2point5C = BigDecimal.ZERO;
    @Column(columnDefinition = "DECIMAL(10,2)")
    private BigDecimal price30kg3 = BigDecimal.ZERO;
    @Column(columnDefinition = "DECIMAL(10,2)")
    private BigDecimal price30kg3C = BigDecimal.ZERO;
    @Column(columnDefinition = "DECIMAL(10,2)")
    private BigDecimal price50kg5 = BigDecimal.ZERO;
    @Column(columnDefinition = "DECIMAL(10,2)")
    private BigDecimal price1000kgJ = BigDecimal.ZERO;
    @Column(columnDefinition = "DECIMAL(10,2)")
    private BigDecimal price500kgJ5 = BigDecimal.ZERO;
}
