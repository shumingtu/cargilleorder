package com.cargill.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.Calendar;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
public class User extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "BIGINT")
    private BigInteger id;
    private String account;
    private String userName;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;
    @Column(length = 10)
    private String dept;
    @Column(length = 50)
    private String actCode;
    private Calendar lastActCodeTime;
    @Column(length = 1)
    private String status;
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar lastLoginTime;
}
