package com.cargill.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
public class MarketingFile extends BaseEntity {
    @Id
    private String code;

    private Integer carouselSec;

    private String originalName1;
    private String fileName1;
    private String fileLink1;

    private String originalName2;
    private String fileName2;
    private String fileLink2;

    private String originalName3;
    private String fileName3;
    private String fileLink3;

    private String originalName4;
    private String fileName4;
    private String fileLink4;

    private String originalName5;
    private String fileName5;
    private String fileLink5;
}
