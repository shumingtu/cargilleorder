package com.cargill.transform;

import java.util.Calendar;
import java.util.Optional;

import org.springframework.stereotype.Component;

import com.cargill.Constants;
import com.cargill.dto.CustomerOrderDto;
import com.cargill.model.CustomerOrder;
import com.cargill.model.parameter.OrderTypeCode;

@Component
public class CustomerOrderTransform extends AbstractTransform<CustomerOrder, CustomerOrderDto> {

    public CustomerOrderDto toDto(CustomerOrder order) {
        return new CustomerOrderDto(
                order.getId(),
                order.getOrderNo(),
                order.getCreateTime(),
                order.getCustomerSubId(),
                order.getCustomerSubName(),
                order.getCustomerPhone(),

                Optional.ofNullable(order.getDeliveryDate()).map(Calendar::getTime).map(Constants.SDF_DATE_ONLY::format).orElse(""),
                Optional.ofNullable(order.getDeliveryDate()).map(Calendar::getTime).map(Constants.SDF_TIME_ONLY::format).orElse(""),
                
                order.getShipTo(),
                order.getDescWeb(),
                order.getDescApp(),
                OrderTypeCode.valueOf(order.getType()),
                order.getPoCode(),
                Optional.ofNullable(order.getCreateTime()).map(Calendar::getTime).map(Constants.SDF::format).orElse(""),
                order.getOrderPerson(),
                order.getTmcode(),
                order.getModifyTime(),
                (new OrderDetailTransform()).toDtos(order.getOrderDetailList()),
                order.getModifyLogList()
        );
    }
}
