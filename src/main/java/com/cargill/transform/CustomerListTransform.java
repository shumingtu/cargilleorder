package com.cargill.transform;

import com.cargill.dto.CustomerDto;
import com.cargill.model.Customer;
import org.springframework.stereotype.Component;

@Component
public class CustomerListTransform extends AbstractTransform<Customer, CustomerDto>{

    public CustomerDto toDto(Customer customer) {
        return new CustomerDto(
                customer.getCustomerPk().getCustomerSubId(),
                customer.getCustomerPk().getCustomerMainId(),
                customer.getCustomerPk().getAddressShipTo(),
                false,
                customer.getCustomerSubName().replaceAll("　", "").trim(),
                customer.getCustomerMainName().replaceAll("　", "").trim(),
                customer.getPhone(),
                customer.getTmcode(),
                customer.getAddress(),
                null,
                null,
                customer.getMemo()
        );

    }
}
