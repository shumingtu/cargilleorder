package com.cargill.transform;

import com.cargill.dto.CustomerQuotaDto;
import com.cargill.model.CustomerQuota;
import org.springframework.stereotype.Component;

@Component
public class CustomerQuotaTransform extends AbstractTransform<CustomerQuota, CustomerQuotaDto>{

    public CustomerQuotaDto toDto(CustomerQuota customerQuota) {
        return new CustomerQuotaDto(
                customerQuota.getCustomerId(),
                customerQuota.getCustomerName(),
                customerQuota.getNote()
        );

    }
}
