package com.cargill.transform;

import com.cargill.dto.AppAccountDto;
import com.cargill.dto.WebAccountDto;
import com.cargill.model.AppUser;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Optional;

@Component
public class AppUserTransform extends AbstractTransform<AppUser, AppAccountDto> {

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");

    @Override
    public AppAccountDto toDto(AppUser appUser) {
        return new AppAccountDto(
                appUser.getId(),
                appUser.getType(),
                appUser.getUnicode(),
                appUser.getUserName(),
                appUser.getEmail(),
                appUser.getPhone(),
                appUser.getCanAct(),
                "",
                Optional.ofNullable(appUser.getVerifyCode()).orElse("無"),
                appUser.getVerifyResult(),
                Optional.ofNullable(appUser.getVerifyCounts()).orElse(0),
                Optional.ofNullable(appUser.getVerifySendCounts()).orElse(0),
                Optional.ofNullable(appUser.getVerifyLasttime()).isPresent() ? sdf.format(appUser.getVerifyLasttime().getTime()) : "尚未",
                Optional.ofNullable(appUser.getVerifyCreatetime()).isPresent() ? sdf.format(appUser.getVerifyCreatetime().getTime()) : "尚未"
        );
    }
}

