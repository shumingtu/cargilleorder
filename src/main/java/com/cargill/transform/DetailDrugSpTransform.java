package com.cargill.transform;

import com.cargill.dto.DetailDrugSpDto;
import com.cargill.model.parameter.order.DetailDrugSP;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Optional;

@Component
public class DetailDrugSpTransform extends AbstractTransform<DetailDrugSP, DetailDrugSpDto> {

    @Override
    public DetailDrugSpDto toDto(DetailDrugSP drugSP) {
        return new DetailDrugSpDto(
                drugSP.getCode(),
                drugSP.getName(),
                drugSP.getCount1kg(),
                drugSP.getCount2kg(),
                drugSP.getRecommend(),
                drugSP.getTotalCount(),
                drugSP.getDays(),
                drugSP.getNote(),
                Optional.ofNullable(drugSP.getPrice()).orElse(BigDecimal.ZERO)
        );
    }
}
