package com.cargill.transform;

import com.cargill.dto.MarketingFileDto;
import com.cargill.model.MarketingFile;
import org.springframework.stereotype.Component;

@Component
public class MarketingFileTransform extends AbstractTransform<MarketingFile, MarketingFileDto> {
    @Override
    public MarketingFileDto toDto(MarketingFile file) {
        MarketingFileDto dto = new MarketingFileDto();
        dto.setCarouselSec(file.getCarouselSec());

        dto.setFileName1(file.getFileName1());
        dto.setOriginalName1(file.getOriginalName1());
        dto.setFileLink1(file.getFileLink1());

        dto.setFileName2(file.getFileName2());
        dto.setOriginalName2(file.getOriginalName2());
        dto.setFileLink2(file.getFileLink2());

        dto.setFileName3(file.getFileName3());
        dto.setOriginalName3(file.getOriginalName3());
        dto.setFileLink3(file.getFileLink3());

        dto.setFileName4(file.getFileName4());
        dto.setOriginalName4(file.getOriginalName4());
        dto.setFileLink4(file.getFileLink4());

        dto.setFileName5(file.getFileName5());
        dto.setOriginalName5(file.getOriginalName5());
        dto.setFileLink5(file.getFileLink5());

        return dto;
    }
}
