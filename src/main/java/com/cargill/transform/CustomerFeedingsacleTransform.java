package com.cargill.transform;

import com.cargill.dto.CustomerFeedingscaleDto;
import com.cargill.model.CustomerFeedingscale;
import org.springframework.stereotype.Component;

@Component
public class CustomerFeedingsacleTransform extends AbstractTransform<CustomerFeedingscale, CustomerFeedingscaleDto>{

    public CustomerFeedingscaleDto toDto(CustomerFeedingscale customerFeedingscale) {
        return new CustomerFeedingscaleDto(
                customerFeedingscale.getCustomerId(),
                customerFeedingscale.getCustomerName(),
                customerFeedingscale.getFeedingScale(),
                customerFeedingscale.getSize(),
                customerFeedingscale.getNote()
        );

    }
}
