package com.cargill.transform;

import com.cargill.dto.DetailPdDto;
import com.cargill.dto.DetailSpDto;
import com.cargill.dto.OrderDetailDto;
import com.cargill.model.parameter.ContextWrapper;
import com.cargill.model.parameter.order.DetailPD;
import com.cargill.model.parameter.order.DetailSP;
import com.cargill.model.parameter.order.OrderDetail;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class OrderDetailTransform extends AbstractTransform<OrderDetail, OrderDetailDto> {
    @Override
    public OrderDetailDto toDto(OrderDetail detail) {
        DetailSpDto detailSpDto = (new DetailSpTransform()).toDto(Optional.ofNullable(detail.getDetailSP()).orElse(new DetailSP()));
        DetailPdDto detailPdDto = (new DetailPdTransform()).toDto(Optional.ofNullable(detail.getDetailPD()).orElse(new DetailPD()));

        String jsonDetailSP = "{}";
        String jsonDetailPD = "{}";
        try {
            ObjectMapper objectMapper = ContextWrapper.getContext().getBean("objectMapper", ObjectMapper.class);

            jsonDetailSP = objectMapper.writeValueAsString(detailSpDto);
            jsonDetailPD = objectMapper.writeValueAsString(detailPdDto);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return new OrderDetailDto(
                detail.getCode(),
                detail.getName(),
                detail.getUnit(),
                detail.getCount(),
                detail.getSpec(),
                detail.getWeight(),
                detail.getPhase(),
                detail.getPrice(),
                detail.getVehicleType(),
                detail.getVehicleName(),
                detailSpDto,
                jsonDetailSP,
                detailPdDto,
                jsonDetailPD
        );
    }
}
