package com.cargill.transform;

import com.cargill.Constants;
import com.cargill.dto.DetailSpDto;
import com.cargill.model.parameter.order.DetailSP;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Collections;
import java.util.Optional;

@Component
public class DetailSpTransform extends AbstractTransform<DetailSP, DetailSpDto> {

    @Override
    public DetailSpDto toDto(DetailSP sp) {
        return new DetailSpDto(
                sp.getId(),
                false,
                false,
                sp.getValue(),
                sp.getCreateDate(),
                Optional.ofNullable(sp.getSymptom()).orElse(Collections.emptyList()),
                sp.getDeliveryDate(),
                Optional.ofNullable(sp.getDeliveryDate()).map(d -> d.split(" ")[0].trim()).orElse(""),
                Optional.ofNullable(sp.getDeliveryDate()).map(d -> d.split(" ")[1].trim()).orElse(""),
                sp.getProductLineNote(),
                sp.getProductNote(),
                sp.getProductSpec(),
                sp.getNoteA(),
                sp.getNoteB(),
                sp.getNoteC(),
                (new DetailDrugSpTransform()).toDtos(Optional.ofNullable(sp.getDrugCompanyList()).orElse(Collections.emptyList())),
                (new DetailDrugSpTransform()).toDtos(Optional.ofNullable(sp.getDrugSelfList()).orElse(Collections.emptyList())),
                sp.getTotalPrice(),
                sp.getCodeWeight(),
                sp.getSpPrice(),
                sp.getModifyLogs()
        );
    }
}
