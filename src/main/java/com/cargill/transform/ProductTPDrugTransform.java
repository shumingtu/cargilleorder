package com.cargill.transform;

import com.cargill.dto.ProductTPDrugDto;
import com.cargill.model.ProductTPDrug;
import org.springframework.stereotype.Component;

@Component
public class ProductTPDrugTransform extends AbstractTransform<ProductTPDrug, ProductTPDrugDto> {
    @Override
    public ProductTPDrugDto toDto(ProductTPDrug product) {
        return new ProductTPDrugDto(
                product.getCode(),
                product.getName(),
                product.getAreaCode(),
                product.getAreaType(),
                product.getDrugType(),
                product.getRecommendedAmount(),
                product.getMaxAmount(),
                product.getDrugWithdrawal(),
                product.getNote()
        );
    }
}
