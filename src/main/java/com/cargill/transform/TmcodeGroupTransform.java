package com.cargill.transform;

import com.cargill.dto.TmcodeGroupDto;
import com.cargill.model.TmcodeGroup;
import org.springframework.stereotype.Component;

@Component
public class TmcodeGroupTransform extends AbstractTransform<TmcodeGroup, TmcodeGroupDto>{

    @Override
    public TmcodeGroupDto toDto(TmcodeGroup tmcodeGroup) {
        return new TmcodeGroupDto(
                tmcodeGroup.getTmcodeId(),
                tmcodeGroup.getGroupName()
        );
    }
}
