package com.cargill.transform;

import org.springframework.stereotype.Component;

import com.cargill.dto.NotificationCustomerDto;
import com.cargill.model.SchedulerNotification;

@Component
public class SchedulerNotificationTransform extends AbstractTransform<SchedulerNotification, NotificationCustomerDto> {

    public NotificationCustomerDto toDto(SchedulerNotification sn) {
        return new NotificationCustomerDto(
        		sn.getId(),
        		sn.getCreateTime(),
        		sn.getModifyTime(),
        		sn.getNotificationDate(),
        		sn.getTmcode(),
        		sn.getCustomerSubName(),
        		sn.getCustomerSubId(),
        		sn.getMessage(),
        		sn.getOwner(),
        		sn.isFinished(),
        		sn.isCancelled()
        );
    }
}
