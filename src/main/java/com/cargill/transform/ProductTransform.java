package com.cargill.transform;

import com.cargill.dto.ProductDto;
import com.cargill.model.Product;
import org.springframework.stereotype.Component;

@Component
public class ProductTransform extends AbstractTransform<Product, ProductDto> {
    @Override
    public ProductDto toDto(Product product) {
        return new ProductDto(
                product.getCode(),
                product.getName(),
                product.getPhase(),
                product.getPriceX(),
                product.getPrice3kg3K(),
                product.getPrice10kg1(),
                product.getPrice20kg2(),
                product.getPrice25kg2point5C(),
                product.getPrice30kg3(),
                product.getPrice30kg3C(),
                product.getPrice50kg5(),
                product.getPrice1000kgJ(),
                product.getPrice500kgJ5()
        );
    }
}
