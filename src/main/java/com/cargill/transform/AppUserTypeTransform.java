package com.cargill.transform;

import com.cargill.dto.AppUserTypeDto;
import com.cargill.model.AppUserType;
import org.springframework.stereotype.Component;

@Component
public class AppUserTypeTransform extends AbstractTransform<AppUserType, AppUserTypeDto>{

    @Override
    public AppUserTypeDto toDto(AppUserType type) {
        return new AppUserTypeDto(
                type.getCode(),
                type.getName()
        );
    }
}
