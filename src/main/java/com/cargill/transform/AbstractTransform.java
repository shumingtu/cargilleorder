package com.cargill.transform;

import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractTransform<ENTITY, DTO> {

    public abstract DTO toDto(ENTITY entity);

    public List<DTO> toDtos(List<ENTITY> entities) {
        return entities.stream().map(this::toDto).collect(Collectors.toList());
    }
}
