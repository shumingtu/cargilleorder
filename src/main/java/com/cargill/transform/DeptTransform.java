package com.cargill.transform;

import com.cargill.dto.DeptDto;
import com.cargill.model.Dept;
import org.springframework.stereotype.Component;

@Component
public class DeptTransform extends AbstractTransform<Dept, DeptDto> {
    @Override
    public DeptDto toDto(Dept dept) {
        return new DeptDto(
                dept.getCode(),
                dept.getName()
        );
    }
}
