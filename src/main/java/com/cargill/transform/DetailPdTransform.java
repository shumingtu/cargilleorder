package com.cargill.transform;

import com.cargill.dto.DetailPdDto;
import com.cargill.model.parameter.order.DetailPD;
import org.springframework.stereotype.Component;

@Component
public class DetailPdTransform extends AbstractTransform<DetailPD, DetailPdDto> {

    @Override
    public DetailPdDto toDto(DetailPD pd) {
        return new DetailPdDto(
                pd.getId(),
                false,
                pd.getCreateDate(),
                pd.getNote()
        );
    }
}
