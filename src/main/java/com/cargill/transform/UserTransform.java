package com.cargill.transform;

import com.cargill.dto.WebAccountDto;
import com.cargill.model.User;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.text.SimpleDateFormat;

@Component
public class UserTransform extends AbstractTransform<User, WebAccountDto> {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");

    @Override
    public WebAccountDto toDto(User user) {
        return new WebAccountDto(
                user.getId(),
                user.getAccount(),
                user.getUserName(),
                user.getDept(),
                user.getActCode(),
                user.getLastActCodeTime(),
                user.getStatus(),
                user.getLastLoginTime(),
                sdf.format(user.getLastActCodeTime() == null ? Calendar.getInstance().getTime() : user.getLastActCodeTime().getTime()),
                ""
        );
    }


}
