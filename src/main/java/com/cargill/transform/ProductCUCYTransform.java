package com.cargill.transform;

import com.cargill.dto.ProductCUCYDto;
import com.cargill.model.ProductCUCY;
import org.springframework.stereotype.Component;

@Component
public class ProductCUCYTransform extends AbstractTransform<ProductCUCY, ProductCUCYDto> {
    @Override
    public ProductCUCYDto toDto(ProductCUCY product) {
        return new ProductCUCYDto(
                product.getProductCUCYPK().getCode(),
                product.getProductCUCYPK().getDrugCode(),
                product.getName(),
                product.getDrugName(),
                product.getAmount()
        );
    }
}
