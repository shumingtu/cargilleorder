package com.cargill.transform;

import com.cargill.dto.ViewProductTPDrugDto;
import com.cargill.model.view.ViewProductTPDrug;
import org.springframework.stereotype.Component;

@Component
public class ViewProductTPDrugTransform extends AbstractTransform<ViewProductTPDrug, ViewProductTPDrugDto> {
    @Override
    public ViewProductTPDrugDto toDto(ViewProductTPDrug product) {
        return new ViewProductTPDrugDto(
                product.getCode(),
                product.getName(),
                product.getPrice(),
                product.getAreaCode(),
                product.getAreaType(),
                product.getDrugType(),
                product.getRecommendedAmount(),
                product.getMaxAmount(),
                product.getDrugWithdrawal(),
                product.getNote(),
                product.getTpCode(),
                product.getTpName()
        );
    }
}
