package com.cargill.advice;

import com.cargill.dto.response.ErrorDto;
import com.cargill.exception.CargillApiException;
import com.cargill.exception.CargillException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
@Slf4j
public class ExceptionHandler {
    @org.springframework.web.bind.annotation.ExceptionHandler(CargillApiException.class)
    public ResponseEntity<ErrorDto> handleGoldLuckException(CargillApiException e) {
        log.error(e.getMessage(), e);
        e.printStackTrace();
        return new ResponseEntity<>(new ErrorDto(e.getErrorCode(), e.getMessage()), HttpStatus.EXPECTATION_FAILED);
    }

    @ResponseBody
    @org.springframework.web.bind.annotation.ExceptionHandler(Exception.class)
    public ModelAndView handleException(final Exception e) {
        e.printStackTrace();
        ModelAndView mav = new ModelAndView();

        if (e instanceof CargillException) {
            switch (((CargillException) e).getErrorCode()) {
                case INVALID_ORDER_DATA:
                case INVALID_CUSTOMER:
                    mav.setViewName("error/error_order");
                    break;
                default:
                    mav.setViewName("error/error");
            }
        } else {
            mav.setViewName("error/error");
        }

        return mav;
    }
}
