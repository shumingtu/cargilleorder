package com.cargill.advice;

import com.cargill.dto.WebAccountDto;
import com.cargill.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.Optional;

@ControllerAdvice
@Component
@Scope("session")
public class SessionInfo {
    WebAccountDto loginUser;

    @Autowired
    UserService userService;

    @ModelAttribute
    public void getCurrentUser(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Optional.ofNullable(auth).ifPresent((it) -> {
            if (!"anonymousUser".equals(auth.getName())) {
                model.addAttribute("loginUser",
                        Optional.ofNullable(loginUser).orElseGet(() -> userService.getWebAccountById(auth.getName())));
            }
        });
    }
}
