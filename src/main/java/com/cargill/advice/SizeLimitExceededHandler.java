package com.cargill.advice;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
@Slf4j
public class SizeLimitExceededHandler {
    @ResponseBody
    @ExceptionHandler({MultipartException.class})
    public ModelAndView handleSizeLimitExceeded() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("error/error_upload");
        return mav;
    }
}
