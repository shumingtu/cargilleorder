package com.cargill.util;

import java.util.UUID;

public final class UUIDUtil {

	public static String generateNewUUID() {
		return UUID.randomUUID().toString();
	}
	
}
