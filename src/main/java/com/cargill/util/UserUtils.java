package com.cargill.util;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.cargill.dto.WebAccountDto;
import com.cargill.service.UserService;

@Component
public class UserUtils {

	@Autowired
    private UserService userService;
	
	private static UserService userHolder;
    
    public static void setUserService(UserService userService) {
		UserUtils.userHolder = userService;
	}
    
    @PostConstruct
    public void init() {
    	UserUtils.setUserService(userService);
    }
    
    public static WebAccountDto getUserInfo() {
    	String loginUserId = SecurityContextHolder.getContext().getAuthentication().getName();
    	return userHolder.getWebAccountById(loginUserId);
    }
}
