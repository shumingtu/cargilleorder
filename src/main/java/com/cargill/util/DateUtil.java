package com.cargill.util;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * 日期工具
 * @author Mike
 *
 */
public final class DateUtil {
	
	/** 格式: yyyy-MM-dd */
	public static final DateTimeFormatter DashDateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	
	/**
	 * 取得今天日期
	 * @return
	 */
	public static Date now() {
		return Date.from(LocalDate.now().atTime(0, 0, 0).atZone(ZoneId.systemDefault()).toInstant());
	}
	
	/**
	 * 從今天往前推算一個月
	 * @return {@code java.util.Date}
	 */
	public static Date getBeforeOneMonthByNow() {
		Instant inst = LocalDate.now().minusMonths(1).atStartOfDay()
				.atZone(ZoneId.systemDefault()).toInstant();
		return Date.from(inst);
	}
	
	/**
	 * 從今天往後推算一個月
	 * @return
	 */
	public static Date getAfterOneMonthByNow() {
		Instant inst = LocalDate.now().plusMonths(1).atStartOfDay()
				.atZone(ZoneId.systemDefault()).toInstant();
		return Date.from(inst);
	}
	
	/**
	 * 取得今天日期當月的第一天
	 * @return
	 */
	public static Date getFirstDateByNow() {
		Instant inst = LocalDate.now().with(TemporalAdjusters.firstDayOfMonth()).atTime(LocalTime.MIN)
		.atZone(ZoneId.systemDefault()).toInstant();
		return Date.from(inst);
	}
	
	/**
	 * 取得今天日期當用的最後一天
	 * @return
	 */
	public static Date getLastDateByNow() {
		Instant inst = LocalDate.now().with(TemporalAdjusters.lastDayOfMonth()).atTime(LocalTime.MAX)
		.atZone(ZoneId.systemDefault()).toInstant();
		return Date.from(inst);
	}
	
	/**
	 * 將{@code java.util.Calendar}物件轉為{@code java.time.LocalDateTime}
	 * @param calendar
	 * @return
	 */
	public static LocalDateTime convertToLocalDateTime(Calendar calendar) {
		return LocalDateTime.ofInstant(calendar.toInstant(), ZoneId.systemDefault());
	}
	
	/**
	 * 將{@code java.util.Calendar}物件轉為{@code java.time.LocalDate}
	 * @param calendar
	 * @return
	 */
	public static LocalDate convertToLocalDate(Calendar calendar) {
		return convertToLocalDateTime(calendar).toLocalDate();
	}
	
	/**
	 * 將{@code java.util.Date}物件轉為{@code java.time.LocalDate}
	 * @param date
	 * @return
	 */
	public static LocalDate convertToLocalDate(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return convertToLocalDateTime(calendar).toLocalDate();
	}
	
	/**
	 * 將{@code java.time.LocalDate}物件轉為{@code java.util.Calendar}
	 * @param localDate
	 * @return
	 */
	public static Calendar convert(LocalDate localDate) {
		return GregorianCalendar.from(localDate.atStartOfDay(ZoneId.systemDefault()));
	}
	
}
