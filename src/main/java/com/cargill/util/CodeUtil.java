package com.cargill.util;

import java.security.SecureRandom;
import java.util.Calendar;
import java.util.UUID;

import org.joda.time.DateTime;

import com.cargill.Constants;
import com.google.common.base.Splitter;

public class CodeUtil {
    private static final String NUMBERS = "0123456789";

    private static String generateRandomCode(Integer length) {
        StringBuilder sb = new StringBuilder(Constants.DEFAULT_CODE_LENGTH);
        for (int i = 0; i < length; i++)
            sb.append(NUMBERS.charAt((new SecureRandom()).nextInt(NUMBERS.length())));
        return sb.toString();
    }

    public static String generateRandomCode() {
        return generateRandomCode(Constants.DEFAULT_CODE_LENGTH);
    }

    public static String generateSMSCode() {
        return generateRandomCode(Constants.DEFAULT_SMS_CODE_LENGTH);
    }

    public static String generateMarketingFileName() {
        return Calendar.getInstance().getTimeInMillis() + "-" + generateRandomCode(5);
    }

    public static String generateOrderNo() {
    	/* <20190527> FIXED: 訂單單號重覆 start */
    	// 由 YYYYMMddHHmmssSS 改為 YYYYMMddHHmmssSS_UUID[0]
//        return DateTime.now().toString("YYYYMMddHHmmssSS");
    	Splitter splitter = Splitter.on("-");
    	StringBuilder builder = new StringBuilder();
    	builder.append(DateTime.now().toString("YYYYMMddHHmmssSS"));
    	builder.append("_");
    	builder.append(splitter.splitToList(UUIDUtil.generateNewUUID()).get(0));
    	return builder.toString();
        /* <20190527> FIXED: 訂單單號重覆 end */
    }
    
    public static String generateTpNo() {
//        return DateTime.now().toString("HHmmsss");
        return UUID.randomUUID().toString();
    }

    public static String generatePdNo() {
        return DateTime.now().toString("HHmmsss");
    }
    
    public static void main(String[] args) {
    	System.out.println(CodeUtil.generateTpNo());
    }
}
