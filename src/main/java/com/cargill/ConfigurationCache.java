package com.cargill;

import com.cargill.service.DeptService;
import com.cargill.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

@EnableCaching(proxyTargetClass = true)
@Configuration
public class ConfigurationCache {

    @Autowired
    private DeptService deptService;

    @EventListener(ApplicationReadyEvent.class)
    public void prepareDeptCache(ApplicationReadyEvent event) {
        deptService.getAllDepts();
    }
}
