package com.cargill;

import com.cargill.properties.StorageMarketingProperties;
import com.cargill.service.MarketingFileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

@Configuration
@EnableConfigurationProperties(StorageMarketingProperties.class)
@Slf4j
public class ConfigurationStorage {

    @Autowired
    private MarketingFileService marketingFileService;

    @EventListener(ApplicationReadyEvent.class)
    public void initStorageService(ApplicationReadyEvent event) {
        marketingFileService.init();
    }
}
