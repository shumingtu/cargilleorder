package com.cargill.repository;

import com.cargill.model.TmcodeGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TmcodeGroupRepository extends JpaRepository<TmcodeGroup, String>, JpaSpecificationExecutor<TmcodeGroup> {

    List<TmcodeGroup> findByGroupName(String groupName);
}
