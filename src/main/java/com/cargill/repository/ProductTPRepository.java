package com.cargill.repository;

import com.cargill.model.ProductTP;
import com.cargill.model.ProductTPDrug;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;


@Repository
public interface ProductTPRepository extends JpaRepository<ProductTP, String>, JpaSpecificationExecutor<ProductTP> {
}
