package com.cargill.repository;

import com.cargill.model.ProductCUCY;
import com.cargill.model.ProductCUCYPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductCUCYRepository extends JpaRepository<ProductCUCY, ProductCUCYPK>, JpaSpecificationExecutor<ProductCUCY> {

    List<ProductCUCY> findByProductCUCYPK_CodeIn(List<String> code);

}
