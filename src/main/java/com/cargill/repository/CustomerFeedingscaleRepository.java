package com.cargill.repository;

import com.cargill.model.CustomerFeedingscale;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerFeedingscaleRepository extends JpaRepository<CustomerFeedingscale, String>, JpaSpecificationExecutor<CustomerFeedingscale> {

    CustomerFeedingscale findByCustomerId(String customerId);
}
