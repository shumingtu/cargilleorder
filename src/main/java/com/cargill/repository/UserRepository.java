package com.cargill.repository;

import com.cargill.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, BigInteger>, JpaSpecificationExecutor<User> {
    User findById(BigInteger id);

    User findByAccount(String account);

    User findByAccountAndStatus(String account, String status);

    Page<User> findByAccountIgnoreCaseContainingAndUserNameIgnoreCaseContainingAndDeptIn(String account, String userName, List<String> dept_ids, Pageable pageable);
    Page<User> findByAccountIgnoreCaseContainingAndUserNameIgnoreCaseContaining(String account, String userName, Pageable pageable);

    BigInteger deleteByAccount(String account);

    //Page<User> findByCondition(Pageable pageable);


}
