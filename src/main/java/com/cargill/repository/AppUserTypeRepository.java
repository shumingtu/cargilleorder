package com.cargill.repository;

import com.cargill.model.AppUserType;
import com.cargill.model.Dept;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AppUserTypeRepository extends JpaRepository<AppUserType, String>, JpaSpecificationExecutor<AppUserType> {

    List<Dept> findByNameIgnoreCaseContaining(String name);
}
