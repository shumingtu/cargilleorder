package com.cargill.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.cargill.model.Customer;
import com.cargill.model.CustomerPK;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, CustomerPK>, JpaSpecificationExecutor<Customer> {

    List<Customer> findByCustomerPk_CustomerSubId(String sub_id);
    
    List<Customer> findByCustomerPk_CustomerSubIdIn(List<String> sub_ids);

    Customer findByCustomerPk_CustomerMainIdAndCustomerPk_CustomerSubId(String mainId,String subId);

    @Query(value = "select c from Customer c where (c.customerPk.customerSubId like CONCAT('%',:customerId,'%') or c.customerPk.customerMainId like CONCAT('%',:customerId,'%')) and (c.customerSubName like CONCAT('%',:customerName,'%') or c.customerMainName like CONCAT('%',:customerName,'%')) and c.tmcode=:tmcode")
    Page<Customer> queryByConditionWithTmcodeToPage(@Param("customerId") String customerId, @Param("customerName") String customerName, @Param("tmcode") String tmcode, Pageable pageable);

    @Query(value = "select c from Customer c where (c.customerPk.customerSubId like CONCAT('%',:customerId,'%') or c.customerPk.customerMainId like CONCAT('%',:customerId,'%')) and (c.customerSubName like CONCAT('%',:customerName,'%') or c.customerMainName like CONCAT('%',:customerName,'%')) ")
    Page<Customer> queryByConditionWithoutTmcodeToPage(@Param("customerId") String customerId, @Param("customerName") String customerName, Pageable pageable);

    @Query(value = "select c from Customer c where c.customerPk.customerSubId like CONCAT('%',:customerId,'%') " +
            "and c.customerSubName like CONCAT('%',:customerName,'%')  and c.tmcode=:tmcode order by c.customerPk.customerSubId asc, c.customerSubName asc, c.customerPk.addressShipTo asc")
    Page<Customer> queryByConditionWithoutCustomerMainIdWithTmcodeToPage(@Param("customerId") String customerId, @Param("customerName") String customerName, @Param("tmcode") String tmcode, Pageable pageable);

    @Query(value = "select c from Customer c where c.customerPk.customerSubId like CONCAT('%',:customerId,'%') " +
            "and c.customerSubName like CONCAT('%',:customerName,'%')  order by c.customerPk.customerSubId asc, c.customerSubName asc, c.customerPk.addressShipTo asc")
    Page<Customer> queryByConditionWithoutCustomerMainIdWithoutTmcodeToPage(@Param("customerId") String customerId, @Param("customerName") String customerName, Pageable pageable);


}
