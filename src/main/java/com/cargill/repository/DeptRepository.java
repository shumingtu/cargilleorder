package com.cargill.repository;

import com.cargill.model.Dept;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DeptRepository extends JpaRepository<Dept, String>, JpaSpecificationExecutor<Dept> {

    List<Dept> findByNameIgnoreCaseContaining(String name);
}
