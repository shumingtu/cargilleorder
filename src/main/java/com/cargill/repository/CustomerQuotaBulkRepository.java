package com.cargill.repository;

import com.cargill.model.CustomerQuota;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class CustomerQuotaBulkRepository extends SimpleJpaRepository<CustomerQuota, String> {

    private EntityManager entityManager;

    public CustomerQuotaBulkRepository(EntityManager entityManager) {
        super(CustomerQuota.class, entityManager);
        this.entityManager = entityManager;
    }

    @Transactional
    public void save(List<CustomerQuota> items) {
        items.forEach(item -> entityManager.persist(item));
    }
}
