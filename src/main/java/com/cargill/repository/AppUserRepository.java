package com.cargill.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.cargill.model.AppUser;

@Repository
public interface AppUserRepository extends JpaRepository<AppUser, BigInteger>, JpaSpecificationExecutor<AppUser> {

    Page<AppUser> findAll(Pageable pageable);

    AppUser findById(BigInteger id);

    AppUser findByUnicode(String unicode);

    BigInteger deleteByUnicode(String unicode);

    Page<AppUser> findByUnicodeIgnoreCaseContainingAndUserNameIgnoreCaseContainingAndTypeIn(String unicode, String userName, List<String> type_ids, Pageable pageable);
    Page<AppUser> findByUnicodeIgnoreCaseContainingAndUserNameIgnoreCaseContaining(String unicode, String userName, Pageable pageable);

    List<AppUser> findByUnicodeIn(List<String> unicodes);
}
