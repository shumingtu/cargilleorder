package com.cargill.repository;


import com.cargill.model.ProductTPDrug;
import com.cargill.model.parameter.TPAreaCode;
import com.cargill.model.parameter.TPAreaType;
import com.cargill.model.parameter.TPDrugType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface ProductTPDrugRepository extends JpaRepository<ProductTPDrug, String>, JpaSpecificationExecutor<ProductTPDrug> {

    @Transactional
    @Modifying
    @Query(value = "delete from ProductTPDrug tpd where tpd.areaType=:#{#areaType} and tpd.drugType=:#{#drugType} ")
    void deleteByAreaAndDrug(@Param("areaType") TPAreaType areaType, @Param("drugType") TPDrugType drugType);

    @Transactional
    @Modifying
    @Query(value = "delete from ProductTPDrug tpd where tpd.areaType=:#{#areaType} and tpd.drugType=:#{#drugType} and tpd.areaCode=:#{#areaCode} ")
    void deleteByAllType(@Param("areaType") TPAreaType areaType, @Param("drugType") TPDrugType drugType, @Param("areaCode") TPAreaCode areaCode);
}
