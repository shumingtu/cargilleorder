package com.cargill.repository;


import com.cargill.model.DrugInventory;
import com.cargill.model.parameter.TPAreaCode;
import com.cargill.model.parameter.TPAreaType;
import com.cargill.model.parameter.TPDrugType;
import com.cargill.model.view.ViewProductTPDrug;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.List;

@Repository
public interface DrugInventoryRepository extends JpaRepository<DrugInventory, String>, JpaSpecificationExecutor<DrugInventory> {

    List<DrugInventory> findByStatus(String status);

    DrugInventory findByCustomerSubIdAndCodeAndStatus(String customerSubId, String code, String status);

    @Transactional
    @Modifying
    @Query(value = "update  DrugInventory di set di.createTime =now()   where di.status = '1' ")
    void updateCreateDateByStatusOne( );



}
