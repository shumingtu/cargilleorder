package com.cargill.repository;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;

import com.cargill.model.Customer;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.cargill.model.CustomerOrder;

@Repository
public interface CustomerOrderRepository extends CrudRepository<CustomerOrder, BigInteger>, JpaSpecificationExecutor<CustomerOrder> {

    CustomerOrder findByOrderNo(String order_no);

    List<CustomerOrder> findByDeliveryDateBetween(Calendar start, Calendar end);
    
    Page<CustomerOrder> findAllByOrderByIdDesc(Specification<T> spec, Pageable pageable);
    
    //Page<CustomerOrder> findByAppAndTmcodes(Specification<CustomerOrder> spec, Pageable pageable);
    
    @Query(value = "select tmcode from CustomerOrder group by tmcode")
    List<String> findDistinctTmcodeBy();

    List<CustomerOrder> findByCustomerSubIdAndTmcodeAndCreateTimeAfter(String customerSubId,String tmcode, Calendar createtime);

    List<CustomerOrder> findByCustomerSubIdAndCreateTimeAfterAndDetailsLike(String customerSubId, Calendar createtime, String details);

}
