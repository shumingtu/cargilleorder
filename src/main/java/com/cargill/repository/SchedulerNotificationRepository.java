package com.cargill.repository;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cargill.model.SchedulerNotification;

@Repository
public interface SchedulerNotificationRepository extends JpaRepository<SchedulerNotification, BigInteger> {

	public Page<SchedulerNotification> findAllByOrderByNotificationDateDesc(Pageable pageable);
	
	public List<SchedulerNotification> findByNotificationDateAndFinishedAndCancelled(Date notificationDate, boolean finished, boolean cancelled);
	
	public List<SchedulerNotification> findByNotificationDateBetween(Date start, Date end);
}
