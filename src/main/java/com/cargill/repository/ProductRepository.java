package com.cargill.repository;

import com.cargill.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import javax.annotation.Nullable;
import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, String>, JpaSpecificationExecutor<Product> {

	List<Product> findByCodeStartingWith(String code);

	List<Product> findByNameContaining(String name);

	Product findByCode(String code);

}
