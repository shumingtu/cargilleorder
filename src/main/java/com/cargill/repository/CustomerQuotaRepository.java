package com.cargill.repository;

import com.cargill.model.CustomerQuota;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerQuotaRepository extends JpaRepository<CustomerQuota, String>, JpaSpecificationExecutor<CustomerQuota> {

    CustomerQuota findByCustomerId(String customerId);
}
