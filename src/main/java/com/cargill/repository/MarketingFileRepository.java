package com.cargill.repository;

import com.cargill.model.MarketingFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface MarketingFileRepository extends JpaRepository<MarketingFile, String>, JpaSpecificationExecutor<MarketingFile> {
}
