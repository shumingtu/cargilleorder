package com.cargill.repository;


import com.cargill.model.parameter.TPAreaType;
import com.cargill.model.parameter.TPDrugType;
import com.cargill.model.view.ViewProductTPDrug;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ViewProductTPDrugRepository extends JpaRepository<ViewProductTPDrug, String> {
    List<ViewProductTPDrug> findByCodeStartingWithAndDrugType(String code, TPDrugType type);

	List<ViewProductTPDrug> findByNameContainingAndDrugType(String name, TPDrugType type);
	//這個查詢回傳不只一個條件
	ViewProductTPDrug findByCodeAndDrugType(String code, TPDrugType type);

	ViewProductTPDrug findByCodeAndDrugTypeAndAreaType(String code, TPDrugType type, TPAreaType areaType);


}
