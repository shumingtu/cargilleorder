package com.cargill;

import com.cargill.properties.SmsProperties;
import com.cargill.properties.StorageMarketingProperties;
import com.cargill.service.MarketingFileService;
import com.cargill.service.SmsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

@Configuration
@EnableConfigurationProperties(SmsProperties.class)
@Slf4j
public class ConfigurationSms {

    @Autowired
    private SmsService smsService;
}
