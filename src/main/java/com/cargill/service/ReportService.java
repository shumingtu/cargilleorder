package com.cargill.service;

import com.cargill.dto.request.ReportRequest;
import org.apache.poi.ss.usermodel.Workbook;

public interface ReportService {

    Workbook generateReport1(ReportRequest request);

    Workbook generateReport2(ReportRequest request);
    
    /* <201905> Add: 新增App訂單統計 start */
    /**
     * 產生App訂單統計
     * @param request
     * @return
     */
    Workbook generateReport3(ReportRequest request);
    /* <201905> Add: 新增App訂單統計 end */

}
