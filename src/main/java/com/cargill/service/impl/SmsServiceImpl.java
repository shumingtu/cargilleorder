package com.cargill.service.impl;

import com.cargill.exception.CargillException;
import com.cargill.exception.ErrorCode;
import com.cargill.model.parameter.SmsSubmitReq;
import com.cargill.model.parameter.SubmitRes;
import com.cargill.properties.SmsProperties;
import com.cargill.service.SmsService;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;
import java.net.URI;
import java.util.Base64;
import java.util.Optional;

@Service
@Slf4j
public class SmsServiceImpl implements SmsService {

    @Autowired
    private SmsProperties smsProperties;

    @Autowired
    private XmlMapper xmlMapper;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public String sendSms(String user_phone, String message) throws CargillException {
        if (!smsProperties.isRealExecute()) {
            log.warn("=== Sms message will not send to user (" + user_phone + ") on development mode: " + message);
            return "";
        }

        String big5Message = message;
        try {

            UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.newInstance()
                    .queryParam("xml", getXmlString(new SmsSubmitReq(
                            smsProperties.getSysid(),
                            smsProperties.getAddress(),
                            user_phone,
                            Base64.getEncoder().encodeToString(message.getBytes("UTF-8")),
                            false,
                            false)));

            ResponseEntity<String> responseEntity = restTemplate.postForEntity(
                    getEndpointUri(),
                    uriComponentsBuilder.build().getQueryParams(),
                    String.class
            );

            if (responseEntity.getStatusCode() != HttpStatus.OK) {
                throw new CargillException(ErrorCode.SMS_ERROR, responseEntity.getStatusCode().toString());
            }

            SubmitRes submitRes = xmlMapper.readValue(responseEntity.getBody(), SubmitRes.class);
            return Optional.of(submitRes)
                    .filter((it) ->
                            "00000".equals(submitRes.getResultCode()) ||
                                    "00010".equals(submitRes.getResultCode()))
                    .map((it) -> submitRes.getMessageId())
                    .orElseThrow(() -> new CargillException(ErrorCode.SMS_ERROR, "Invalid ResultCode in response: " + responseEntity.getBody()));
        } catch (Exception e) {
            log.error("=== sending sms failed: " + e.getMessage());
            e.printStackTrace();
            throw new CargillException(ErrorCode.SMS_ERROR, e.getMessage());
        }
    }

    private URI getEndpointUri() {
        return URI.create("http://" + smsProperties.getHost() + ":" + smsProperties.getPort() + smsProperties.getEndpointPath());
    }

    private String getXmlString(SmsSubmitReq request) {
        try {
            Marshaller marshaller = JAXBContext.newInstance(SmsSubmitReq.class).createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);

            StringWriter sw = new StringWriter();
            marshaller.marshal(request, sw);

            return sw.toString().trim();
        } catch (Exception e) {
            log.error("=== serializing xml failed: " + e.getMessage());
            e.printStackTrace();

            return "";
        }
    }
}
