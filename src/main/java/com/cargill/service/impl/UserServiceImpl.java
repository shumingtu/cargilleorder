package com.cargill.service.impl;

import com.cargill.dto.WebAccountDto;
import com.cargill.dto.request.WebAccountRequest;
import com.cargill.exception.CargillException;
import com.cargill.exception.ErrorCode;
import com.cargill.model.User;
import com.cargill.repository.UserRepository;
import com.cargill.service.UserService;
import com.cargill.transform.UserTransform;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
@Slf4j
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserTransform userTransform;

    @Override
    public WebAccountDto getWebAccount(BigInteger id) {
        User user = Optional.ofNullable(userRepository.findById(id))
                .orElseThrow(() -> new CargillException(ErrorCode.NOT_FOUND));
        return userTransform.toDto(user);
    }

    @Override
    public WebAccountDto getWebAccountByAccount(String account) {
        User user = Optional.ofNullable(userRepository.findByAccount(account))
                .orElseThrow(() -> new CargillException(ErrorCode.NOT_FOUND));
        return userTransform.toDto(user);
    }

    @Override
    public WebAccountDto getWebAccountById(String user_id) {
        User user = Optional.ofNullable(userRepository.getOne(new BigInteger(user_id)))
                .orElseThrow(() -> new CargillException(ErrorCode.NOT_FOUND));
        return userTransform.toDto(user);
    }

    @Override
    public Page<WebAccountDto> findByCondition(WebAccountRequest condition, List<String> dept_ids, Pageable pageable) {
        return (dept_ids.isEmpty() ?
                userRepository.findByAccountIgnoreCaseContainingAndUserNameIgnoreCaseContaining(condition.getAccount(), condition.getUserName(), pageable)
                :
                userRepository.findByAccountIgnoreCaseContainingAndUserNameIgnoreCaseContainingAndDeptIn(condition.getAccount(), condition.getUserName(), dept_ids, pageable))
                .map(userTransform::toDto);
    }

    @Override
    public void deleteById(BigInteger id) {
        userRepository.delete(id);
    }

    @Override
    public BigInteger deleteByAccount(String account) {
        return userRepository.deleteByAccount(account);
    }

    @Override
    public WebAccountDto insertAccount(WebAccountDto accountDto) {
        User user = new User();
        user.setAccount(accountDto.getAccount());
        user.setDept(accountDto.getDept());
        user.setUserName(accountDto.getUserName());
        user.setStatus(accountDto.getStatus());
        return userTransform.toDto(userRepository.save(user));
    }

    @Override
    public WebAccountDto updateAccount(WebAccountDto accountDto) {
        User user = userRepository.findById(accountDto.getId());
        user.setDept(accountDto.getDept());
        user.setUserName(accountDto.getUserName());
        return userTransform.toDto(userRepository.save(user));
    }

    @Override
    public WebAccountDto updateAccountActData(WebAccountDto accountDto) {
        User user = userRepository.findById(accountDto.getId());
        user.setActCode(accountDto.getActCode());
        user.setLastActCodeTime(accountDto.getLastActCodeTime());
        return userTransform.toDto(userRepository.save(user));
    }

    @Override
    public Boolean isAccountExist(String account) {
        User user = userRepository.findByAccount(account);
        if (null == user) {
            return Boolean.FALSE;
        } else {
            return Boolean.TRUE;
        }

    }
}
