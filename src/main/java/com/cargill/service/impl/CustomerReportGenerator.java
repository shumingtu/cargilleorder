package com.cargill.service.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.cargill.dto.request.ReportRequest;
import com.cargill.model.Customer;
import com.cargill.model.CustomerOrder;

@Service
public class CustomerReportGenerator extends AbstractAppOrderReportGenerator {
	
	@Override
	public boolean supports(ReportRequest request) {
		return "opt3".equals(request.getType()) 
				&& "customer".equalsIgnoreCase(request.getOrderType());
	}

	@Override
	public Map<String, List<CustomerOrder>> pile(List<CustomerOrder> orders) {
		return orders.stream().collect(Collectors.groupingBy(CustomerOrder::getCustomerSubId));
	}

	@Override
	protected void build(
			Workbook workbook, 
			Map<String, Customer> customerMap, 
			List<CustomerOrder> orders) {
		throw new UnsupportedOperationException();
	}

	@Override
	protected void build(
			Workbook workbook, 
			Map<String, Customer> customerMap, 
			List<CustomerOrder> orders,
			Map<String, List<CustomerOrder>> piles) {
		CellStyle cellStyle = workbook.createCellStyle();
	    DataFormat df = workbook.createDataFormat();
	    cellStyle.setDataFormat(df.getFormat("0.00%"));  
		Sheet sheet = workbook.getSheetAt(0);
		for (Map.Entry<String, List<CustomerOrder>> entrySet : piles.entrySet()) {
			Row row = sheet.createRow(1 + sheet.getLastRowNum());
			String customerSubId = entrySet.getKey();
			CustomerOrder order = entrySet.getValue().get(0);
			row.createCell(0).setCellValue(Optional.of(order.getDeliveryDate().getTime()).map(SDF_DATE::format).orElse(""));
			row.createCell(1).setCellValue(order.getCustomerSubName());
			row.createCell(2).setCellValue(customerSubId);
			row.createCell(3).setCellValue(order.getTmcode());
			Customer customer = customerMap.get(order.getCustomerSubId());
			if (customer != null) {
				// sscode
				row.createCell(4).setCellValue(customer.getSscode());
				// 畜種
				row.createCell(5).setCellValue(customer.getLivestock());
			}
			BigDecimal totalCount = new BigDecimal(this.getTotalCount(entrySet.getValue()));
			BigDecimal appCount = new BigDecimal(this.getAppCount(entrySet.getValue()));
			row.createCell(6).setCellValue(totalCount.longValue());
			row.createCell(7).setCellValue(this.getWebCount(entrySet.getValue()));
			row.createCell(8).setCellValue(appCount.longValue());
			row.createCell(9).setCellValue(this.getCustomerOrderCount(entrySet.getValue()));
			row.createCell(10).setCellValue(this.getBusinessOrderCount(entrySet.getValue()));
			BigDecimal avg = appCount.divide(totalCount, 4, BigDecimal.ROUND_HALF_UP);
			Cell cell = row.createCell(11);
			cell.setCellStyle(cellStyle);
			cell.setCellValue(avg.doubleValue());
		}
	}

	private long getCustomerOrderCount(List<CustomerOrder> orders) {
		return this.getClientOrder(orders).stream()
				.filter(o -> getTmcodeInOrderPerson(o.getOrderPerson()).equals(o.getTmcode()) == false)
				.count();
	}

	private long getBusinessOrderCount(List<CustomerOrder> orders) {
		return this.getClientOrder(orders).stream()
				.filter(o -> getTmcodeInOrderPerson(o.getOrderPerson()).equals(o.getTmcode()))
				.count();
	}
	
	private String getTmcodeInOrderPerson(String orderPersion) {
		Pattern p = Pattern.compile("\\d+");
		Matcher m = p.matcher(orderPersion);
		while (m.find()) {
			return m.group();
		}
		return StringUtils.EMPTY;
	}

}
