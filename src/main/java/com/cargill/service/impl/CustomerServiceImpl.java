package com.cargill.service.impl;

import com.cargill.dto.CustomerDto;
import com.cargill.dto.CustomerFeedingscaleDto;
import com.cargill.dto.CustomerQuotaDto;
import com.cargill.dto.request.CustomerMemoRequest;
import com.cargill.dto.request.SearchCustomerRequest;
import com.cargill.exception.CargillException;
import com.cargill.exception.ErrorCode;
import com.cargill.model.Customer;
import com.cargill.repository.CustomerFeedingscaleRepository;
import com.cargill.repository.CustomerQuotaRepository;
import com.cargill.repository.CustomerRepository;
import com.cargill.service.CustomerService;
import com.cargill.transform.CustomerFeedingsacleTransform;
import com.cargill.transform.CustomerListTransform;
import com.cargill.transform.CustomerQuotaTransform;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
@Slf4j
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private CustomerQuotaRepository customerQuotaRepository;
    @Autowired
    private CustomerFeedingscaleRepository customerFeedingscaleRepository;
    @Autowired
    private CustomerListTransform customerListTransform;
    @Autowired
    private CustomerQuotaTransform customerQuotaTransform;
    @Autowired
    private CustomerFeedingsacleTransform customerFeedingsacleTransform;

    @Override
    public List<CustomerDto> findBySubId(String sub_id) {
        return Optional.ofNullable(customerRepository.findByCustomerPk_CustomerSubId(sub_id))
                .map(customerListTransform::toDtos)
                .orElseThrow(() -> new CargillException(ErrorCode.INVALID_CUSTOMER));
    }

    @Override
    public CustomerFeedingscaleDto findFeedingScaleBySubId(String sub_id) {
        return Optional.ofNullable(customerFeedingscaleRepository.findByCustomerId(sub_id))
                .map(customerFeedingsacleTransform::toDto)
                .orElseGet(CustomerFeedingscaleDto::new);
    }

    @Override
    public CustomerQuotaDto getQuota(String sub_id) {
        return Optional.ofNullable(sub_id)
                .map(it -> customerQuotaRepository.findByCustomerId(sub_id))
                .map(customerQuotaTransform::toDto)
                .orElseGet(CustomerQuotaDto::new);
    }

    @Override
    public Page<CustomerDto> findByCondition(SearchCustomerRequest condition, Pageable pageable) {

        Page<CustomerDto> customerPage = (condition.getTmcode().equals("")
                ? customerRepository.queryByConditionWithoutTmcodeToPage(condition.getCustomerId(), condition.getCustomerName(), pageable)
                : customerRepository.queryByConditionWithTmcodeToPage(condition.getCustomerId(), condition.getCustomerName(), condition.getTmcode(), pageable)
        ).map(customerListTransform::toDto);

        List<CustomerDto> cusotomerList = customerPage.getContent();
        List<String> subIdList = cusotomerList.stream().map(
                CustomerDto::getCustomerSubId).collect(Collectors.toList());

        for (String subId : subIdList) {
            CustomerQuotaDto customerQuota = Optional.ofNullable(customerQuotaRepository.findByCustomerId(subId))
                    .map(customerQuotaTransform::toDto)
                    .orElseGet(CustomerQuotaDto::new);
            Optional.ofNullable(customerQuota.getNote())
                    .ifPresent(note -> customerQuota.setNote(note.replaceAll("\\R", "<br />")));


            CustomerFeedingscaleDto customerFeedingscale = Optional.ofNullable(customerFeedingscaleRepository.findByCustomerId(subId))
                    .map(customerFeedingsacleTransform::toDto)
                    .orElseGet(CustomerFeedingscaleDto::new);

            Optional.ofNullable(customerFeedingscale.getNote()).ifPresent(note ->
                    customerFeedingscale.setNote(note.replaceAll("\\R", "<br />")));

            cusotomerList.stream().filter((a) ->
                    a.getCustomerSubId().equals(subId)).map((b) -> {
                b.setCustomerQuota(customerQuota);
                b.setCustomerFeedingscale(customerFeedingscale);
                return b;
            }).collect(Collectors.toList());
        }
        return customerPage;
    }

    @Override
    public Page<CustomerDto> findByConditionwWithoutCustomerMainId(SearchCustomerRequest condition, Pageable pageable) {

        Page<CustomerDto> customerPage = (condition.getTmcode().equals("")
                ? customerRepository.queryByConditionWithoutCustomerMainIdWithoutTmcodeToPage(condition.getCustomerId(), condition.getCustomerName(), pageable)
                : customerRepository.queryByConditionWithoutCustomerMainIdWithTmcodeToPage(condition.getCustomerId(), condition.getCustomerName(), condition.getTmcode(), pageable)
        ).map(customerListTransform::toDto);
        List<CustomerDto> cusotomerList = customerPage.getContent();
        List<String> subIdList = cusotomerList.stream().map(
                (d) -> d.getCustomerSubId()).collect(Collectors.toList());
        for (String subId : subIdList) {
            CustomerQuotaDto customerQuota = Optional.ofNullable(customerQuotaRepository.findByCustomerId(subId))
                    .map(customerQuotaTransform::toDto)
                    .orElseGet(CustomerQuotaDto::new);
            CustomerFeedingscaleDto customerFeedingscale = Optional.ofNullable(customerFeedingscaleRepository.findByCustomerId(subId))
                    .map(customerFeedingsacleTransform::toDto)
                    .orElseGet(CustomerFeedingscaleDto::new);
            cusotomerList.stream().filter((a) ->
                    a.getCustomerSubId().equals(subId)).map((b) ->
            {
                b.setCustomerQuota(customerQuota);
                b.setCustomerFeedingscale(customerFeedingscale);
                return b;
            }).collect(Collectors.toList());
        }
        return customerPage;
    }

    @Override
    public void saveMemo(String customerMainId, String customerSubId, String memoString) {
        System.out.println("CustomerServiceImpl ,saveMemo 136 :"+customerMainId +", "+customerSubId+ ", "+ memoString);
        Customer customer =  customerRepository.findByCustomerPk_CustomerMainIdAndCustomerPk_CustomerSubId(customerMainId,customerSubId);
        customer.setMemo(memoString);
        customerRepository.save(customer);
    }
}
