package com.cargill.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.cargill.dto.request.ReportRequest;
import com.cargill.model.Customer;
import com.cargill.model.CustomerOrder;

@Service
public class PharmacyOrderReportGenerator extends AbstractReportGenerator {
	
	@Override
	public boolean supports(ReportRequest request) {
		return false;
	}
	
	@Override
	protected List<CustomerOrder> getOrders(ReportRequest request) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Map<String, List<CustomerOrder>> pile(List<CustomerOrder> orderList) {
		throw new UnsupportedOperationException();
	}

	@Override
	protected void build(
			Workbook workbook, 
			Map<String, Customer> customerMap, 
			List<CustomerOrder> orderList) {
		throw new UnsupportedOperationException();
	}

	@Override
	protected void build(Workbook workbook, Map<String, Customer> customerMap, List<CustomerOrder> orders,
			Map<String, List<CustomerOrder>> piles) {
		throw new UnsupportedOperationException();
	}

}
