package com.cargill.service.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cargill.dto.request.ReportRequest;
import com.cargill.model.AppUser;
import com.cargill.model.Customer;
import com.cargill.model.CustomerOrder;
import com.cargill.repository.AppUserRepository;

@Service
public class BusinessReportGenerator extends AbstractAppOrderReportGenerator {
	
	private AppUserRepository appUserRepository;
	
	@Autowired
	public void setAppUserRepository(AppUserRepository appUserRepository) {
		this.appUserRepository = appUserRepository;
	}

	@Override
	public boolean supports(ReportRequest request) {
		return "opt3".equals(request.getType()) 
				&& "business".equalsIgnoreCase(request.getOrderType());
	}

	@Override
	public Map<String, List<CustomerOrder>> pile(List<CustomerOrder> orders) {
		List<String> tmcodes = this.getTmcodesByOrders(orders);
		List<AppUser> appUsers = appUserRepository.findByUnicodeIn(tmcodes);
		Map<String, String> appUserMap = appUsers.parallelStream().collect(Collectors.toMap(AppUser::getUnicode, AppUser::getType));
		return orders.stream()
				.filter(o -> "S".equals(appUserMap.get(o.getTmcode())))
				.collect(Collectors.groupingBy(CustomerOrder::getTmcode));
	}

	@Override
	protected void build(
			Workbook workbook, 
			Map<String, Customer> customerMap, 
			List<CustomerOrder> orders) {
		throw new UnsupportedOperationException();
	}

	@Override
	protected void build(
			Workbook workbook, 
			Map<String, Customer> customerMap, 
			List<CustomerOrder> orders,
			Map<String, List<CustomerOrder>> piles) {
		List<String> tmcodes = this.getTmcodesByOrders(orders);
		Map<String, String> appUserMap = this.getAppUserMap(tmcodes);
		CellStyle cellStyle = workbook.createCellStyle();
	    DataFormat df = workbook.createDataFormat();
	    cellStyle.setDataFormat(df.getFormat("0.00%")); 
		Sheet sheet = workbook.getSheetAt(0);
		for (Map.Entry<String, List<CustomerOrder>> entrySet : piles.entrySet()) {
			Row row = sheet.createRow(1 + sheet.getLastRowNum());
			String tmcode = entrySet.getKey();
			CustomerOrder order = entrySet.getValue().get(0);
			row.createCell(0).setCellValue(Optional.of(order.getDeliveryDate().getTime()).map(SDF_DATE::format).orElse(""));
			row.createCell(1).setCellValue(appUserMap.getOrDefault(tmcode, StringUtils.EMPTY));
			row.createCell(2).setCellValue(tmcode);
			row.createCell(3).setCellValue(tmcode);
			BigDecimal totalCount = new BigDecimal(this.getTotalCount(entrySet.getValue()));
			BigDecimal appCount = new BigDecimal(this.getAppCount(entrySet.getValue()));
			row.createCell(4).setCellValue(totalCount.longValue());
			row.createCell(5).setCellValue(this.getWebCount(entrySet.getValue()));
			row.createCell(6).setCellValue(appCount.longValue());
			BigDecimal avg = appCount.divide(totalCount, 4, BigDecimal.ROUND_HALF_UP);
			Cell cell = row.createCell(7);
			cell.setCellStyle(cellStyle);
			cell.setCellValue(avg.doubleValue());
		}
	}
	
	private List<String> getTmcodesByOrders(List<CustomerOrder> orders) {
		return orders.stream().map(CustomerOrder::getTmcode).collect(Collectors.toList());
	}

	private Map<String, String> getAppUserMap(List<String> tmcodes) {
		return appUserRepository.findByUnicodeIn(tmcodes).stream()
				.collect(Collectors.toMap(AppUser::getUnicode, AppUser::getUserName));
	}
}
