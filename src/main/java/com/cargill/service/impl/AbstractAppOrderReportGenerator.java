package com.cargill.service.impl;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import com.cargill.dto.request.ReportRequest;
import com.cargill.model.CustomerOrder;

public abstract class AbstractAppOrderReportGenerator extends AbstractReportGenerator {

	@Override
	protected List<CustomerOrder> getOrders(ReportRequest request) {
		return customerOrderService.getReportData(request);
	}

	protected long getTotalCount(List<CustomerOrder> orders) {
		if (CollectionUtils.isEmpty(orders)) {
			return 0;
		}
		return orders.size();
	}

	protected long getWebCount(List<CustomerOrder> orders) {
		if (CollectionUtils.isEmpty(orders)) {
			return 0;
		}
		return orders.stream()
		.filter(o -> StringUtils.isBlank(o.getOrderSys()))
		.count();
	}

	protected long getAppCount(List<CustomerOrder> orders) {
		if (CollectionUtils.isEmpty(orders)) {
			return 0;
		}
		return this.getClientOrder(orders).size();
	}
	
	protected List<CustomerOrder> getClientOrder(List<CustomerOrder> orders) {
		if (orders == null) {
			return Collections.emptyList();
		}
		return orders.stream()
				.filter(o -> StringUtils.isNotBlank(o.getOrderSys()))
				.collect(Collectors.toList());
	}
}
