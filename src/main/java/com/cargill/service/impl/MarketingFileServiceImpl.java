package com.cargill.service.impl;

import com.cargill.Constants;
import com.cargill.dto.MarketingFileDto;
import com.cargill.dto.request.MarketingFileUploadRequest;
import com.cargill.exception.CargillException;
import com.cargill.exception.ErrorCode;
import com.cargill.model.MarketingFile;
import com.cargill.properties.StorageMarketingProperties;
import com.cargill.repository.MarketingFileRepository;
import com.cargill.service.MarketingFileService;
import com.cargill.transform.MarketingFileTransform;
import com.cargill.util.CodeUtil;
import com.google.common.collect.Streams;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class MarketingFileServiceImpl implements MarketingFileService {

    @Autowired
    private StorageMarketingProperties storageProperties;

    @Autowired
    private MarketingFileRepository marketingFileRepository;

    private Path rootLocation;

    @Override
    public void init() {
        this.rootLocation = Paths.get(storageProperties.getUploadPath());
        log.info("=== Default fild upload path: " + this.rootLocation);
        if (!Files.exists(this.rootLocation)) {
            try {
                Files.createDirectories(this.rootLocation);
            } catch (IOException e) {
                throw new CargillException(ErrorCode.FILE_PATH_INIT_FAILED);
            }
        }
    }

    public Resource getFile(String file_name) {
        try {
            Resource resource = new UrlResource(this.rootLocation.resolve(file_name).normalize().toUri());
            if (resource.exists()) {
                return resource;
            } else {
                throw new CargillException(ErrorCode.INVALIE_FILE_FOR_DOWNLOADING);
            }
        } catch (MalformedURLException ex) {
            throw new CargillException(ErrorCode.INVALIE_FILE_FOR_DOWNLOADING);
        }
    }

    @Override
    public void saveFile(MarketingFileUploadRequest file_request) throws CargillException {
        MarketingFile marketingFile = Optional.ofNullable(marketingFileRepository
                .findOne("1")).orElseGet(() -> {
            MarketingFile f = new MarketingFile();
            f.setCode("1");
            return f;
        });
        marketingFile.setCarouselSec(Integer.valueOf(file_request.getCarouselSec()));

        Streams.mapWithIndex(file_request.getFiles().stream(), (file, index) -> {
            int currentIndex = Integer.valueOf(index + "");
            boolean doEmpty = file_request.getDoUploads().get(currentIndex).isEmpty();
            String fileName = "";
            String fileNameOrig = "";
            if (!file.isEmpty()) {
                fileName = CodeUtil.generateMarketingFileName();
                fileNameOrig = StringUtils.cleanPath(file.getOriginalFilename());
                if (fileNameOrig.contains("..")) {
                    log.warn("=== Failed to store file: " + fileNameOrig);
                    throw new CargillException(ErrorCode.FILE_UPLOAD_FAILED);
                }

                try {
                    Files.copy(file.getInputStream(), this.rootLocation.resolve(fileName),
                            StandardCopyOption.REPLACE_EXISTING);
                } catch (IOException e) {
                    log.warn("=== Failed to store file: " + fileNameOrig);
                    throw new CargillException(ErrorCode.FILE_UPLOAD_FAILED);
                }
            }

            switch (currentIndex) {
                case 0:
                    if (!file.isEmpty() || doEmpty) {
                        marketingFile.setFileName1(fileName);
                        marketingFile.setOriginalName1(fileNameOrig);
                    }
                    marketingFile.setFileLink1(file_request.getFileLinks().get(0));
                    break;
                case 1:
                    if (!file.isEmpty() || doEmpty) {
                        marketingFile.setFileName2(fileName);
                        marketingFile.setOriginalName2(fileNameOrig);
                    }
                    marketingFile.setFileLink2(file_request.getFileLinks().get(1));
                    break;
                case 2:
                    if (!file.isEmpty() || doEmpty) {
                        marketingFile.setFileName3(fileName);
                        marketingFile.setOriginalName3(fileNameOrig);
                    }
                    marketingFile.setFileLink3(file_request.getFileLinks().get(2));
                    break;
                case 3:
                    if (!file.isEmpty() || doEmpty) {
                        marketingFile.setFileName4(fileName);
                        marketingFile.setOriginalName4(fileNameOrig);
                    }
                    marketingFile.setFileLink4(file_request.getFileLinks().get(3));
                    break;
                case 4:
                    if (!file.isEmpty() || doEmpty) {
                        marketingFile.setFileName5(fileName);
                        marketingFile.setOriginalName5(fileNameOrig);
                    }
                    marketingFile.setFileLink5(file_request.getFileLinks().get(4));
                    break;
                default:
                    break;
            }

            return file;
        }).collect(Collectors.toList());
        marketingFileRepository.save(marketingFile);
    }

    @Override
    public MarketingFileDto getAllFiles() {
        MarketingFile marketingFile = Optional.ofNullable(marketingFileRepository
                .findOne("1")).orElseGet(() -> {
            MarketingFile f = new MarketingFile();
            f.setCarouselSec(Constants.DEFAULT_CAROUSEL_SEC);
            f.setCode("1");
            return f;
        });

        return new MarketingFileTransform().toDto(marketingFile);
    }
}
