package com.cargill.service.impl;

import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;

import com.cargill.dto.request.ReportRequest;
import com.cargill.model.Customer;
import com.cargill.model.CustomerOrder;
import com.cargill.model.parameter.OrderTypeCode;

@Service
public class OrderReportGenerator extends AbstractReportGenerator {
	
	@Override
	public boolean supports(ReportRequest request) {
		return "opt1".equals(request.getType());
	}
	
	@Override
	protected List<CustomerOrder> getOrders(ReportRequest request) {
		return customerOrderRepository.findByDeliveryDateBetween(request.getStartDate(), request.getEndDate());
	}

	@Override
	public Map<String, List<CustomerOrder>> pile(List<CustomerOrder> orderList) {
		return null;
	}

	@Override
	protected void build(
			Workbook workbook, 
			Map<String, Customer> customerMap, 
			List<CustomerOrder> orderList) {
		Sheet sheet = workbook.getSheetAt(0);
		orderList.stream().map(order -> order.getOrderDetailList().stream().map(detail -> {
            Row row = sheet.createRow(1 + sheet.getLastRowNum());
            row.createCell(0).setCellValue(Optional.of(order.getCreateTime().getTime()).map(SDF::format).orElse(""));
            row.createCell(1).setCellValue(Optional.of(order.getDeliveryDate().getTime()).map(SDF::format).orElse(""));
            row.createCell(2).setCellValue(order.getOrderNo());
            row.createCell(3).setCellValue(order.getCustomerSubId());
            row.createCell(4).setCellValue(order.getCustomerSubName());
            row.createCell(5).setCellValue(order.getShipTo());


            row.createCell(6).setCellValue(detail.getCode());
            row.createCell(7).setCellValue(detail.getName());
            row.createCell(8).setCellValue(detail.getUnit());
            row.createCell(9).setCellValue(detail.getCount());
            row.createCell(10).setCellValue(detail.getSpec());
            /* <201905> Add: 擴充SSCode及畜種欄位 start */
            Customer customer = customerMap.get(order.getCustomerSubId());
            if (customer != null) {
            	// sscode
            	row.createCell(11).setCellValue(customer.getSscode());
            	// 畜種
            	row.createCell(12).setCellValue(customer.getLivestock());
				//業務區域
				row.createCell(17).setCellValue(customer.getTmcode());
            }
            /* <201905> Add: 擴充SSCode及畜種欄位 end */
            /* <20190801> Add: 擴充來源、人員 start */
            // 來源
            String orderSys = StringUtils.isBlank(order.getOrderSys()) ? "WEB" : order.getOrderSys();
        	row.createCell(14).setCellValue(orderSys);
        	// 人員
        	row.createCell(15).setCellValue(order.getOrderPerson());
            /* <20190801> Add: 擴充來源、人員 end */
			//訂單狀態
			row.createCell(16).setCellValue(this.getType(order.getType()));
			//運輸車種
			row.createCell(18).setCellValue(detail.getVehicleName());
			//APP
			String appPerson = StringUtils.substringBetween(order.getModifyLogs(),"APP "," ");
			appPerson= StringUtils.substringBefore(appPerson,"(");
			row.createCell(19).setCellValue(appPerson);
			//WEB
			String webPerson = StringUtils.substringBetween(order.getModifyLogs(),"WEB "," ");
			row.createCell(20).setCellValue(webPerson);
			//修改紀錄
			row.createCell(21).setCellValue(order.getModifyLogs());
            return row;
        }).collect(toList())).collect(toList());

	}

	@Override
	protected void build(Workbook workbook, Map<String, Customer> customerMap, List<CustomerOrder> orders,
			Map<String, List<CustomerOrder>> piles) {
		throw new UnsupportedOperationException();
	}

	private String getType(String data){
		String type ="";
		if(StringUtils.equals(data,"WEB")){
			type="CS新單";
		}else if(StringUtils.equals(data,"APP")){
			type="APP新單";
		}else if(StringUtils.equals(data,"WEB_END")){
			type="CS結單";
		}else if(StringUtils.equals(data,"APP_END")){
			type="APP結單";
		}else if(StringUtils.equals(data,"DEL")){
			type="已刪除";
		}else if(StringUtils.equals(data,"END")){
			type="已結單";
		}
		return type;
	}
}
