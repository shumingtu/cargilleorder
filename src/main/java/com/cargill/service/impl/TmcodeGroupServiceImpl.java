package com.cargill.service.impl;

import com.cargill.dto.TmcodeGroupDto;
import com.cargill.exception.CargillException;
import com.cargill.exception.ErrorCode;
import com.cargill.model.TmcodeGroup;
import com.cargill.repository.TmcodeGroupRepository;
import com.cargill.service.TmcodeGroupService;
import com.cargill.transform.TmcodeGroupTransform;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@Slf4j
public class TmcodeGroupServiceImpl implements TmcodeGroupService {

    @Autowired
    private TmcodeGroupRepository tmcodeGroupRepository;
    @Autowired
    private TmcodeGroupTransform tmcodeGroupTransform;

    @Override
    public List<TmcodeGroupDto> getTmcodeByGroupName(String groupName) {
        List<TmcodeGroup> tmcodeGroupList = groupName.equals("") ? tmcodeGroupRepository.findAll()
                : tmcodeGroupRepository.findByGroupName(groupName);
        return tmcodeGroupTransform.toDtos(tmcodeGroupList);
    }
}
