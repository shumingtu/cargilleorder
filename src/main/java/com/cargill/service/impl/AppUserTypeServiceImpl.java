package com.cargill.service.impl;


import com.cargill.dto.AppUserTypeDto;
import com.cargill.repository.AppUserTypeRepository;
import com.cargill.exception.CargillException;
import com.cargill.exception.ErrorCode;
import com.cargill.model.AppUserType;
import com.cargill.service.AppUserTypeService;
import com.cargill.transform.AppUserTypeTransform;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
@Slf4j
public class AppUserTypeServiceImpl implements AppUserTypeService {

    @Autowired
    private AppUserTypeRepository appUserTypeRepository;
    @Autowired
    private AppUserTypeTransform appUserTypeTransform;

    @Override
    @Cacheable(value = "AppUserTypeCache")
    public List<AppUserTypeDto> getAllTypes() {
        List<AppUserType> typeList = Optional.ofNullable(appUserTypeRepository.findAll())
                .orElseThrow(() -> new CargillException(ErrorCode.NOT_FOUND));
        return appUserTypeTransform.toDtos(typeList);
    }
}
