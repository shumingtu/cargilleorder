package com.cargill.service.impl;

import com.cargill.dto.AppAccountDto;
import com.cargill.dto.WebAccountDto;
import com.cargill.dto.request.AppAccountRequest;
import com.cargill.dto.request.WebAccountRequest;
import com.cargill.exception.CargillException;
import com.cargill.exception.ErrorCode;
import com.cargill.model.AppUser;
import com.cargill.model.User;
import com.cargill.service.AppUserService;
import com.cargill.transform.AppUserTransform;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.cargill.repository.AppUserRepository;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
@Slf4j
public class AppUserServiceImpl implements AppUserService {

    @Autowired
    private AppUserRepository appUserRepository;
    @Autowired
    private AppUserTransform appUserTransform;

    @Override
    public Page<AppAccountDto> findByCondition(AppAccountRequest condition, List<String> type_ids, Pageable pageable) {
        return (type_ids.isEmpty() ?
                appUserRepository.findByUnicodeIgnoreCaseContainingAndUserNameIgnoreCaseContaining(condition.getUnicode(), condition.getUserName(), pageable)
                :
                appUserRepository.findByUnicodeIgnoreCaseContainingAndUserNameIgnoreCaseContainingAndTypeIn(condition.getUnicode(), condition.getUserName(), type_ids, pageable))
                .map(appUserTransform::toDto);
    }

    @Override
    public AppAccountDto getAppAccountByUnicode(String unicode) {
        AppUser appUser = Optional.ofNullable(appUserRepository.findByUnicode(unicode))
                .orElseThrow(() -> new CargillException(ErrorCode.NOT_FOUND));
        return appUserTransform.toDto(appUser);
    }

    @Override
    public AppAccountDto getAppAccount(BigInteger id) {
        AppUser appUser = Optional.ofNullable(appUserRepository.findById(id))
                .orElseThrow(() -> new CargillException(ErrorCode.NOT_FOUND));
        return appUserTransform.toDto(appUser);
    }

    @Override
    public BigInteger deleteByUnicode(String unicode) {
        return appUserRepository.deleteByUnicode(unicode);
    }

    @Override
    public void deleteById(BigInteger id) {
        appUserRepository.delete(id);
    }

    @Override
    public AppAccountDto insertAccount(AppAccountDto accountDto) {
        AppUser appUser = new AppUser();
        appUser.setType(accountDto.getType());
        appUser.setUnicode(accountDto.getUnicode());
        appUser.setUserName(accountDto.getUserName());
        appUser.setPhone(accountDto.getPhone());
        appUser.setCanAct(accountDto.getCanAct());
        appUser.setVerifyResult("F");
        return appUserTransform.toDto(appUserRepository.save(appUser));
    }

    @Override
    public AppAccountDto updateAccount(AppAccountDto accountDto) {
        AppUser appUser = appUserRepository.findById(accountDto.getId());
        appUser.setType(accountDto.getType());
        appUser.setUserName(accountDto.getUserName());
        appUser.setPhone(accountDto.getPhone());
        appUser.setCanAct(accountDto.getCanAct());
        return appUserTransform.toDto(appUserRepository.save(appUser));
    }

    @Override
    public Boolean isAccountExist(String unicode) {
        AppUser appUser = appUserRepository.findByUnicode(unicode);
        if (null == appUser) {
            return Boolean.FALSE;
        } else {
            return Boolean.TRUE;
        }

    }

    @Override
    public AppAccountDto updateAccountBySms(BigInteger account_id, String sms_code) {
        AppUser appUser = appUserRepository.findById(account_id);
        appUser.setVerifyCode(sms_code);
        appUser.setVerifyCreatetime(Calendar.getInstance());
        appUser.setVerifySendCounts(Optional.ofNullable(appUser.getVerifySendCounts()).orElse(0) + 1);

        return appUserTransform.toDto(appUserRepository.save(appUser));
    }

}
