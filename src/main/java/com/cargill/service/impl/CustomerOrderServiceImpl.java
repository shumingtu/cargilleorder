package com.cargill.service.impl;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import com.cargill.dto.A2CellDto;
import com.cargill.dto.A2Dto;
import com.cargill.dto.A2RowDto;
import com.cargill.dto.CustomerOrderDto;
import com.cargill.dto.DetailPdDto;
import com.cargill.dto.DetailSpDto;
import com.cargill.dto.TmcodeGroupDto;
import com.cargill.dto.request.CustomerOrderUpdateRequest;
import com.cargill.dto.request.ReportRequest;
import com.cargill.dto.request.SearchA2Request;
import com.cargill.dto.request.SearchCustomerOrderRequest;
import com.cargill.exception.CargillException;
import com.cargill.exception.ErrorCode;
import com.cargill.model.Customer;
import com.cargill.model.CustomerOrder;
import com.cargill.model.Product;
import com.cargill.model.parameter.OrderTypeCode;
import com.cargill.model.parameter.TPDrugType;
import com.cargill.model.parameter.order.DetailDrugSP;
import com.cargill.model.parameter.order.DetailPD;
import com.cargill.model.parameter.order.DetailSP;
import com.cargill.model.parameter.order.OrderDetail;
import com.cargill.model.view.ViewProductTPDrug;
import com.cargill.properties.AppProperties;
import com.cargill.repository.CustomerOrderRepository;
import com.cargill.repository.CustomerQuotaRepository;
import com.cargill.repository.ProductRepository;
import com.cargill.repository.ViewProductTPDrugRepository;
import com.cargill.service.CustomerOrderService;
import com.cargill.transform.CustomerOrderTransform;
import com.cargill.util.CodeUtil;
import com.google.common.collect.Maps;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
public class CustomerOrderServiceImpl implements CustomerOrderService {

    private static final String FORMAT_DATETIME = "YYYY-MM-dd HH:mm";
    private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyyMMdd HH:mm");
    private static final DateTimeFormatter FORMATTER = DateTimeFormat.forPattern("YYYY-MM");
    private static final SimpleDateFormat SDF_HH_MM = new SimpleDateFormat(FORMAT_DATETIME);

    @Autowired
    private CustomerOrderRepository customerOrderRepository;
    @Autowired
    private CustomerQuotaRepository customerQuotaRepository;
    @Autowired
    private CustomerOrderTransform customerOrderTransform;
    @Autowired
	private ViewProductTPDrugRepository viewProductTPDrugRepository;
	@Autowired
	private ProductRepository productRepository
	;@Autowired
    private RestTemplate restTemplate;
    private AppProperties appProperties;
    
    @Autowired
	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
    @Autowired
    public void setAppProperties(AppProperties appProperties) {
		this.appProperties = appProperties;
	}

    private static String replaceCellValue(OrderDetail detail) {
        // *散裝=x , 3kg=3K , 10kg=1 , 20kg=2 , 25kg=2.5 ,
        // 30kg=3 , 30kg(C)=3C , 50kg=5 , 1000kg=J , 500kg(J5)=J5
        String unitDisplay = detail.getUnit();
        switch (detail.getUnit()) {
            case "散裝/X":
                unitDisplay = "x";
                break;
            case "3kg/3K":
                unitDisplay = "3K";
                break;
            case "10kg/1":
                unitDisplay = "1";
                break;
            case "20kg/2":
                unitDisplay = "2";
                break;
            case "25kg/2.5":
                unitDisplay = "2.5";
                break;
            case "30kg/3":
                unitDisplay = "3";
                break;
            case "30kg(C)/3C":
                unitDisplay = "3C";
                break;
            case "50kg/5":
                unitDisplay = "5";
                break;
            case "1000kg/J":
                unitDisplay = "J";
                break;
            case "500kg(J5)/J5":
                unitDisplay = "J5";
                break;
        }

        return MessageFormat.format("{0}/{1}<br />", unitDisplay, detail.getCount());
    }

	@Override
	public CustomerOrder getOrder(String order_no) {
		CustomerOrder order = Optional.ofNullable(customerOrderRepository.findByOrderNo(order_no))
				.orElseThrow(() -> new CargillException(ErrorCode.INVALID_ORDER_DATA));
		/* <20210715> Mike: 只有在APP建的單才需要重新修正訂單金額及特配金額 START */
		if (StringUtils.equals(OrderTypeCode.APP.name(), order.getType())) {
			this.updateDetailSpPrice(order.getOrderDetailList());
		}
		/* <20210715> Mike: 只有在APP建的單才需要重新修正訂單金額及特配金額 END */
		return order;
	}

    @Override
    public Page<CustomerOrderDto> findAllCustomerOrder(SearchCustomerOrderRequest condition, Pageable pageable) {
    	/* <20201117> MikeFu: 訂單出貨日期加入倒序 START */
    	Sort sort = new Sort(Direction.DESC, "deliveryDate");
    	pageable = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), sort);
    	/* <20201117> MikeFu: 訂單出貨日期加入倒序 END */
        return customerOrderRepository.findAll((root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (!condition.getOrderNo().equals("")) {
                predicates.add(cb.like(root.get("orderNo"), "%" + condition.getOrderNo() + "%"));
            }
            if (!condition.getCustomerId().equals("")) {
                predicates.add(cb.like(root.get("customerSubId"), "%" + condition.getCustomerId() + "%"));
            }
            if (!condition.getCustomerName().equals("")) {
                predicates.add(cb.like(root.get("customerSubName"), "%" + condition.getCustomerName() + "%"));
            }
            if (!condition.getPoCode().equals("")) {
                predicates.add(cb.like(root.get("poCode"), "%" + condition.getPoCode() + "%"));
            }
            if (!condition.getDeliveryStartDate().equals("") && !condition.getDeliveryEndDate().equals("")) {
                predicates.add(cb.between(root.get("deliveryDate").as(String.class), condition.getDeliveryStartDate() + " 00:00:00", condition.getDeliveryEndDate() + " 23:59:59"));
            }
            if (!condition.getOrderType().equals("")) {
                if (condition.getOrderType().equals("WEB") || condition.getOrderType().equals("APP")) {
                    predicates.add(cb.equal(root.get("type").as(String.class), condition.getOrderType()));
                } else if(condition.getOrderType().equals("DEL") ){
                    predicates.add(cb.like(root.get("type").as(String.class), "DEL"));
                } else {
                    predicates.add(cb.like(root.get("type").as(String.class), "%END%"));
                }

            }
            query.where(predicates.toArray(new Predicate[0]));
            return null;
    	}, pageable).map(customerOrderTransform::toDto);
    }

    @Override
    public Page<CustomerOrderDto> findByAppAndTmcodes(List<TmcodeGroupDto> tmcodeGroupDtoList, Pageable pageable) {
    	/* <201905> ADD: 訂單出貨日期加入倒序 start */
    	Sort sort = new Sort(Direction.ASC, "deliveryDate");
    	pageable = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), sort);
    	/* <201905> ADD: 訂單出貨日期加入倒序 end */
    	long start = System.currentTimeMillis();
    	Page<CustomerOrderDto> response = customerOrderRepository.findAll((root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(cb.equal(root.get("type"), OrderTypeCode.APP.toString()));
            if (tmcodeGroupDtoList != null) {
                List<String> tmcodeList = new ArrayList<String>();
                if (null != tmcodeGroupDtoList) {
                    for (TmcodeGroupDto tmcodeGroupDto : tmcodeGroupDtoList) {
                        tmcodeList.add(tmcodeGroupDto.getTmcodeId());
                    }
                }
                predicates.add(root.get("tmcode").in(tmcodeList));
            }
            query.where(predicates.toArray(new Predicate[0]));
            return null;
        }, pageable).map(customerOrderTransform::toDto);
    	long end = System.currentTimeMillis();
    	System.out.println("花費時間: " + (end - start));
        return response;
    }

    @Override
    public CustomerOrderDto createOrUpdateOrder(CustomerOrderUpdateRequest request, String order_person) {
        CustomerOrder updateOrder = Optional.ofNullable(request)
                .map((r) -> {
                    CustomerOrder order = Optional.ofNullable(customerOrderRepository.findByOrderNo(r.getOrderNo()))
                            .orElseGet(() -> {
                                CustomerOrder tempOrder = new CustomerOrder();
                                tempOrder.setOrderNo(CodeUtil.generateOrderNo());
                                return tempOrder;
                            });

                    order.setTmcode(request.getTmcode());

                    order.setOrderPerson(Optional.ofNullable(order.getOrderPerson()).orElse(order_person));
                    order.setCustomerSubId(request.getCustomerSubId());
                    order.setCustomerSubName(request.getCustomerSubName());
                    order.setCustomerPhone(request.getCustomerPhone());
                    order.setDeliveryDate(DateTime.parse(request.getDeliveryDate(), DateTimeFormat.forPattern(FORMAT_DATETIME))
                            .toGregorianCalendar());
                    order.setShipTo(request.getShipTo());
                    order.setDescApp(request.getDescApp());
                    order.setDescWeb(request.getDescWeb());

                    order.setType(OrderTypeCode.WEB.toString());
                    order.setPoCode(request.getPoCode());
                    Optional.ofNullable(order.getPoCode())
                            .filter(it -> !order.getPoCode().isEmpty())
                            .ifPresent(it -> order.setType(OrderTypeCode.WEB_END.toString()));

                    order.setOrderDetailList(
                            Optional.ofNullable(request.getOrderDetailList())
                                    .orElse(Collections.emptyList())
                                    .stream().map(it -> {
                                OrderDetail detail = new OrderDetail();
                                detail.setCode(it.getCode());
                                detail.setCount(it.getCount());
                                detail.setName(it.getName());
                                detail.setSpec(it.getSpec());
                                detail.setUnit(it.getUnit());
                                detail.setWeight(it.getWeight());
                                detail.setPhase(it.getPhase());
                                detail.setPrice(it.getPrice());
                                detail.setVehicleType(it.getVehicleType());
                                detail.setVehicleName(it.getVehicleName());

                                Optional.ofNullable(it.getDetailSP())
                                        .filter(DetailSpDto::getExist)
                                        .ifPresent((sp) -> {
                                            DetailSP detailSP = new DetailSP();
                                            detailSP.setId(Optional.ofNullable(sp.getId()).orElse(CodeUtil.generateTpNo()));
                                            detailSP.setCreateDate(Optional.ofNullable(sp.getCreateDate()).orElse(DateTime.now().toString(FORMAT_DATETIME)));
                                            detailSP.setValue(sp.getValue());
                                            detailSP.setSymptom(sp.getSymptom());
                                            detailSP.setDeliveryDate(sp.getDeliveryDate());
                                            detailSP.setProductLineNote(sp.getProductLineNote());
                                            detailSP.setProductNote(sp.getProductNote());
                                            detailSP.setProductSpec(sp.getProductSpec());
                                            detailSP.setNoteA(sp.getNoteA());
                                            detailSP.setNoteB(sp.getNoteB());
                                            detailSP.setNoteC(sp.getNoteC());

                                            detailSP.setTotalPrice(sp.getTotalPrice());
                                            detailSP.setCodeWeight(sp.getCodeWeight());
                                            detailSP.setSpPrice(sp.getSpPrice());

                                            detailSP.setDrugCompanyList(Optional.ofNullable(sp.getDrugCompanyList()).orElse(Collections.emptyList()).stream().map(c -> {
                                                DetailDrugSP drugSP = new DetailDrugSP();
                                                drugSP.setCode(c.getCode());
                                                drugSP.setCount1kg(c.getCount1kg());
                                                drugSP.setCount2kg(c.getCount2kg());
                                                drugSP.setDays(Optional.ofNullable(c.getDays()).orElse(""));
                                                drugSP.setName(c.getName());
                                                drugSP.setNote(c.getNote());
                                                drugSP.setRecommend(c.getRecommend());
                                                drugSP.setTotalCount(c.getTotalCount());
                                                drugSP.setPrice(c.getPrice());

                                                return drugSP;
                                            }).collect(Collectors.toList()));
                                            detailSP.setDrugSelfList(Optional.ofNullable(sp.getDrugSelfList()).orElse(Collections.emptyList()).stream().map(c -> {
                                                DetailDrugSP drugSP = new DetailDrugSP();
                                                drugSP.setCode(c.getCode());
                                                drugSP.setCount1kg(c.getCount1kg());
                                                drugSP.setCount2kg(c.getCount2kg());
                                                drugSP.setDays(c.getDays());
                                                drugSP.setName(c.getName());
                                                drugSP.setNote(c.getNote());
                                                drugSP.setRecommend(c.getRecommend());
                                                drugSP.setTotalCount(c.getTotalCount());
                                                drugSP.setPrice(c.getPrice());

                                                return drugSP;
                                            }).collect(Collectors.toList()));

                                            if (sp.getUpdate()) {
                                                if (detailSP.getModifyLogs().isEmpty()) {
                                                	// <20190802> 修改紀錄變更者名稱異常問題
//                                                    detailSP.setModifyLogs(Collections.singletonList(SDF.format(new Date()) + " WEB " + order.getOrderPerson() + " " + "新增"));
                                                    detailSP.setModifyLogs(Collections.singletonList(
                                                    		   SDF.format(new Date()) + " WEB " + order_person + " " + "新增"));
                                                } else {
                                                	// <20190802> 修改紀錄變更者名稱異常問題
//                                                    detailSP.getModifyLogs().add(SDF.format(new Date()) + " WEB " + order.getOrderPerson() + " " + "儲存");
                                                    detailSP.getModifyLogs().add(SDF.format(new Date()) + " WEB " + order_person + " " + "儲存");
                                                }
                                            }

                                            detail.setDetailSP(detailSP);
                                        });

                                Optional.ofNullable(it.getDetailPD())
                                        .filter(DetailPdDto::getExist)
                                        .ifPresent((pd) -> {
                                            DetailPD detailPD = new DetailPD();
                                            detailPD.setId(Optional.ofNullable(pd.getId()).orElse(CodeUtil.generatePdNo()));
                                            detailPD.setCreateDate(Optional.ofNullable(pd.getCreateDate()).orElse(DateTime.now().toString(FORMAT_DATETIME)));
                                            detailPD.setNote(pd.getNote());

                                            detail.setDetailPD(detailPD);
                                        });

                                return detail;
                            }).collect(Collectors.toList())
                    );
                    return order;
                }).orElseThrow(() -> new CargillException(ErrorCode.INVALID_ORDER_DATA));

        Optional.ofNullable(request.getOrderNo()).ifPresent((it) -> updateOrder.updateDetails());
        if (updateOrder.getId() != null) {
        	CustomerOrder customerOrder = this.customerOrderRepository.findOne(updateOrder.getId());
        	if (customerOrder == null
        			|| (customerOrder.getModifyTime() != null
        			&& customerOrder.getModifyTime().compareTo(request.getModifyTime()) != 0)) {
        		throw new CargillException(ErrorCode.ERROR_ORDER_DATA);
        	}
        }
        customerOrderRepository.save(updateOrder);
        return customerOrderTransform.toDto(updateOrder);
    }
    
    public void afterCreateOrUpdateOrder(CustomerOrderDto dto) {
        System.out.println("CustomerOrderServiceImpl ,afterCreateOrUpdateOrder 345 :"+ dto.toString());
    	String poCode = dto.getPoCode();
        System.out.println("CustomerOrderServiceImpl ,afterCreateOrUpdateOrder 347 :" + poCode);
    	OrderTypeCode orderType = dto.getType();
        System.out.println("CustomerOrderServiceImpl ,afterCreateOrUpdateOrder 347 :" + orderType.toString());

        if (StringUtils.isNotBlank(poCode)
        		&& OrderTypeCode.WEB_END.equals(orderType)) {
        	HttpHeaders headers = new HttpHeaders();
        	headers.setContentType(MediaType.APPLICATION_JSON);
            System.out.println("CustomerOrderServiceImpl ,afterCreateOrUpdateOrder 352 :");
        	Map<String, Object> map = Maps.newHashMap();
        	map.put("orderNo", dto.getOrderNo());
        	map.put("customerId", dto.getCustomerSubId());
        	map.put("poCode", poCode);
        	map.put("type", orderType.name());
        	HttpEntity<Map<String, Object>> entity = new HttpEntity<>(map, headers);
            System.out.println("CustomerOrderServiceImpl ,afterCreateOrUpdateOrder 362 :"+appProperties.getOrderUrl());
            System.out.println("CustomerOrderServiceImpl ,afterCreateOrUpdateOrder 363 :"+entity);
            try{
                restTemplate.postForLocation(appProperties.getOrderUrl(), entity);    
            }catch (Exception e ){
                System.out.println("CustomerOrderServiceImpl ,afterCreateOrUpdateOrder 367 :"+e); 
            }

            System.out.println("CustomerOrderServiceImpl ,afterCreateOrUpdateOrder 370 :");
        }
    }

    public CustomerOrderDto duplicateOrder(CustomerOrder order, String person) {
        CustomerOrder orderNew = new CustomerOrder();
        BeanUtils.copyProperties(order, orderNew, new String[] { "customer" });

        orderNew.setOrderNo(CodeUtil.generateOrderNo());
        orderNew.setModifyLogs("");
        Calendar today = Calendar.getInstance();
        orderNew.setCreateTime(today);
        orderNew.setModifyTime(today);
        orderNew.setOrderPerson(person);
        orderNew.setId(null);
        orderNew.setPoCode("");
        orderNew.setType(OrderTypeCode.WEB.toString());
        /* <20210528> Mike: 因複製單後產出單訂單統計報表呈現來源值有誤 START */
        // 因Web複製的訂單該欄位為空值
        orderNew.setOrderSys(null);
        /* <20210528> Mike: 因複製單後產出單訂單統計報表呈現來源值有誤 END */

        orderNew.postLoad();
        List<OrderDetail> orderDetailList = orderNew.getOrderDetailList();
        

		/* <20210715> Mike: 更新特配單金額 START */
		this.updateDetailSpPrice(orderDetailList);
		/* <20210715> Mike: 更新特配單金額 END */
        
        
        if(orderDetailList != null) {
        	for(OrderDetail orderDetail : orderDetailList) {
        		
        		if(orderDetail.getDetailSP() != null ) {
        			orderDetail.getDetailSP().setCreateDate(SDF_HH_MM.format(today.getTime()));
        		}
        	}
        	orderNew.updateDetails();
        }

        return customerOrderTransform.toDto(customerOrderRepository.save(orderNew));
    }

    public void removeOrder(CustomerOrder order) {
        customerOrderRepository.delete(order);
    }
    public void updateOrderIsDeleteToY(CustomerOrder order){
        order.setType("DEL");
        customerOrderRepository.save(order);
    }

    @Override
    public List<A2Dto> buildA2(SearchA2Request request) {
        return customerOrderRepository.findAll((root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();

            Optional.ofNullable(request.getCustomerId())
                    .filter(s -> !s.isEmpty())
                    .ifPresent(s -> predicates.add(cb.like(root.get("customerSubId"), "%" + request.getCustomerId() + "%")));

            Optional.ofNullable(request.getCustomerName())
                    .filter(s -> !s.isEmpty())
                    .ifPresent(s -> predicates.add(cb.like(root.get("customerSubName"), "%" + request.getCustomerName() + "%")));

            Optional.ofNullable(request.getTmcode())
                    .filter(s -> !s.isEmpty())
                    .ifPresent(s -> predicates.add(cb.like(root.get("tmcode"), "%" + request.getTmcode() + "%")));
            // 查詢出當月區間
            Optional.ofNullable(request.getDateMonth())
                    .filter(s -> !s.isEmpty())
                    .ifPresent(s -> {
                        DateTime createMonth = FORMATTER.parseDateTime(request.getDateMonth());
                        // 2021/07/06 新增 查詢當月起往前推所有資料
                        predicates.add(cb.lessThanOrEqualTo(root.get("deliveryDate"),  createMonth.plusMonths(1).toDate()));
                        // 2021/07/06 留存舊有邏輯 日期查詢
                        // predicates.add(cb.between(root.get("deliveryDate"),
                        //         createMonth.withDayOfMonth(1).toDate(),
                        //         createMonth.plusMonths(1).toDate()
                        // ));
                    });

            query.where(predicates.toArray(new Predicate[0]));
            return null;
        }).stream()
                .collect(Collectors.groupingBy(CustomerOrder::getCustomerSubId))
                .entrySet()
                .stream()
                .map(data -> {
                    A2Dto a2 = new A2Dto();
                    data.getValue().stream().findFirst().ifPresent(order -> {
                        a2.setCustomerSubId(order.getCustomerSubId());
                        a2.setCustomerSubName(order.getCustomerSubName());
                        a2.setCustomerPhone(order.getCustomerPhone());
                        a2.setTmcode(order.getTmcode());

                        Optional.ofNullable(customerQuotaRepository.findByCustomerId(order.getCustomerSubId()))
                                .ifPresent(it -> a2.setCustomerQuota(it.getNote().replaceAll("\\R", "<br />")));
                    });

                    data.getValue().stream()
                            .collect(Collectors.groupingBy(CustomerOrder::getShipTo))
                            .entrySet()
                            .stream()
                            .peek(order -> {
                                A2RowDto row = new A2RowDto();
                                row.getCellList().set(0, new A2CellDto(order.getKey()));
                                IntStream.range(0, row.getCellList().size())
                                        .skip(1)
                                        .mapToObj(i -> row.getCellList().set(i, new A2CellDto(i + "")))
                                        .collect(Collectors.toList());
                                a2.getRowList().add(row);

                                // 訂單
                                order.getValue().stream()
                                        .peek((orderForDetail) -> {
                                            int groupingDay = new DateTime(orderForDetail.getDeliveryDate()).dayOfMonth().get();

                                            String orderDateTime = Optional.of(orderForDetail.getDeliveryDate().getTime()).map(SDF::format).orElse("");
                                            DateTime queryDate = FORMATTER.parseDateTime(request.getDateMonth());
                                            String queryDateYyyymm =  String.valueOf(queryDate.getYear())+(queryDate.getMonthOfYear()<10?"0":"")+String.valueOf(queryDate.getMonthOfYear());
                                            String orderDeliveryDateYyyymm = orderDateTime.substring(0,6);

                                            // 訂單詳細資料
                                            orderForDetail.getOrderDetailList()
                                                    .stream()
                                                    .sorted(
                                                           Comparator.comparing((OrderDetail::getCode)
                                                           )
                                                    )
                                                    .collect(Collectors.groupingBy(OrderDetail::getCode))
                                                    .entrySet()
                                                    .stream()
                                                    .peek((detail) -> {
                                                        A2RowDto currentRow = a2.getRowList().stream().filter(it -> detail.getKey().equals(it.getCode()))
                                                                .findFirst().orElseGet(() -> {
                                                                    A2RowDto temp = new A2RowDto();
                                                                    IntStream.range(0, temp.getCellList().size())
                                                                            .mapToObj(i -> temp.getCellList().set(i, new A2CellDto("")))
                                                                            .collect(Collectors.toList());

                                                                    a2.getRowList().add(temp);

                                                                    return temp;
                                                                });

                                                        OrderDetail cellOrder = detail.getValue().get(0);
                                                        A2CellDto cell = new A2CellDto();


                                                        currentRow.setCode(detail.getKey());
                                                        // 2021/07/06 如果訂單資料和查詢日期 不同時 即為非當月訂單 就是 久未訂購的飼料
                                                        if (orderDeliveryDateYyyymm.equals(queryDateYyyymm)) {
                                                            // 原邏輯
                                                            cell.setValue(detail.getKey() + "" + cellOrder.getName());
                                                            currentRow.getCellList().set(0, cell);
                                                            A2CellDto groupingCell = currentRow.getCellList().get(groupingDay);
                                                            groupingCell.setValue(groupingCell.getValue() + replaceCellValue(cellOrder));
                                                            currentRow.getCellList().set(groupingDay, groupingCell);
                                                        } else {
                                                            // 新增久未訂購的邏輯
                                                            cell.setValue(detail.getKey() + "" + cellOrder.getName());
                                                            currentRow.getCellList().set(0, cell);
                                                            A2CellDto groupingCell = currentRow.getCellList().get(groupingDay);
                                                            groupingCell.setValue("");
                                                            currentRow.getCellList().set(groupingDay, groupingCell);
                                                        }
                                                    })
                                                    .collect(Collectors.toList());
                                        })
                                        .collect(Collectors.toList());
                            }).collect(Collectors.toList());

                    return a2;
                }).collect(Collectors.toList())
                .stream()
                .peek(dto -> {
                	List<A2RowDto> sortedList = dto.getRowList()
                    .stream()
                    .sorted(Comparator.comparing(A2RowDto::getCode))
                    .collect(Collectors.toList());
                	dto.setRowList(sortedList);
                })
                .sorted(Comparator.comparing(A2Dto::getCustomerSubId))
                .collect(Collectors.toList());
    }

	@Override
	public List<CustomerOrder> getReportData(ReportRequest request) {
		return customerOrderRepository.findAll((root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();
            Join<CustomerOrder, Customer> coJoinC = root.join("customer", JoinType.LEFT);
            predicates.add(cb.between(root.get("deliveryDate"), request.getStartDate(), request.getEndDate()));
            if (!request.getSscode().equals("")) {
                predicates.add(cb.like(coJoinC.get("sscode"), request.getSscode()));
            }
            if (!request.getLivestock().equals("")) {
                predicates.add(cb.like(coJoinC.get("livestock"), request.getLivestock()));
            }
            if (!request.getTmcode().equals("")) {
            	predicates.add(cb.like(root.get("tmcode"), request.getTmcode()));
            }
            query.where(predicates.toArray(new Predicate[0]));
            return null;
    	});
	}

	/**
	 * 更新特配單金額
	 * 
	 * @author Mike
	 *
	 * @param orderDetailList 特配單清單
	 */
	private void updateDetailSpPrice(List<OrderDetail> orderDetailList) {
		try {
			log.info("[更新特配單金額] 開始更新");
			if (CollectionUtils.isEmpty(orderDetailList)) {
				log.info("[更新特配單金額] 無特配單需要更新");
				return;
			}
			for (int i = 0; i < orderDetailList.size(); i++) {
				OrderDetail detail = orderDetailList.get(0);
				log.info("[更新特配單金額] 正在處理第 {} 筆的特配單", (i + 1));
				// 更新特配單價格
				Product product = productRepository.findByCode(detail.getCode());
				BigDecimal price = this.getSpPrice(product, detail);
				detail.setPrice(price.doubleValue());
				if (detail.getDetailSP() == null) {
					// 無指定特配單工廠，所以無特配小單需要處理
					log.info("[更新特配單金額] 無指定特配單工廠");
					continue;
				}
				// 更新特配小單-公司藥價格
				log.info("[更新特配單金額] 開始更新特配小單-公司藥價格");
				this.updateDurgCompanyPrice(detail.getDetailSP().getDrugCompanyList());
				log.info("[更新特配單金額] 結束更新特配小單-公司藥價格");
				// 更新特配小單-自備藥價格
				log.info("[更新特配單金額] 開始更新特配小單-自備藥價格");
				this.updateDrugSelfPrice(detail.getDetailSP().getDrugSelfList());
				log.info("[更新特配單金額] 結束更新特配小單-自備藥價格");
			}
		} finally {
			log.info("[更新特配單金額] 結束更新");
		}
	}

	/**
	 * 取得特配單金額
	 * 
	 * @author Mike
	 *
	 * @param product 產品檔
	 * @param detail  特配單
	 * @return
	 */
	private BigDecimal getSpPrice(Product product, OrderDetail detail) {
		switch (detail.getUnit()) {
		case "散裝/X":
			return product.getPriceX();
		case "3kg/3K":
			return product.getPrice3kg3K();
		case "10kg/1":
			return product.getPrice10kg1();
		case "20kg/2":
			return product.getPrice20kg2();
		case "25kg/2.5":
			return product.getPrice25kg2point5C();
		case "30kg/3":
			return product.getPrice30kg3();
		case "30kg(C)/3C":
			return product.getPrice30kg3C();
		case "50kg/5":
			return product.getPrice50kg5();
		case "1000kg/J":
			return product.getPrice1000kgJ();
		case "500kg(J5)/J5":
			return product.getPrice500kgJ5();
		default:
			return BigDecimal.ZERO;
		}
	}

	/**
	 * 更新特配小單 - 公司藥
	 * 
	 * @author Mike
	 *
	 * @param drugCompanyList 特配小單公司藥清單
	 */
	private void updateDurgCompanyPrice(List<DetailDrugSP> drugCompanyList) {
		try {
			log.info("[更新特配小單] 開始更新公司藥");
			if (CollectionUtils.isEmpty(drugCompanyList)) {
				log.info("[更新特配小單] 無公司藥需更新");
				return;
			}
			for (DetailDrugSP sp : drugCompanyList) {
				BigDecimal drug = this.getDrugPrice(TPDrugType.COMPANY, sp.getCode());
				sp.setPrice(drug);
			}
		} finally {
			log.info("[更新特配小單] 結束更新公司藥");
		}
	}

	/**
	 * 更新特配小單 - 自備藥
	 * 
	 * @author Mike
	 *
	 * @param drugSelfList 特配小單自備藥清單
	 */
	private void updateDrugSelfPrice(List<DetailDrugSP> drugSelfList) {
		try {
			log.info("[更新特配小單] 開始更新自備藥");
			if (CollectionUtils.isEmpty(drugSelfList)) {
				log.info("[更新特配小單] 無自備藥需更新");
				return;
			}
			for (DetailDrugSP sp : drugSelfList) {
				BigDecimal drug = this.getDrugPrice(TPDrugType.SELF, sp.getCode());
				sp.setPrice(drug);
			}
		} finally {
			log.info("[更新特配小單] 結束更新自備藥");
		}
	}

	/**
	 * 取得特配小單價格
	 * 
	 * @author Mike
	 *
	 * @param type 特配小單類型
	 * @param code 特配藥代碼
	 * @return
	 */
	private BigDecimal getDrugPrice(TPDrugType type, String code) {
		try {
			log.info("[特配小單價格] 開始取得特配小單價格");
			if (type == null || StringUtils.isBlank(code)) {
				log.info("[特配小單價格] 無資料可以取得特配小單價格");
				return BigDecimal.ZERO;
			}
			//ViewProductTPDrug drug = viewProductTPDrugRepository.findByCodeAndDrugType(code, type);
            List<ViewProductTPDrug> drugList = Collections.emptyList();
            drugList = viewProductTPDrugRepository.findByCodeStartingWithAndDrugType(
                    code, TPDrugType.COMPANY
            );

            if(drugList.size() == 0){
                log.info("[特配小單價格] 查無該特配小單的價格");
            }

//			if (drug == null) {
//				log.info("[特配小單價格] 查無該特配小單的價格");
//			}


			return drugList.size() == 0 ? BigDecimal.ZERO : drugList.get(0).getPrice();
		} finally {
			log.info("[特配小單價格] 結束取得特配小單價格");
		}
	}
}
