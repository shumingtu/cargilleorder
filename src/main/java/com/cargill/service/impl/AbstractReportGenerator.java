package com.cargill.service.impl;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;

import com.cargill.dto.request.ReportRequest;
import com.cargill.model.Customer;
import com.cargill.model.CustomerOrder;
import com.cargill.repository.CustomerOrderRepository;
import com.cargill.repository.CustomerRepository;
import com.cargill.service.CustomerOrderService;
import com.cargill.service.IReportGenerator;

public abstract class AbstractReportGenerator implements IReportGenerator {

	protected CustomerOrderService customerOrderService;
	protected CustomerOrderRepository customerOrderRepository;
	private CustomerRepository customerRepository;
	protected final SimpleDateFormat SDF = new SimpleDateFormat("yyyy/MM/dd HH:mm");
	protected final SimpleDateFormat SDF_DATE = new SimpleDateFormat("yyyy/MM");

	@Autowired
	public void setCustomerOrderService(CustomerOrderService customerOrderService) {
		this.customerOrderService = customerOrderService;
	}
	@Autowired
	public void setCustomerOrderRepository(CustomerOrderRepository customerOrderRepository) {
		this.customerOrderRepository = customerOrderRepository;
	}
	@Autowired
	public void setCustomerRepository(CustomerRepository customerRepository) {
		this.customerRepository = customerRepository;
	}

	@Override
	public void buildWorkbook(Workbook workbook, ReportRequest request) {
		List<CustomerOrder> orders = this.getOrders(request);
		Map<String, Customer> customerMap = this.getCustomerMap(orders);
		Map<String, List<CustomerOrder>> piles = this.pile(orders);
		switch (request.getType()) {
		case "opt1":
			this.build(workbook, customerMap, orders);
			break;
		case "opt3":
			this.build(workbook, customerMap, orders, piles);
			break;
		}
	}

	private Map<String, Customer> getCustomerMap(List<CustomerOrder> orderList) {
		List<String> customerIds = orderList.stream().map(CustomerOrder::getCustomerSubId).collect(Collectors.toList());
		List<Customer> customers = customerRepository.findByCustomerPk_CustomerSubIdIn(customerIds);
		return customers.stream()
				.collect(Collectors.toMap(c -> c.getCustomerPk().getCustomerSubId(), c -> c, (oc, nc) -> {
					if (nc != null)
						return nc;
					else
						return oc;
				}));
	}
	
	protected abstract List<CustomerOrder> getOrders(ReportRequest request);

	protected abstract Map<String, List<CustomerOrder>> pile(List<CustomerOrder> orders);
	
	protected abstract void build(Workbook workbook, Map<String, Customer> customerMap, List<CustomerOrder> orders);
	
	protected abstract void build(Workbook workbook, Map<String, Customer> customerMap, List<CustomerOrder> orders, Map<String, List<CustomerOrder>> piles);

}
