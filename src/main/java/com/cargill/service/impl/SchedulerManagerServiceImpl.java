package com.cargill.service.impl;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import com.cargill.model.CustomerOrder;
import com.cargill.properties.BatchProperties;
import com.cargill.repository.CustomerOrderRepository;
import com.cargill.util.DateUtil;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import com.cargill.dto.NotificationCustomerDto;
import com.cargill.model.SchedulerNotification;
import com.cargill.repository.SchedulerNotificationRepository;
import com.cargill.service.SchedulerManagerService;
import com.cargill.transform.SchedulerNotificationTransform;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;




@Slf4j
@Service
public class SchedulerManagerServiceImpl implements SchedulerManagerService {
	
	private SchedulerNotificationRepository schedulerNotificationRepository;
	private SchedulerNotificationTransform schedulerNotificationTransform;
	private CustomerOrderRepository customerOrderRepository;
	private BatchProperties batchProperties;
	private SchedulerManagerService schedulerManagerService;
	@Value("${cargill.batch.spcific.notify.date}")
	private String notifyDate;


	@Autowired
	public void setSchedulerNotificationRepository(SchedulerNotificationRepository schedulerNotificationRepository) {
		this.schedulerNotificationRepository = schedulerNotificationRepository;
	}
	@Autowired
	public void setSchedulerNotificationTransform(SchedulerNotificationTransform schedulerNotificationTransform) {
		this.schedulerNotificationTransform = schedulerNotificationTransform;
	}

	@Override
	public Page<NotificationCustomerDto> getNotifictionList(Pageable pageable) {
		return this.schedulerNotificationRepository.findAllByOrderByNotificationDateDesc(pageable)
		.map(schedulerNotificationTransform::toDto);
	}

	@Autowired
	public void setSchedulerManagerService(SchedulerManagerService schedulerManagerService) {
		this.schedulerManagerService = schedulerManagerService;
	}

	@Autowired
	public void setBatchProperties(BatchProperties batchProperties) {
		this.batchProperties = batchProperties;
	}

	@Override
	public boolean cancelNotification(BigInteger id) {
		SchedulerNotification sn = this.schedulerNotificationRepository.findOne(id);
		if (sn == null) {
			throw new RuntimeException("取消推播失敗, 對象不存在: " + id);
		}
		sn.setCancelled(Boolean.TRUE);
		this.schedulerNotificationRepository.save(sn);
		return true;
	}

	@Override
	public List<SchedulerNotification> getNotifictions() {

		List<SchedulerNotification> datas = Collections.emptyList();
		if (batchProperties.getRealExecute()) {
			datas = this.schedulerNotificationRepository
					.findByNotificationDateAndFinishedAndCancelled(DateUtil.now(), Boolean.FALSE, Boolean.FALSE);
		}
		else {
		
			Date now = new Date();
			log.info("[排程-推播] 指定推播日期: {}", notifyDate);
			if (StringUtils.isNotBlank(notifyDate)) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				try {
					now = sdf.parse(notifyDate);
				} catch (ParseException e) {
					log.error("[排程-推播] 解析 cargill.batch.spcific.notify.date 有誤. 解析失敗原因: {}", 
							ExceptionUtils.getStackTrace(e));
				}
			}
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(now);
			datas = this.schedulerNotificationRepository.findByNotificationDateAndFinishedAndCancelled(calendar.getTime(),
					Boolean.FALSE, Boolean.FALSE);
					
		}

		List<SchedulerNotification> sns = new ArrayList<>();

		//設定起始日
		Calendar date = Calendar.getInstance();
		date.setTime(DateUtil.now());
		//date.set(2020, Calendar.JANUARY, 1);
		//資料處理
		for (SchedulerNotification sn : datas) {

			String customerSubId = sn.getCustomerSubId();
			String tmCode = sn.getTmcode();
			List<CustomerOrder> customerOrders= this.customerOrderRepository
					.findByCustomerSubIdAndCreateTimeAfterAndDetailsLike(customerSubId,date, "%"+sn.getCode()+"%");

			//無訂單資料加入發送清單
			if(customerOrders.isEmpty()){
				sns.add(sn);
			}else{
				String remark = "訂單編號:"+customerOrders.get(0).getOrderNo() +" 已於"+customerOrders.get(0).getCreateTime()+"產生";
				//有訂單資料取消並記錄
				this.schedulerManagerService.cancelNotificationUpdateRemark(sn.getId(),remark);
			}
		}

		return sns;
	}

	@Override
	public boolean cancelNotificationUpdateRemark(BigInteger id,String remark) {
		SchedulerNotification sn = this.schedulerNotificationRepository.findOne(id);
		if (sn == null) {
			throw new RuntimeException("取消失敗, 對象不存在: " + id);
		}
		sn.setCancelled(Boolean.TRUE);
		sn.setRemark(remark);
		this.schedulerNotificationRepository.save(sn);
		return true;
	}
}
