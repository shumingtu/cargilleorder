package com.cargill.service.impl;

import static java.util.stream.Collectors.toList;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cargill.dto.request.ReportRequest;
import com.cargill.exception.CargillException;
import com.cargill.exception.ErrorCode;
import com.cargill.model.CustomerOrder;
import com.cargill.repository.CustomerOrderRepository;
import com.cargill.service.IReportGenerator;
import com.cargill.service.ReportService;

import lombok.extern.slf4j.Slf4j;

@Service
@Transactional
@Slf4j
public class ReportServiceImpl implements ReportService {
    private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy/MM/dd HH:mm");

    @Autowired
    private CustomerOrderRepository customerOrderRepository;
    private Collection<IReportGenerator> reportGenerators;
    @Autowired
    public void setReportGenerators(Collection<IReportGenerator> reportGenerators) {
		this.reportGenerators = reportGenerators;
	}

    private Workbook getWorkbookFromTemplate(String template_file) {
        try {
            return WorkbookFactory.create(getClass().getResourceAsStream("/" + template_file));
        } catch (Exception e) {
            log.error("=== Invalid template file:" + e.getMessage());
            throw new CargillException(ErrorCode.INVALID_TEMPLATE_FILE);
        }
    }

    @Override
    public Workbook generateReport1(ReportRequest request) {
        Workbook workbook = this.getWorkbookByType(request);
		IReportGenerator generator = this.getReportGenerator(request);
		generator.buildWorkbook(workbook, request);
        return workbook;
    }

    @Override
    public Workbook generateReport2(ReportRequest request) {
        Workbook workbook = getWorkbookFromTemplate("report/opt2.xlsx");
        Sheet sheet = workbook.getSheetAt(0);

        List<CustomerOrder> orderList = customerOrderRepository.findByDeliveryDateBetween(request.getStartDate(), request.getEndDate());
        orderList.stream().map(order -> order.getOrderDetailList().stream().map(detail -> {
            Optional.ofNullable(detail.getDetailSP()).ifPresent(dsp -> dsp.getDrugCompanyList().stream().map(d -> {
                Row row = sheet.createRow(1 + sheet.getLastRowNum());

                row.createCell(0).setCellValue("公司藥");
                row.createCell(1).setCellValue(Optional.of(order.getDeliveryDate().getTime()).map(SDF::format).orElse(""));
                row.createCell(2).setCellValue(detail.getDetailSP().getId());
                row.createCell(3).setCellValue(order.getCustomerSubId());
                row.createCell(4).setCellValue(order.getCustomerSubName());

                row.createCell(5).setCellValue(detail.getCode());
                row.createCell(6).setCellValue(detail.getName());
                row.createCell(7).setCellValue(d.getCode());
                row.createCell(8).setCellValue(d.getName());
                row.createCell(9).setCellValue(Optional.ofNullable(d.getCount1kg()).orElse(0d));
                row.createCell(10).setCellValue(Optional.ofNullable(d.getCount2kg()).orElse(0d));
                row.createCell(11).setCellValue(Optional.ofNullable(d.getTotalCount()).orElse(0d));
                row.createCell(12).setCellValue(Optional.ofNullable(detail.getUnit()).orElse(""));
                row.createCell(13).setCellValue(Optional.ofNullable(dsp.getTotalPrice()).orElse(0));
                row.createCell(14).setCellValue(Optional.ofNullable(dsp.getCodeWeight()).orElse(0d));
                row.createCell(15).setCellValue(Optional.ofNullable(dsp.getSpPrice()).orElse(0d));
                row.createCell(16).setCellValue(Optional.ofNullable(dsp.getValue()).orElse(""));

                return row;

            }).collect(toList()));

            Optional.ofNullable(detail.getDetailSP()).ifPresent(dsp -> dsp.getDrugSelfList().stream().map(d -> {
                Row row = sheet.createRow(1 + sheet.getLastRowNum());


                row.createCell(0).setCellValue("自備藥");
                row.createCell(1).setCellValue(Optional.of(order.getDeliveryDate().getTime()).map(SDF::format).orElse(""));
                row.createCell(2).setCellValue(detail.getDetailSP().getId());
                row.createCell(3).setCellValue(order.getCustomerSubId());
                row.createCell(4).setCellValue(order.getCustomerSubName());

                row.createCell(5).setCellValue(detail.getCode());
                row.createCell(6).setCellValue(detail.getName());
                row.createCell(7).setCellValue(d.getCode());
                row.createCell(8).setCellValue(d.getName());
                row.createCell(9).setCellValue(Optional.ofNullable(d.getCount1kg()).orElse(0d));
                row.createCell(10).setCellValue(Optional.ofNullable(d.getCount2kg()).orElse(0d));
                row.createCell(11).setCellValue(Optional.ofNullable(d.getTotalCount()).orElse(0d));
                row.createCell(12).setCellValue(Optional.ofNullable(detail.getUnit()).orElse(""));
                row.createCell(13).setCellValue(Optional.ofNullable(dsp.getTotalPrice()).orElse(0));
                row.createCell(14).setCellValue(Optional.ofNullable(dsp.getCodeWeight()).orElse(0d));
                row.createCell(15).setCellValue(Optional.ofNullable(dsp.getSpPrice()).orElse(0d));
                row.createCell(16).setCellValue(Optional.ofNullable(dsp.getValue()).orElse(""));

                return row;

            }).collect(toList()));

            return detail;

        }).collect(toList())).collect(toList());

        return workbook;
    }

	@Override
	public Workbook generateReport3(ReportRequest request) {
		Workbook workbook = this.getWorkbookByType(request);
		IReportGenerator generator = this.getReportGenerator(request);
		generator.buildWorkbook(workbook, request);
        return workbook;
	}

	private Workbook getWorkbookByType(ReportRequest request) {
		switch (request.getType()) {
		case "opt1":
			return getWorkbookFromTemplate("report/opt1.xlsx");
		case "opt2":
		case "opt3":
			String orderType = request.getOrderType();
			if ("customer".equals(orderType)) {
				return getWorkbookFromTemplate("report/opt3.xlsx");
			}
			else if ("business".equals(orderType)) {
				return getWorkbookFromTemplate("report/opt4.xlsx");
			} else {
				return null;
			}
		default:
			return null;
		}
	}
	
	private IReportGenerator getReportGenerator(ReportRequest request) {
		for (IReportGenerator generate : reportGenerators) {
			if (generate.supports(request)) {
				return generate;
			}
		}
		return null;
	}
}
