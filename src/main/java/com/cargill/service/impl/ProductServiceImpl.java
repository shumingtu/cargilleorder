package com.cargill.service.impl;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cargill.dto.ProductDto;
import com.cargill.dto.ViewProductTPDrugDto;
import com.cargill.model.Product;
import com.cargill.model.ProductCUCY;
import com.cargill.model.parameter.TPDrugType;
import com.cargill.model.view.ViewProductTPDrug;
import com.cargill.repository.ProductCUCYRepository;
import com.cargill.repository.ProductRepository;
import com.cargill.repository.ViewProductTPDrugRepository;
import com.cargill.service.ProductService;
import com.cargill.transform.ProductTransform;
import com.cargill.transform.ViewProductTPDrugTransform;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ProductCUCYRepository productCUCYRepository;
    @Autowired
    private ViewProductTPDrugRepository viewProductTPDrugRepository;
    @Autowired
    private ProductTransform productTransform;
    @Autowired
    private ViewProductTPDrugTransform viewProductTPDrugTransform;

    @Override
    public List<ProductDto> getProductsByCodeOrName(String code, String name) {
        List<Product> productList = Collections.emptyList();
        if (StringUtils.isNotBlank(code)) {
            productList = productRepository.findByCodeStartingWith(code);
        } else if (StringUtils.isNotBlank(name)) {
            productList = productRepository.findByNameContaining(name);
        }

        return productTransform.toDtos(productList.stream().map(p -> {
            p.setCode(p.getCode().replaceAll("\\.0", ""));
            return p;
        }).collect(Collectors.toList()));
    }

    @Override
    public List<ProductCUCY> getProductsCUCY(List<String> codes) {
        return productCUCYRepository.findByProductCUCYPK_CodeIn(codes);
    }

    @Override
    public List<ViewProductTPDrugDto> getDrugs(String code, String name, TPDrugType type) {
        List<ViewProductTPDrug> drugList = Collections.emptyList();
        if (Optional.ofNullable(code).filter(it -> !it.isEmpty()).isPresent()) {
            drugList = viewProductTPDrugRepository.findByCodeStartingWithAndDrugType(code, type);
        } else if (Optional.ofNullable(name).isPresent()) {
            drugList = viewProductTPDrugRepository.findByNameContainingAndDrugType(name, type);
        }

        return drugList.stream().map(viewProductTPDrugTransform::toDto).collect(Collectors.toList());
    }
}
