package com.cargill.service.impl;

import com.cargill.dto.DeptDto;
import com.cargill.exception.CargillException;
import com.cargill.exception.ErrorCode;
import com.cargill.model.Dept;
import com.cargill.repository.DeptRepository;
import com.cargill.service.DeptService;
import com.cargill.transform.DeptTransform;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Optional;

@Service
public class DeptServiceImpl implements DeptService {

    @Autowired
    private DeptRepository deptRepository;
    @Autowired
    private DeptTransform deptTransform;

    @Override
    @Cacheable(value = "DeptCache")
    public List<DeptDto> getAllDepts() {
        List<Dept> deptList = Optional.ofNullable(deptRepository.findAll())
                .orElseThrow(() -> new CargillException(ErrorCode.NOT_FOUND));
        return deptTransform.toDtos(deptList);
    }
}
