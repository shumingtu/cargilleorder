package com.cargill.service.impl;

import com.cargill.Constants;
import com.cargill.dto.WebAccountDto;
import com.cargill.dto.request.ActiveUserRequest;
import com.cargill.exception.CargillException;
import com.cargill.exception.ErrorCode;
import com.cargill.model.User;
import com.cargill.repository.UserRepository;
import com.cargill.service.AuthService;
import com.cargill.transform.UserTransform;
import com.cargill.util.PasswordUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
@Slf4j
public class AuthServiceImpl implements AuthService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserTransform userTransform;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String account) throws UsernameNotFoundException {
        User user = Optional.ofNullable(userRepository.findByAccountAndStatus(account, "S"))
                .orElseThrow(() -> new UsernameNotFoundException(ErrorCode.LOGIN_FAILED.getDescription()));

        List<GrantedAuthority> list = new ArrayList<>();
        list.add(new SimpleGrantedAuthority(Constants.DEFAULT_ROLE_PREFIX + user.getDept()));

        return new org.springframework.security.core.userdetails.User(user.getId() + "", user.getPassword(), list);
    }


    public WebAccountDto activeUser(ActiveUserRequest request) throws CargillException {
        // 檢查密碼
        if (!request.getPassword().equals(request.getPassword2())) {
            log.warn("=== p1: " + request.getPassword() + ", p2: " + request.getPassword2());
            throw new CargillException(ErrorCode.ACTIVE_FAILED_INVALID_PASSWORD);
        }
        User user = Optional.ofNullable(userRepository.findByAccount(request.getUsername()))
                .orElseThrow(() -> new CargillException(ErrorCode.ACTIVE_FAILED_INVALID_USER));

        // 檢查啟動碼
        if (!request.getActiveCode().equals(user.getActCode())) {
            throw new CargillException(ErrorCode.ACTIVE_FAILED_INVALID_CODE);
        }

        // 檢查啟動日期
        Calendar today = Calendar.getInstance();
        if (Optional.ofNullable(user.getLastActCodeTime())
                .orElseThrow(() -> new CargillException(ErrorCode.ACTIVE_FAILED_INVALID_DATE))
                .getTimeInMillis() < today.getTimeInMillis()) {
            throw new CargillException(ErrorCode.ACTIVE_FAILED_INVALID_DATE);
        }

        // 更新密碼
        user.setPassword(PasswordUtil.encrytePassword(request.getPassword()));
        user.setStatus("S");
        userRepository.save(user);

        return userTransform.toDto(user);
    }
}
