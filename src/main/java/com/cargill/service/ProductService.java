package com.cargill.service;

import com.cargill.dto.ProductDto;
import com.cargill.dto.ProductTPDrugDto;
import com.cargill.dto.ViewProductTPDrugDto;
import com.cargill.model.ProductCUCY;
import com.cargill.model.ProductTPDrug;
import com.cargill.model.parameter.TPDrugType;

import java.util.List;

public interface ProductService {
    List<ProductDto> getProductsByCodeOrName(String code, String name);

    List<ProductCUCY> getProductsCUCY(List<String> codes);

    List<ViewProductTPDrugDto> getDrugs(String code, String name, TPDrugType type);
}
