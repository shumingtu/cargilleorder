package com.cargill.service.importdata;

import com.cargill.dto.ImportProcessResultDto;
import com.cargill.exception.CargillException;
import com.cargill.exception.ErrorCode;
import com.cargill.model.Customer;
import com.cargill.model.CustomerPK;
import com.cargill.repository.CustomerRepository;
import com.cargill.service.AbsImportService;
import com.google.common.collect.Streams;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
@Slf4j
public class CustomerDataImportServiceImpl extends AbsImportService {

    @Autowired
    CustomerRepository customerRepository;

    protected int headerCount() {
        return 1;
    }

    @Override
    protected ImportProcessResultDto processSheet(XSSFWorkbook workbook, IMPORT_TYPE type, UPDATE_MODE mode) {
        ImportProcessResultDto importResult = getImportProcessResult();
        Optional.ofNullable(workbook.getSheetAt(0)).ifPresent((s) -> {
            Streams.stream(s.iterator()).skip(headerCount())
                    .limit(importResult.getOriginalRowsCount())
                    .map((row) -> {
                        try {
                            CustomerPK pk = new CustomerPK();
                            String customerMainId = this.getCellValue(row.getCell(0)).get();
                            pk.setCustomerMainId(customerMainId);

                            String customerSubId = this.getCellValue(row.getCell(2)).get();
                            pk.setCustomerSubId(customerSubId);

                            String addressShipTo = this.getCellValue(row.getCell(4)).get();
                            pk.setAddressShipTo(addressShipTo);
                            /*
                            Optional.ofNullable(row.getCell(0))
                                    .filter(id -> id.getCellTypeEnum().equals(CellType.NUMERIC))
                                    .map((id) -> Double.valueOf(id.getNumericCellValue()).intValue())
                                    .ifPresent((id) -> pk.setCustomerMainId(String.valueOf(id)));

                            Optional.ofNullable(row.getCell(2))
                                    .filter(id -> id.getCellTypeEnum().equals(CellType.NUMERIC))
                                    .map((id) -> Double.valueOf(id.getNumericCellValue()).intValue())
                                    .ifPresent((id) -> pk.setCustomerSubId(String.valueOf(id)));

                            pk.setAddressShipTo(Optional.ofNullable(row.getCell(4))
                                    .filter(id -> id.getCellTypeEnum().equals(CellType.STRING))
                                    .map((id) -> id.getStringCellValue().trim()).orElseThrow(() -> new CargillException(ErrorCode.IMPORT_FILE_NULL_VALUE)));
                            */
                            Customer data = Optional.ofNullable(customerRepository.findOne(pk))
                                    .map((it) -> {
                                        it.setModifyTime(Calendar.getInstance());
                                        return it;
                                    }).orElse(new Customer());
                            data.setCustomerPk(pk);

                            Optional.ofNullable(row.getCell(7)).ifPresent((id) -> {
                                data.setAddress(id.getStringCellValue().trim());
                            });

                            Optional.ofNullable(row.getCell(1)).ifPresent((id) -> {
                                data.setCustomerMainName(id.getStringCellValue().trim());
                            });

                            Optional.ofNullable(row.getCell(3)).ifPresent((id) -> {
                                data.setCustomerSubName(id.getStringCellValue().trim());
                            });

                            Optional.ofNullable(row.getCell(5)).ifPresent((id) -> {
                                data.setPhone(id.getStringCellValue().trim());
                            });

                            data.setTmcode(this.getCellValue(row.getCell(6)).orElse(""));
                            
                            /* <201905> Add: 擴充SSCode及畜種欄位 start */
                            // sscode
                            data.setSscode(this.getCellValue(row.getCell(11)).orElse(""));
                            // 畜種
                            data.setLivestock(this.getCellValue(row.getCell(12)).orElse(""));
                            /* <201905> Add: 擴充SSCode及畜種欄位 end */

                            customerRepository.save(data);
                            importResult.setSuccessRowsCount(importResult.getSuccessRowsCount() + 1);
                        } catch (Exception ignore_e) {
                            log.error("=== Import CUSTOMER data failed (" + importResult.getProcessFileName() + "): " + ignore_e);
                            importResult.getFailedRows().add((long) row.getRowNum() + headerCount());
                            importResult.setFailedRowsCount(importResult.getFailedRowsCount() + 1);
                        }

                        return row;
                    }).collect(Collectors.toList());
        });
        importResult.setOriginalRowsCount(importResult.getOriginalRowsCount());
        importResult.setProcessResult(true);

        return importResult;
    }

    @Override
    protected void deleteAll(IMPORT_TYPE type) {
        customerRepository.deleteAll();
    }
}
