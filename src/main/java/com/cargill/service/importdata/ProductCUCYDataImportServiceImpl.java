package com.cargill.service.importdata;

import com.cargill.dto.ImportProcessResultDto;
import com.cargill.model.ProductCUCY;
import com.cargill.model.ProductCUCYPK;
import com.cargill.repository.ProductCUCYRepository;
import com.cargill.service.AbsImportService;
import com.google.common.collect.Streams;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ProductCUCYDataImportServiceImpl extends AbsImportService {

    @Autowired
    ProductCUCYRepository productCUCYRepository;

    private String previousCode;
    private String previousName;

    private String getCellValue(Cell column_code, Function<String, String> function) {
        String cellValue = "";
        try {
            cellValue = this.getCellValue(column_code).get();
        } catch (Exception ignored_e) {
        }

        return function.apply(cellValue);
    }

    protected int headerCount() {
        return 1;
    }

    @Override
    protected boolean checkValidWorkbook(XSSFWorkbook workbook, IMPORT_TYPE type) {
        return Optional.ofNullable(workbook.getSheetAt(0))
                .filter((s) -> 1 < s.getPhysicalNumberOfRows())
                .filter((s) -> 5 == s.getRow(0).getPhysicalNumberOfCells())
                .map((s) -> {
                    Row row = s.getRow(0);
                    return "產品編號".equals(row.getCell(0).getStringCellValue()) &&
                            "產品名稱".equals(row.getCell(1).getStringCellValue()) &&
                            "藥品編號".equals(row.getCell(2).getStringCellValue()) &&
                            "藥品名稱".equals(row.getCell(3).getStringCellValue()) &&
                            "每噸用量".equals(row.getCell(4).getStringCellValue());
                })
                .orElse(false);
    }

    @Override
    protected ImportProcessResultDto processSheet(XSSFWorkbook workbook, IMPORT_TYPE type, UPDATE_MODE mode) {
        ImportProcessResultDto importResult = getImportProcessResult();

        Optional.ofNullable(workbook.getSheetAt(0)).ifPresent((s) -> {
            Streams.stream(s.iterator()).skip(headerCount())
                    .limit(importResult.getOriginalRowsCount())
                    .map((row) -> {
                        try {
                            ProductCUCYPK pk = new ProductCUCYPK(
                                    this.getCellValue(row.getCell(0),
                                            cellValue ->
                                                    Optional.ofNullable(cellValue)
                                                            .filter(code -> !code.isEmpty())
                                                            .map((code) -> this.previousCode = code)
                                                            .orElse(this.previousCode)
                                    ),
                                    this.getCellValue(row.getCell(2)).get()
                            );

                            ProductCUCY data = Optional.ofNullable(productCUCYRepository.findOne(pk))
                                    .map((it) -> {
                                        it.setModifyTime(Calendar.getInstance());
                                        return it;
                                    }).orElse(new ProductCUCY());
                            data.setProductCUCYPK(pk);

                            String codeName = this.getCellValue(row.getCell(1),
                                    cellValue ->
                                            Optional.ofNullable(cellValue)
                                                    .filter(code -> !code.isEmpty())
                                                    .map((code) -> this.previousName = code.trim())
                                                    .orElse(this.previousName)
                            );
                            data.setName(codeName);

                            Optional.ofNullable(row.getCell(3)).ifPresent((id) ->
                                    data.setDrugName(id.getStringCellValue().trim())
                            );

                            Optional.ofNullable(row.getCell(4))
                                    .filter(id -> id.getCellTypeEnum().equals(CellType.NUMERIC))
                                    .map((id) -> id.getNumericCellValue())
                                    .ifPresent((id) -> data.setAmount(BigDecimal.valueOf(id)));

                            productCUCYRepository.save(data);
                            importResult.setSuccessRowsCount(importResult.getSuccessRowsCount() + 1);
                        } catch (Throwable ignore_e) {
                            log.error("=== Import ProductCUCY data failed (" + importResult.getProcessFileName() + "): " + ignore_e);
                            ignore_e.printStackTrace();
                            importResult.getFailedRows().add((long) row.getRowNum() + headerCount());
                            importResult.setFailedRowsCount(importResult.getFailedRowsCount() + 1);
                        }

                        return row;
                    }).collect(Collectors.toList());
        });

        importResult.setProcessResult(true);
        return importResult;
    }

    @Override
    protected void deleteAll(IMPORT_TYPE type) {
        productCUCYRepository.deleteAll();
    }
}
