package com.cargill.service.importdata;

import com.cargill.dto.ImportProcessResultDto;
import com.cargill.model.ProductTP;
import com.cargill.repository.ProductTPRepository;
import com.cargill.service.AbsImportService;
import com.google.common.collect.Streams;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ProductTPDataImportServiceImpl extends AbsImportService {

    @Autowired
    ProductTPRepository productTPRepository;

    protected int headerCount() {
        return 1;
    }

    @Override
    protected boolean checkIsNotBlank(Row row) {
        return (
                (null != row.getCell(0)) &&
                        (null != row.getCell(1)) &&
                        !CellType.BLANK.equals(row.getCell(0).getCellTypeEnum()) &&
                        !CellType.BLANK.equals(row.getCell(1).getCellTypeEnum())
        );
    }

    @Override
    protected boolean checkValidWorkbook(XSSFWorkbook workbook, IMPORT_TYPE type) {
        return Optional.ofNullable(workbook.getSheetAt(0))
                .filter((s) -> 1 < s.getPhysicalNumberOfRows())
                .filter((s) -> 3 == s.getRow(1).getPhysicalNumberOfCells())
                .map((s) -> {
                    Row row = s.getRow(0);
                    return "CODE".equalsIgnoreCase(row.getCell(0).getStringCellValue()) &&
                            "NAME".equals(row.getCell(1).getStringCellValue()) &&
                            "價格".equals(row.getCell(2).getStringCellValue());
                }).orElse(false);
    }

    @Override
    protected ImportProcessResultDto processSheet(XSSFWorkbook workbook, IMPORT_TYPE type, UPDATE_MODE mode) {
        ImportProcessResultDto importResult = getImportProcessResult();

        Optional.ofNullable(workbook.getSheetAt(0)).ifPresent((s) -> {
            Streams.stream(s.iterator()).skip(headerCount())
                    .limit(importResult.getOriginalRowsCount())
                    .map((row) -> {
                        try {
                            String code = this.getCellValue(row.getCell(0)).get();
                            ProductTP data = Optional.ofNullable(productTPRepository.findOne(code))
                                    .map((it) -> {
                                        it.setModifyTime(Calendar.getInstance());
                                        return it;
                                    }).orElse(new ProductTP());
                            data.setCode(code);

                            Optional.ofNullable(row.getCell(1)).ifPresent((id) -> {
                                data.setName(id.getStringCellValue().trim());
                            });


                            Optional.ofNullable(row.getCell(2)).ifPresent((id) -> {
                                if (CellType.NUMERIC.equals(id.getCellTypeEnum())) {
                                    data.setPrice(BigDecimal.valueOf(id.getNumericCellValue()));
                                }else{
                                    data.setPrice(BigDecimal.ZERO);
                                }

                            });

                            productTPRepository.save(data);
                            importResult.setSuccessRowsCount(importResult.getSuccessRowsCount() + 1);
                        } catch (Exception ignore_e) {
                            log.error("=== Import ProductTP data " + importResult.getProcessFileName() + " failed: " + ignore_e.toString());
                            importResult.getFailedRows().add((long) row.getRowNum() + headerCount());
                            importResult.setFailedRowsCount(importResult.getFailedRowsCount() + 1);
                        }

                        return row;
                    }).collect(Collectors.toList());
        });

        importResult.setProcessResult(true);
        return importResult;

    }

    @Override
    protected void deleteAll(IMPORT_TYPE type) {
        productTPRepository.deleteAll();
    }
}
