package com.cargill.service.importdata;

import com.cargill.dto.ImportProcessResultDto;
import com.cargill.exception.CargillException;
import com.cargill.exception.ErrorCode;
import com.cargill.model.ProductTPDrug;
import com.cargill.model.parameter.TPAreaCode;
import com.cargill.model.parameter.TPAreaType;
import com.cargill.model.parameter.TPDrugType;
import com.cargill.repository.ProductTPDrugRepository;
import com.cargill.service.AbsImportService;
import com.google.common.collect.Streams;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ProductTPDrugDataImportServiceImpl extends AbsImportService {

    @Autowired
    ProductTPDrugRepository productTPDrugRepository;

    protected int   headerCount() {
        return 1;
    }
    
    @Override
    protected boolean checkIsNotBlank(Row row) {
    	 String code = row.getCell(0) == null  ? null : row.getCell(0).toString();
         String name = row.getCell(1)== null ? null : row.getCell(1).toString();
         
         if(StringUtils.isBlank(code) && StringUtils.isBlank(name)) {
         	return false;
         }
         
        return true;
    }

    
    @Override
    protected boolean checkValidWorkbook(XSSFWorkbook workbook, IMPORT_TYPE type) {
        return Optional.ofNullable(workbook.getSheetAt(0))
                .filter((s) -> 1 < s.getPhysicalNumberOfRows())
                .filter((s) -> 6 == s.getRow(1).getPhysicalNumberOfCells())
                .map((s) -> {
                    Row row = s.getRow(0);
                    return "CODE".equalsIgnoreCase(row.getCell(0).getStringCellValue()) &&
                            "原料名稱".equals(row.getCell(1).getStringCellValue().replaceAll(" ", "")) &&
                            "建議用量".equals(row.getCell(2).getStringCellValue()) &&
                            "最高劑量".equals(row.getCell(3).getStringCellValue()) &&
                            "停藥期".equals(row.getCell(4).getStringCellValue()) &&
                            "備註".equals(row.getCell(5).getStringCellValue());
                }).orElse(false);
    }

    @Override
    protected ImportProcessResultDto processSheet(XSSFWorkbook workbook, IMPORT_TYPE type, UPDATE_MODE mode) {
        ImportProcessResultDto importResult = getImportProcessResult();

        Optional.ofNullable(workbook.getSheetAt(0)).ifPresent((s) -> {
            Streams.stream(s.iterator()).skip(headerCount())
                    .limit(importResult.getOriginalRowsCount())
                    .map((row) -> {
                        try {
                        	String code = row.getCell(0) == null ? null : this.getCellValue(row.getCell(0)).get();
	                        String name = row.getCell(1) == null ? null : this.getCellValue(row.getCell(1)).get();
                            
                            if(StringUtils.isBlank(code) || StringUtils.isBlank(name)) {
                            	throw new Exception("code:" + code + ", name:" + name);
                            }
                            
                            log.info("code:{}", code);
                            
                            String complexCode = "";
                            switch (type) {
                                case PRODUCT_TP_TC_COMPANY_TWENTY:
                                    complexCode = TPAreaCode.TWENTY.toString() + "_" +
                                            TPAreaType.TC.toString() + "_" +
                                            TPDrugType.COMPANY.toString() + "_" + code;
                                    break;
                                case PRODUCT_TP_TC_SELF_TWENTY:
                                    complexCode = TPAreaCode.TWENTY.toString() + "_" +
                                            TPAreaType.TC.toString() + "_" +
                                            TPDrugType.SELF.toString() + "_" + code;
                                    break;
                                case PRODUCT_TP_KS_COMPANY_NULL:
                                    complexCode = TPAreaCode.NULL.toString() + "_" +
                                            TPAreaType.KS.toString() + "_" +
                                            TPDrugType.COMPANY.toString() + "_" + code;
                                    break;
                                case PRODUCT_TP_KS_SELF_40:
                                    complexCode = TPAreaCode.FOURTY.toString() + "_" +
                                            TPAreaType.KS.toString() + "_" +
                                            TPDrugType.SELF.toString() + "_" + code;
                                    break;
                                case PRODUCT_TP_KS_SELF_50:
                                    complexCode = TPAreaCode.FIFTY.toString() + "_" +
                                            TPAreaType.KS.toString() + "_" +
                                            TPDrugType.SELF.toString() + "_" + code;
                                    break;
                                case PRODUCT_TP_KS_SELF_60:
                                    complexCode = TPAreaCode.SIXTY.toString() + "_" +
                                            TPAreaType.KS.toString() + "_" +
                                            TPDrugType.SELF.toString() + "_" + code;
                                    break;
                            }


                            ProductTPDrug data = Optional.ofNullable(productTPDrugRepository.findOne(complexCode))
                                    .map((it) -> {
                                        it.setModifyTime(Calendar.getInstance());
                                        return it;
                                    }).orElse(new ProductTPDrug());
                            data.setComplexCode(complexCode);
                            data.setCode(code);

                            Optional.ofNullable(row.getCell(1)).ifPresent((id) -> {
                                data.setName(id.getStringCellValue().trim());
                            });

                            Optional.ofNullable(row.getCell(2)).ifPresent((id) -> {
                                if (CellType.NUMERIC.equals(id.getCellTypeEnum())) {
                                    data.setRecommendedAmount(Double.valueOf(id.getNumericCellValue()).toString());
                                } else if (CellType.STRING.equals(id.getCellTypeEnum())) {
                                    data.setRecommendedAmount(id.getStringCellValue().trim());
                                } else {
                                    data.setRecommendedAmount("");
                                }
                            });

                            Optional.ofNullable(row.getCell(3)).ifPresent((id) -> {
                                if (CellType.NUMERIC.equals(id.getCellTypeEnum())) {
                                    data.setMaxAmount(Double.valueOf(id.getNumericCellValue()).toString());
                                } else if (CellType.STRING.equals(id.getCellTypeEnum())) {
                                    data.setMaxAmount(id.getStringCellValue().trim());
                                } else {
                                    data.setMaxAmount("");
                                }
                            });

                            Optional.ofNullable(row.getCell(4)).ifPresent((id) -> {
                                data.setDrugWithdrawal(id.getStringCellValue().trim());
                            });

                            Optional.ofNullable(row.getCell(5)).ifPresent((id) -> {
                                data.setNote(id.getStringCellValue().trim());
                            });

                            switch (type) {
                                case PRODUCT_TP_TC_COMPANY_TWENTY:
                                    data.setAreaType(TPAreaType.TC);
                                    data.setDrugType(TPDrugType.COMPANY);
                                    data.setAreaCode(TPAreaCode.TWENTY);
                                    break;
                                case PRODUCT_TP_TC_SELF_TWENTY:
                                    data.setAreaType(TPAreaType.TC);
                                    data.setDrugType(TPDrugType.SELF);
                                    data.setAreaCode(TPAreaCode.TWENTY);
                                    break;
                                case PRODUCT_TP_KS_COMPANY_NULL:
                                    data.setAreaType(TPAreaType.KS);
                                    data.setDrugType(TPDrugType.COMPANY);
                                    data.setAreaCode(TPAreaCode.NULL);
                                    break;
                                case PRODUCT_TP_KS_SELF_40:
                                    data.setAreaType(TPAreaType.KS);
                                    data.setDrugType(TPDrugType.SELF);
                                    data.setAreaCode(TPAreaCode.FOURTY);
                                    break;
                                case PRODUCT_TP_KS_SELF_50:
                                    data.setAreaType(TPAreaType.KS);
                                    data.setDrugType(TPDrugType.SELF);
                                    data.setAreaCode(TPAreaCode.FIFTY);
                                    break;
                                case PRODUCT_TP_KS_SELF_60:
                                    data.setAreaType(TPAreaType.KS);
                                    data.setDrugType(TPDrugType.SELF);
                                    data.setAreaCode(TPAreaCode.SIXTY);
                                    break;
                            }

                            productTPDrugRepository.save(data);
                            importResult.setSuccessRowsCount(importResult.getSuccessRowsCount() + 1);
                        } catch (Exception ignore_e) {
                            log.error("=== Import ProductTPDrug data " + importResult.getProcessFileName() + " failed: " + ignore_e.toString());
                            importResult.getFailedRows().add((long) row.getRowNum() + headerCount());
                            importResult.setFailedRowsCount(importResult.getFailedRowsCount() + 1);
                        }

                        return row;
                    }).collect(Collectors.toList());
        });

        importResult.setProcessResult(true);
        return importResult;

    }

    protected final ProductTPDrug splitAndSetProductTPDrugType(IMPORT_TYPE importType, ProductTPDrug data) {
        String[] typeArray = importType.toString().split("_");
        data.setAreaType(typeArray[2].equals("KS") ? TPAreaType.KS : TPAreaType.TC);
        data.setDrugType(typeArray[3].equals("COMPANY") ? TPDrugType.COMPANY : TPDrugType.SELF);
        if (typeArray.length == 5) {
            if (typeArray[3].equals("40")) {
                data.setAreaCode(TPAreaCode.FOURTY);
            } else if (typeArray[3].equals("50")) {
                data.setAreaCode(TPAreaCode.FIFTY);
            } else if (typeArray[3].equals("60")) {
                data.setAreaCode(TPAreaCode.SIXTY);
            }
        }
        return data;
    }



    @Override
    protected void deleteAll(IMPORT_TYPE type) {
        TPAreaType areaType = null;
        TPDrugType drugType = null;
        TPAreaCode areaCode = null;
        switch (type) {
            case PRODUCT_TP_TC_COMPANY_TWENTY:
                areaType = TPAreaType.TC;
                drugType = TPDrugType.COMPANY;
                areaCode = TPAreaCode.TWENTY;
                break;
            case PRODUCT_TP_TC_SELF_TWENTY:
                areaType = TPAreaType.TC;
                drugType = TPDrugType.SELF;
                areaCode = TPAreaCode.TWENTY;
                break;
            case PRODUCT_TP_KS_COMPANY_NULL:
                areaType = TPAreaType.KS;
                drugType = TPDrugType.COMPANY;
                areaCode = TPAreaCode.NULL;
                break;
            case PRODUCT_TP_KS_SELF_40:
                areaType = TPAreaType.KS;
                drugType = TPDrugType.SELF;
                areaCode = TPAreaCode.FOURTY;
                break;
            case PRODUCT_TP_KS_SELF_50:
                areaType = TPAreaType.KS;
                drugType = TPDrugType.SELF;
                areaCode = TPAreaCode.FIFTY;
                break;
            case PRODUCT_TP_KS_SELF_60:
                areaType = TPAreaType.KS;
                drugType = TPDrugType.SELF;
                areaCode = TPAreaCode.SIXTY;
                break;
        }

        log.info("=== type: " + areaType + ", code: " + areaCode + ", drug: " + drugType);

        if(null == areaCode){
            productTPDrugRepository.deleteByAreaAndDrug(areaType,drugType);
        }else{
            productTPDrugRepository.deleteByAllType(areaType,drugType,areaCode);
        }

    }
}
