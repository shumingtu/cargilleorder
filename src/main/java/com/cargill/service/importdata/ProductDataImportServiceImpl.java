package com.cargill.service.importdata;

import com.cargill.dto.ImportProcessResultDto;
import com.cargill.model.Product;
import com.cargill.repository.ProductRepository;
import com.cargill.service.AbsImportService;
import com.google.common.collect.Streams;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ProductDataImportServiceImpl extends AbsImportService {

    @Autowired
    ProductRepository productRepository;

    protected int headerCount() {
        return 1;
    }

    @Override
    protected boolean checkValidWorkbook(XSSFWorkbook workbook, IMPORT_TYPE type) {
        return Optional.ofNullable(workbook.getSheetAt(0))
                .filter((s) -> 1 < s.getPhysicalNumberOfRows())
                .filter((s) -> 13 == s.getRow(1).getPhysicalNumberOfCells())
                .map((s) -> {
                    Row row = s.getRow(0);
                    return "code".equalsIgnoreCase(row.getCell(0).getStringCellValue()) &&
                            "品名".equals(row.getCell(1).getStringCellValue()) &&
                            "使用階段".equals(row.getCell(2).getStringCellValue()) &&
                            "價格(散裝/X)".equals(row.getCell(3).getStringCellValue());
                }).orElse(false);
    }

    @Override
    protected ImportProcessResultDto processSheet(XSSFWorkbook workbook, IMPORT_TYPE type, UPDATE_MODE mode) {
        ImportProcessResultDto importResult = getImportProcessResult();

        Optional.ofNullable(workbook.getSheetAt(0)).ifPresent((s) -> {
            Streams.stream(s.iterator()).skip(headerCount())
                    .limit(importResult.getOriginalRowsCount())
                    .map((row) -> {
                        try {
                            String code = this.getCellValue(row.getCell(0)).get();
                            Product data = Optional.ofNullable(productRepository.findOne(code))
                                    .map((it) -> {
                                        it.setModifyTime(Calendar.getInstance());
                                        return it;
                                    }).orElse(new Product());
                            data.setCode(code);

                            Optional.ofNullable(row.getCell(1)).ifPresent((id) -> {
                                data.setName(id.getStringCellValue().trim());
                            });

                            Optional.ofNullable(row.getCell(2)).ifPresent((id) -> {
                                data.setPhase(id.getStringCellValue().trim());
                            });

                            Optional.ofNullable(row.getCell(3)).ifPresent((id) -> {
                                if (CellType.NUMERIC.equals(id.getCellTypeEnum())) {
                                    data.setPriceX(BigDecimal.valueOf(id.getNumericCellValue()));
                                }else{
                                    data.setPriceX(BigDecimal.ZERO);
                                }

                            });

                            Optional.ofNullable(row.getCell(4)).ifPresent((id) -> {
                                if (CellType.NUMERIC.equals(id.getCellTypeEnum())) {
                                    data.setPrice3kg3K(BigDecimal.valueOf(id.getNumericCellValue()));
                                }else{
                                    data.setPrice3kg3K(BigDecimal.ZERO);
                                }

                            });

                            Optional.ofNullable(row.getCell(5)).ifPresent((id) -> {
                                if (CellType.NUMERIC.equals(id.getCellTypeEnum())) {
                                    data.setPrice10kg1(BigDecimal.valueOf(id.getNumericCellValue()));
                                }else{
                                    data.setPrice10kg1(BigDecimal.ZERO);
                                }

                            });

                            Optional.ofNullable(row.getCell(6)).ifPresent((id) -> {
                                if (CellType.NUMERIC.equals(id.getCellTypeEnum())) {
                                    data.setPrice20kg2(BigDecimal.valueOf(id.getNumericCellValue()));
                                }else{
                                    data.setPrice20kg2(BigDecimal.ZERO);
                                }

                            });

                            Optional.ofNullable(row.getCell(7)).ifPresent((id) -> {
                                if (CellType.NUMERIC.equals(id.getCellTypeEnum())) {
                                    data.setPrice25kg2point5C(BigDecimal.valueOf(id.getNumericCellValue()));
                                }else{
                                    data.setPrice25kg2point5C(BigDecimal.ZERO);
                                }

                            });

                            Optional.ofNullable(row.getCell(8)).ifPresent((id) -> {
                                if (CellType.NUMERIC.equals(id.getCellTypeEnum())) {
                                    data.setPrice30kg3(BigDecimal.valueOf(id.getNumericCellValue()));
                                }else{
                                    data.setPrice30kg3(BigDecimal.ZERO);
                                }

                            });

                            Optional.ofNullable(row.getCell(9)).ifPresent((id) -> {
                                if (CellType.NUMERIC.equals(id.getCellTypeEnum())) {
                                    data.setPrice30kg3C(BigDecimal.valueOf(id.getNumericCellValue()));
                                }else{
                                    data.setPrice30kg3C(BigDecimal.ZERO);
                                }

                            });

                            Optional.ofNullable(row.getCell(10)).ifPresent((id) -> {
                                if (CellType.NUMERIC.equals(id.getCellTypeEnum())) {
                                    data.setPrice50kg5(BigDecimal.valueOf(id.getNumericCellValue()));
                                }else{
                                    data.setPrice50kg5(BigDecimal.ZERO);
                                }

                            });

                            Optional.ofNullable(row.getCell(11)).ifPresent((id) -> {
                                if (CellType.NUMERIC.equals(id.getCellTypeEnum())) {
                                    data.setPrice1000kgJ(BigDecimal.valueOf(id.getNumericCellValue()));
                                }else{
                                    data.setPrice1000kgJ(BigDecimal.ZERO);
                                }

                            });

                            Optional.ofNullable(row.getCell(12)).ifPresent((id) -> {
                                if (CellType.NUMERIC.equals(id.getCellTypeEnum())) {
                                    data.setPrice500kgJ5(BigDecimal.valueOf(id.getNumericCellValue()));
                                }else{
                                    data.setPrice500kgJ5(BigDecimal.ZERO);
                                }

                            });

                            productRepository.save(data);
                            importResult.setSuccessRowsCount(importResult.getSuccessRowsCount() + 1L);
                        } catch (Exception ignore_e) {
                            log.error("=== Import Product data " + importResult.getProcessFileName() + " failed: " + ignore_e.toString());
                            importResult.getFailedRows().add((long) row.getRowNum() + headerCount());
                            importResult.setFailedRowsCount(importResult.getFailedRowsCount() + 1L);

                        }

                        return row;
                    }).collect(Collectors.toList());
        });

        importResult.setProcessResult(true);
        return importResult;
    }

    @Override
    protected void deleteAll(IMPORT_TYPE type) {
        productRepository.deleteAll();
    }
}
