package com.cargill.service.importdata;

import com.cargill.dto.ImportProcessResultDto;
import com.cargill.model.CustomerFeedingscale;
import com.cargill.repository.CustomerFeedingscaleRepository;
import com.cargill.service.AbsImportService;
import com.google.common.collect.Streams;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
@Slf4j
public class CustomerDataFeedingscaleImportServiceImpl extends AbsImportService {

    @Autowired
    CustomerFeedingscaleRepository customerFeedingscaleRepository;

    protected int headerCount() {
        return 1;
    }

    @Override
    protected boolean checkValidWorkbook(XSSFWorkbook workbook, IMPORT_TYPE type) {
        return Optional.ofNullable(workbook.getSheetAt(0))
                .filter((s) -> 1 < s.getPhysicalNumberOfRows())
                .filter((s) -> 4 == s.getRow(0).getPhysicalNumberOfCells())
                .map((s) -> {
                    Row row = s.getRow(0);
                    return "客戶編號".equals(row.getCell(0).getStringCellValue()) &&
                            "客戶姓名".equals(row.getCell(1).getStringCellValue()) &&
                            "飼養規模".equals(row.getCell(2).getStringCellValue()) &&
                            "特配添加注意事項".equals(row.getCell(3).getStringCellValue());
                })
                .orElse(false);
    }

    @Override
    protected boolean checkIsNotBlank(Row row) {
        boolean check1 = (
                (null != row.getCell(0)) &&
                        (null != row.getCell(1)) &&
                        !CellType.BLANK.equals(row.getCell(0).getCellTypeEnum()) &&
                        !CellType.BLANK.equals(row.getCell(1).getCellTypeEnum()) &&
                        (null != row.getCell(0)) &&
                        (null != row.getCell(1))
        );

        if (!check1) {
            check1 = (
                    (null != row.getCell(2)) &&
                            !CellType.BLANK.equals(row.getCell(2).getCellTypeEnum()) &&
                            !row.getCell(2).getStringCellValue().isEmpty()
            );
        }

        return check1;
    }

    @Override
    protected ImportProcessResultDto processSheet(XSSFWorkbook workbook, IMPORT_TYPE type, UPDATE_MODE mode) {
        ImportProcessResultDto importResult = getImportProcessResult();

        Optional.ofNullable(workbook.getSheetAt(0)).ifPresent((s) -> {
            Streams.stream(s.iterator()).skip(headerCount())
                    .limit(importResult.getOriginalRowsCount())
                    .map((row) -> {
                        try {
                            String customerId = this.getCellValue(row.getCell(0)).get();
                            CustomerFeedingscale data = Optional.ofNullable(customerFeedingscaleRepository.findOne(customerId))
                                    .map((it) -> {
                                        it.setModifyTime(Calendar.getInstance());
                                        return it;
                                    }).orElse(new CustomerFeedingscale());

                            data.setCustomerId(customerId);
                            Optional.ofNullable(row.getCell(1)).ifPresent((id) -> {
                                data.setCustomerName(id.getStringCellValue().trim());
                            });

                            Optional.ofNullable(row.getCell(1)).ifPresent((id) -> {
                                data.setCustomerName(id.getStringCellValue().trim());
                            });

                            Optional.ofNullable(row.getCell(2)).ifPresent((id) -> {
                                if (CellType.NUMERIC.equals(id.getCellTypeEnum())) {
                                    data.setFeedingScale(BigDecimal.valueOf(id.getNumericCellValue()).intValue() + "");
                                } else {
                                    data.setFeedingScale(id.getStringCellValue());
                                }
                            });

                            Optional.ofNullable(row.getCell(3)).ifPresent((id) -> {
                                data.setNote(id.getStringCellValue().trim());
                            });

                            customerFeedingscaleRepository.save(data);
                            importResult.setSuccessRowsCount(importResult.getSuccessRowsCount() + 1);
                        } catch (Exception ignore_e) {
                            log.error("=== Import ProductTP data " + importResult.getProcessFileName() + " failed: " + ignore_e.toString());
                            ignore_e.printStackTrace();
                            importResult.getFailedRows().add((long) row.getRowNum() + 1);
                            importResult.setFailedRowsCount(importResult.getFailedRowsCount() + 1);
                        }

                        return row;
                    }).collect(Collectors.toList());
        });
        importResult.setProcessResult(true);
        return importResult;
    }

    @Override
    protected void deleteAll(IMPORT_TYPE type) {
        customerFeedingscaleRepository.deleteAll();
    }
}
