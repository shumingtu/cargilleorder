package com.cargill.service.importdata;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import com.cargill.dto.CustomerDto;
import com.cargill.dto.ImportProcessResultDto;
import com.cargill.dto.WebAccountDto;
import com.cargill.exception.CargillException;
import com.cargill.exception.ErrorCode;
import com.cargill.model.Customer;
import com.cargill.model.CustomerQuota;
import com.cargill.model.DrugInventory;
import com.cargill.repository.CustomerQuotaRepository;
import com.cargill.repository.DrugInventoryRepository;
import com.cargill.service.AbsImportService;
import com.cargill.service.CustomerService;
import com.cargill.service.UserService;
import com.cargill.util.DateUtil;
import com.google.common.collect.Streams;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
@Slf4j
public class DrugInventoryImportServiceImpl extends AbsImportService {

    @Autowired
    DrugInventoryRepository drugInventoryRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private CustomerService customerService;

    private static final String FORMAT_DATETIME = "YYYY-MM-dd HH:mm";


    protected int headerCount() {
        return 1;
    }

    @Override
    protected boolean checkValidWorkbook(XSSFWorkbook workbook, IMPORT_TYPE type) {

        return Optional.ofNullable(workbook.getSheetAt(0))
                .filter((s) -> 1 < s.getPhysicalNumberOfRows())
                .filter((s) -> 6 == s.getRow(0).getPhysicalNumberOfCells())
                .map((s) -> {
                    Row row = s.getRow(0);
                    return "客戶主號".equals(row.getCell(0).getStringCellValue()) &&
                            "CODE".equals(row.getCell(1).getStringCellValue()) &&
                            "自 備 名 稱".equals(row.getCell(2).getStringCellValue()) &&
                            "庫存 (KG)".equals(row.getCell(3).getStringCellValue())&&
                            "分類".equals(row.getCell(4).getStringCellValue())&&
                            "上次進貨時間".equals(row.getCell(5).getStringCellValue()) ;
                })
                .orElse(false);
    }

    @Override
    protected ImportProcessResultDto processSheet(XSSFWorkbook workbook, IMPORT_TYPE type, UPDATE_MODE mode) {
        ImportProcessResultDto importResult = getImportProcessResult();

        Calendar date = Calendar.getInstance();
        date.setTime(DateUtil.now());
        String loginUserId = SecurityContextHolder.getContext().getAuthentication().getName();
        String loginUserName = Optional.ofNullable(userService.getWebAccountById(loginUserId))
                .map(WebAccountDto::getUserName).orElse("");



        Optional.ofNullable(workbook.getSheetAt(0)).ifPresent((s) -> {
            Streams.stream(s.iterator()).skip(headerCount())
                    .limit(importResult.getOriginalRowsCount())
                    .map((row) -> {
                        try {
                            String errMsg = "";

                            DrugInventory data = Optional.ofNullable(drugInventoryRepository.
                                    findByCustomerSubIdAndCodeAndStatus(
                                            BigDecimal.valueOf(row.getCell(0).getNumericCellValue()).intValue() + ""
                                    , row.getCell(1).getStringCellValue()
                                    , "1")).orElse(new DrugInventory());

                            if(null == data.getId()){
                                data = new DrugInventory();

                                if (CellType.NUMERIC.equals(row.getCell(0).getCellTypeEnum())  ) {

                                    data.setCustomerSubId(BigDecimal.valueOf(row.getCell(0).getNumericCellValue()).intValue() + "");
                                } else {
                                    data.setCustomerSubId(row.getCell(0).getStringCellValue());
                                }
//

                                if (StringUtils.isBlank(data.getCustomerSubId())) {
                                    throw new Exception("id is null");
                                }

                            //    List<CustomerDto> customerDtos = customerService.findBySubId(data.getCustomerSubId());
                        //        if (customerDtos.isEmpty()) {
                         //           log.error("=== Import drugInventory data 找不到使用者:"+data.getCustomerSubId());
                          //          throw new Exception();
                          //      }else{
                                    data.setCustomerSubName(data.getCustomerSubId());
                              //  }

                                if (CellType.NUMERIC.equals(row.getCell(1).getCellTypeEnum())) {
                                    data.setCode(BigDecimal.valueOf(row.getCell(1).getNumericCellValue()).intValue() + "");
                                } else {
                                    data.setCode(row.getCell(1).getStringCellValue());
                                }

                            }

                            data.setName(row.getCell(2).getStringCellValue().trim());

                            if (CellType.NUMERIC.equals(row.getCell(3).getCellTypeEnum())) {

                                double amountValue = BigDecimal.valueOf(   row.getCell(3).getNumericCellValue()).doubleValue();
                                String amountInsertValue = "";
                                if( amountValue == (int) amountValue ) {
                                    amountInsertValue = BigDecimal.valueOf(   row.getCell(3).getNumericCellValue()).intValue()+"";
                                }else {
                                    amountInsertValue = BigDecimal.valueOf(   row.getCell(3).getNumericCellValue()).doubleValue()+"";

                                }


                                data.setAmount( amountInsertValue);
//                                data.setAmount(    BigDecimal.valueOf(   row.getCell(3).getNumericCellValue()).setScale(3).doubleValue()+"");
                            } else {
                                data.setAmount(row.getCell(3).getStringCellValue().trim());
                            }


                            data.setType(row.getCell(4).getStringCellValue().trim());


                            data.setStatus("1");
                            data.setUnit("KG");
                            data.setCreateUser(loginUserId);
                            data.setCreateUserName(loginUserName);
                            data.setCreateTime(date);
                            drugInventoryRepository.save(data);
                            importResult.setSuccessRowsCount(importResult.getSuccessRowsCount() + 1);
                        }catch (Exception ignore_e) {
                            log.error("=== Import drugInventory data " + importResult.getProcessFileName() + " failed: " + ignore_e.toString());
                            ignore_e.printStackTrace();
                            importResult.getFailedRows().add((long) row.getRowNum() + headerCount());
                            importResult.setFailedRowsCount(importResult.getFailedRowsCount() + 1);
                        }

                        return row;
                    }).collect(Collectors.toList());
        });
        //執行完成更新所有資料
        drugInventoryRepository.updateCreateDateByStatusOne();
        importResult.setProcessResult(true);
        return importResult;
    }

    @Override
    protected void deleteAll(IMPORT_TYPE type) {
       // drugInventoryRepository.deleteAll();
        List<DrugInventory> datas =  drugInventoryRepository.findByStatus("1");
        for(DrugInventory data : datas){
            data.setStatus("2");
            drugInventoryRepository.save(data);
        }
    }
}
