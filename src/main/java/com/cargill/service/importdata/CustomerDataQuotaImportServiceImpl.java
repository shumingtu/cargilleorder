package com.cargill.service.importdata;

import com.cargill.dto.ImportProcessResultDto;
import com.cargill.model.CustomerQuota;
import com.cargill.repository.CustomerQuotaRepository;
import com.cargill.service.AbsImportService;
import com.google.common.collect.Streams;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
@Slf4j
public class CustomerDataQuotaImportServiceImpl extends AbsImportService {

    @Autowired
    CustomerQuotaRepository customerQuotaRepository;

    protected int headerCount() {
        return 1;
    }

    @Override
    protected boolean checkValidWorkbook(XSSFWorkbook workbook, IMPORT_TYPE type) {
        return Optional.ofNullable(workbook.getSheetAt(0))
                .filter((s) -> 1 < s.getPhysicalNumberOfRows())
                .filter((s) -> 3 == s.getRow(0).getPhysicalNumberOfCells())
                .map((s) -> {
                    Row row = s.getRow(0);
                    return "客戶編號".equals(row.getCell(0).getStringCellValue()) &&
                            "客戶姓名".equals(row.getCell(1).getStringCellValue()) &&
                            "訂單注意事項".equals(row.getCell(2).getStringCellValue());
                })
                .orElse(false);
    }

    @Override
    protected ImportProcessResultDto processSheet(XSSFWorkbook workbook, IMPORT_TYPE type, UPDATE_MODE mode) {
        ImportProcessResultDto importResult = getImportProcessResult();

        Optional.ofNullable(workbook.getSheetAt(0)).ifPresent((s) -> {
            Streams.stream(s.iterator()).skip(headerCount())
                    .limit(importResult.getOriginalRowsCount())
                    .map((row) -> {
                        try {


                            String customerId = this.getCellValue(row.getCell(0)).get();

                            CustomerQuota data = Optional.ofNullable(customerQuotaRepository.findOne(customerId))
                                    .map((it) -> {
                                        it.setModifyTime(Calendar.getInstance());
                                        return it;
                                    }).orElse(new CustomerQuota());

                            data.setCustomerId(customerId);
                            log.info("===setCustomerId " + data.getCustomerId());
                            Optional.ofNullable(row.getCell(1)).ifPresent((id) -> {
                                data.setCustomerName(id.getStringCellValue().trim());
                            });

                            Optional.ofNullable(row.getCell(2)).ifPresent((id) -> {
                                data.setNote(id.getStringCellValue().trim());
                            });

                            customerQuotaRepository.save(data);
                            importResult.setSuccessRowsCount(importResult.getSuccessRowsCount() + 1);
                        } catch (Exception ignore_e) {
                            log.error("=== Import ProductTP data " + importResult.getProcessFileName() + " failed: " + ignore_e.toString());
                            ignore_e.printStackTrace();
                            importResult.getFailedRows().add((long) row.getRowNum() + headerCount());
                            importResult.setFailedRowsCount(importResult.getFailedRowsCount() + 1);
                        }

                        return row;
                    }).collect(Collectors.toList());
        });
        importResult.setProcessResult(true);
        return importResult;
    }

    @Override
    protected void deleteAll(IMPORT_TYPE type) {
        customerQuotaRepository.deleteAll();
    }
}
