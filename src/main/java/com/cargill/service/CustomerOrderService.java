package com.cargill.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.cargill.dto.A2Dto;
import com.cargill.dto.CustomerOrderDto;
import com.cargill.dto.TmcodeGroupDto;
import com.cargill.dto.request.CustomerOrderUpdateRequest;
import com.cargill.dto.request.ReportRequest;
import com.cargill.dto.request.SearchA2Request;
import com.cargill.dto.request.SearchCustomerOrderRequest;
import com.cargill.model.CustomerOrder;

public interface CustomerOrderService {

    CustomerOrder getOrder(String order_no);

    Page<CustomerOrderDto> findByAppAndTmcodes(List<TmcodeGroupDto> tmcodeGroupDtoList, Pageable pageRequest);

    Page<CustomerOrderDto> findAllCustomerOrder(SearchCustomerOrderRequest condition, Pageable pageable);

    CustomerOrderDto createOrUpdateOrder(CustomerOrderUpdateRequest request, String order_person);
    
    void afterCreateOrUpdateOrder(CustomerOrderDto dto);

    void updateOrderIsDeleteToY(CustomerOrder order);

    CustomerOrderDto duplicateOrder(CustomerOrder order, String person);

    void removeOrder(CustomerOrder order);


    List<A2Dto> buildA2(SearchA2Request request);
    
    List<CustomerOrder> getReportData(ReportRequest request);
}
