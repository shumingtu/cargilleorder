package com.cargill.service;

import java.math.BigInteger;
import java.util.List;

import com.cargill.model.SchedulerNotification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.cargill.dto.NotificationCustomerDto;

public interface SchedulerManagerService {

	public Page<NotificationCustomerDto> getNotifictionList(Pageable pageable);
	
	public boolean cancelNotification(BigInteger id);

	public List<SchedulerNotification> getNotifictions();

	public boolean  cancelNotificationUpdateRemark(BigInteger id,String remark);

}
