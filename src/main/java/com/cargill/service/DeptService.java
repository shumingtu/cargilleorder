package com.cargill.service;

import com.cargill.dto.DeptDto;

import java.util.List;

public interface DeptService {
    List<DeptDto> getAllDepts();
}
