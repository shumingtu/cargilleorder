package com.cargill.service;

import com.cargill.dto.CustomerDto;
import com.cargill.dto.CustomerFeedingscaleDto;
import com.cargill.dto.CustomerQuotaDto;
import com.cargill.dto.request.CustomerMemoRequest;
import com.cargill.dto.request.SearchCustomerRequest;
import com.cargill.model.CustomerFeedingscale;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface CustomerService {

    List<CustomerDto> findBySubId(String sub_id);

    CustomerFeedingscaleDto findFeedingScaleBySubId(String sub_id);

    CustomerQuotaDto getQuota(String sub_id);

    Page<CustomerDto> findByCondition(SearchCustomerRequest condition, Pageable pageable);

    Page<CustomerDto> findByConditionwWithoutCustomerMainId(SearchCustomerRequest condition, Pageable pageable);

    void saveMemo(String customerMainId, String customerSubId, String memoString);
}
