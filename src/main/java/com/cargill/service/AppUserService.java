package com.cargill.service;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.cargill.dto.AppAccountDto;
import com.cargill.dto.request.AppAccountRequest;

public interface AppUserService {

    Page<AppAccountDto> findByCondition(AppAccountRequest condition, List<String> type_ids, Pageable pageable);

    AppAccountDto getAppAccount(BigInteger id);

    AppAccountDto getAppAccountByUnicode(String unicode);

    void deleteById(BigInteger id);

    BigInteger deleteByUnicode(String unicode);

    AppAccountDto insertAccount(AppAccountDto accountDto);

    AppAccountDto updateAccount(AppAccountDto accountDto);

    Boolean isAccountExist(String unicode);

    AppAccountDto updateAccountBySms(BigInteger account_id, String sms_code);
}
