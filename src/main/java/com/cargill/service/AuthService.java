package com.cargill.service;

import com.cargill.dto.WebAccountDto;
import com.cargill.dto.request.ActiveUserRequest;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface AuthService extends UserDetailsService {

    WebAccountDto activeUser(ActiveUserRequest request);
}
