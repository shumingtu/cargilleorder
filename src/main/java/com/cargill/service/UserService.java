package com.cargill.service;

import com.cargill.dto.WebAccountDto;
import com.cargill.dto.request.WebAccountRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.math.BigInteger;
import java.util.List;

public interface UserService {
    WebAccountDto getWebAccount(BigInteger id);

    WebAccountDto getWebAccountByAccount(String account);

    WebAccountDto getWebAccountById(String user_id);

    Page<WebAccountDto> findByCondition(WebAccountRequest condition, List<String> dept_ids, Pageable pageable);

    void deleteById(BigInteger id);

    BigInteger deleteByAccount(String account);

    WebAccountDto insertAccount(WebAccountDto accountDto);

    WebAccountDto updateAccount(WebAccountDto accountDto);

    WebAccountDto updateAccountActData(WebAccountDto accountDto);

    Boolean isAccountExist(String account);


}
