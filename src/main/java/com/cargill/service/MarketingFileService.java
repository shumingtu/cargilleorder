package com.cargill.service;

import com.cargill.dto.MarketingFileDto;
import com.cargill.dto.request.MarketingFileUploadRequest;
import com.cargill.exception.CargillException;
import org.springframework.core.io.Resource;

public interface MarketingFileService {

    void init();

    void saveFile(MarketingFileUploadRequest file_request) throws CargillException;

    MarketingFileDto getAllFiles();

    Resource getFile(String file_name);
}
