package com.cargill.service;

import com.cargill.exception.CargillException;

public interface SmsService {

    String sendSms(String user_phone, String message) throws CargillException;
}
