package com.cargill.service;


import com.cargill.dto.TmcodeGroupDto;

import java.util.List;

public interface TmcodeGroupService {

    List<TmcodeGroupDto> getTmcodeByGroupName(String groupName);

}
