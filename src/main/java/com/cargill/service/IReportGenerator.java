package com.cargill.service;

import org.apache.poi.ss.usermodel.Workbook;

import com.cargill.dto.request.ReportRequest;

public interface IReportGenerator {

	public boolean supports(ReportRequest request);
	
	public void buildWorkbook(Workbook workbook, ReportRequest request);
}
