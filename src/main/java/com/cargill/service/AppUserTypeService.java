package com.cargill.service;

import com.cargill.dto.AppUserTypeDto;
import java.util.List;

public interface AppUserTypeService {

    List<AppUserTypeDto> getAllTypes();
}
