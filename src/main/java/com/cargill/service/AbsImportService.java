package com.cargill.service;

import com.cargill.dto.ImportProcessResultDto;
import com.cargill.exception.CargillException;
import com.cargill.exception.ErrorCode;
import com.google.common.collect.Streams;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Optional;

@Slf4j
public abstract class AbsImportService {
    public enum IMPORT_TYPE {
        // 客戶主檔
        CUSTOMER,
        // 客戶 QUOTA
        CUSTOMER_QUOTA,
        // 客戶特配台中
        CUSTOMER_TP_TC,
        // 客戶特配高雄
        CUSTOMER_TP_KS,
        // 客戶飼養規模
        CUSTOMER_FEEDINGSCALE,

        // 產品主檔
        PRODUCT,
        // 特配原料
        PRODUCT_TP,
        // 產品常用抽藥
        PRODUCT_CUCY,
        // 產品特配原料台中公司藥
        PRODUCT_TP_TC_COMPANY_TWENTY,
        // 產品特配原料台中自備藥
        PRODUCT_TP_TC_SELF_TWENTY,
        // 產品特配原料高雄公司藥
        PRODUCT_TP_KS_COMPANY_NULL,
        // 產品特配原料高雄 40 自備藥
        PRODUCT_TP_KS_SELF_40,
        // 產品特配原料高雄 50 自備藥
        PRODUCT_TP_KS_SELF_50,
        // 產品特配原料高雄 60 自備藥
        PRODUCT_TP_KS_SELF_60,
        // 產品特配價格
        PRODUCT_TP_DRUG_PRICE,

        // 自備藥庫存
        DRUG_INVENTORY
    }

    public enum UPDATE_MODE {
        // 更新或新增
        OVERWRITE_OR_APPEND,
        // 刪除現有重新上傳
        DELETE_THEN_INSERT
    }


    private ImportProcessResultDto importProcessResult;

    /***
     * 匯入前先做格式與欄位的檢查，預設不檢查必須交由實作的服務處理
     * @param workbook
     * @param type
     * @return 回傳 true 才會開始動作呼叫 processSheet() 匯入資料
     */
    protected boolean checkValidWorkbook(XSSFWorkbook workbook, IMPORT_TYPE type) {
        return true;
    }

    /***
     * 進行資料匯入
     * @param workbook
     * @param type
     * @param mode
     * @return 回傳 ImportProcessResultDto 物件回報匯入結果
     */
    protected abstract ImportProcessResultDto processSheet(XSSFWorkbook workbook, IMPORT_TYPE type, UPDATE_MODE mode);

    /***
     * 刪除舊資料動作
     */
    protected abstract void deleteAll(IMPORT_TYPE type);

    protected Optional<String> getCellValue(Cell column) {
        return Optional.ofNullable(column).map((c) -> {
            switch (c.getCellTypeEnum()) {
                case NUMERIC:
                    return String.valueOf(Double.valueOf(c.getNumericCellValue()).intValue());
                //return (c.getNumericCellValue() + "").replaceAll("\\.0", "");
                case STRING:
                    return c.getStringCellValue().trim();
                default:
                    throw new CargillException(ErrorCode.IMPORT_FILE_INVALID_COLUMN);
            }
        });
    }

    protected boolean checkIsNotBlank(Row row) {

        boolean check1 = (
                (null != row.getCell(0)) &&
                        (null != row.getCell(1)) &&
                        !CellType.BLANK.equals(row.getCell(0).getCellTypeEnum()) &&
                        !CellType.BLANK.equals(row.getCell(1).getCellTypeEnum()) &&
                        (null != row.getCell(0) && (CellType.STRING.equals(row.getCell(0).getCellTypeEnum()) && !row.getCell(0).getStringCellValue().isEmpty())) &&
                        (null != row.getCell(1) && (CellType.STRING.equals(row.getCell(1).getCellTypeEnum()) && !row.getCell(1).getStringCellValue().isEmpty()))
        );

        if (!check1) {
            return (
                    (null != row.getCell(2)) &&
                            !CellType.BLANK.equals(row.getCell(2).getCellTypeEnum())
            );
        }

        return true;
    }

    protected int headerCount() {
        return 1;
    }

    protected ImportProcessResultDto getImportProcessResult() {
        return importProcessResult;
    }

    public ImportProcessResultDto importFile(MultipartFile upload_file, IMPORT_TYPE type, UPDATE_MODE mode) throws CargillException {
        try {
            XSSFWorkbook workbook = new XSSFWorkbook(Optional.ofNullable(upload_file).orElseThrow(() -> new CargillException(ErrorCode.FILE_UPLOAD_FAILED)).getInputStream());

            // 只有欄位格式檢查成功才可以匯入資料
            if (this.checkValidWorkbook(workbook, type)) {
                if (mode.equals(UPDATE_MODE.DELETE_THEN_INSERT)) {
                    log.warn("=== DELETE ALL " + type + " data due to UPDATE_MODE valued " + mode);
                    this.deleteAll(type);
                }
            } else {
                throw new CargillException(ErrorCode.IMPORT_FILE_INVALID_TYPE);
            }

            importProcessResult = new ImportProcessResultDto(upload_file.getOriginalFilename());
            // 先做欄位的概要檢查以得出實際的資料筆數
            Optional.ofNullable(workbook.getSheetAt(0)).ifPresent((s) -> {
                if (1 < s.getPhysicalNumberOfRows()) {
                    importProcessResult.setOriginalRowsCount(
                            Streams.stream(s.iterator())
                                    .skip(headerCount())
                                    .filter(this::checkIsNotBlank)
                                    .count());
                } else {
                    log.error("=== Invalid file " + upload_file.getOriginalFilename());
                }
            });

            return this.processSheet(workbook, type, mode);
        } catch (IOException e) {
            throw new CargillException(ErrorCode.FILE_UPLOAD_FAILED);
        }
    }
}
