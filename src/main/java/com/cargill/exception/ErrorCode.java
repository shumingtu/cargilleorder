package com.cargill.exception;

public enum ErrorCode {
    SUCCESS(""),
    NOT_FOUND(""),
    INVALID_LOGIN("Invalid login user"),
    LOGIN_FAILED("Login failed"),
    ACTIVE_FAILED_INVALID_PASSWORD("Activation failed with invalid password"),
    ACTIVE_FAILED_INVALID_CODE("Activation failed with invalid active code"),
    ACTIVE_FAILED_INVALID_DATE("Activation failed with invalid active date"),
    ACTIVE_FAILED_INVALID_USER("Activation failed with invalid user"),

    FILE_PATH_INIT_FAILED("Could not initialize storage"),
    FILE_UPLOAD_FAILED("File uploading failed"),
    FILE_UPLOAD_FAILED_INVALID_CAROUSEL("Invalid carousel"),
    INVALIE_FILE_FOR_DOWNLOADING("Invalid file for downloading"),

    IMPORT_FILE_INVALID_TYPE("Invalid data type for specified file"),
    IMPORT_FILE_INVALID_COLUMN("Invalid column value in somewhere row"),
    IMPORT_FILE_NULL_VALUE("Invalid data is null value"),

    INVALID_CUSTOMER("Invalid customer"),
    INVALID_ORDER_DATA("Invalid order data"),
    ERROR_ORDER_DATA("Error occurred while saving order data"),

    INVALID_TEMPLATE_FILE("Invalid Excel file for report"),

    SMS_ERROR("Send SMS failed"),

    UNKNOWN_ERROR("Unknown error");

    private final String description;

    ErrorCode(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
