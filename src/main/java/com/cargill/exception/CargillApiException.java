package com.cargill.exception;

public class CargillApiException extends CargillException {
    public CargillApiException(ErrorCode err_code) {
        super(err_code);
    }

    public CargillApiException(ErrorCode err_code, String message) {
        super(err_code, message);
    }
}
