package com.cargill.exception;

import java.io.Serializable;

public class CargillException extends RuntimeException implements Serializable {

    private ErrorCode code;

    public CargillException(ErrorCode code) {
        super(code.getDescription());
        this.code = code;
    }

    public CargillException(ErrorCode err_code, String message) {
        super(message);
        this.code = err_code;
    }

    public ErrorCode getErrorCode() {
        return code;
    }
}
