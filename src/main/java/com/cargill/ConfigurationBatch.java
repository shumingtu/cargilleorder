package com.cargill;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import com.cargill.properties.BatchProperties;

@Configuration
@EnableConfigurationProperties(BatchProperties.class)
public class ConfigurationBatch {
	
}
