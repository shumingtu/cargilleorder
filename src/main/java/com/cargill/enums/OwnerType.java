package com.cargill.enums;

public enum OwnerType {

	SALES("S"),
	CUSTOMER("C");
	
	private String code;
	
	private OwnerType(String code) {
		this.code = code;
	}
	
	public String getCode() {
		return this.code;
	}
}
