package com.cargill.controller;

import com.cargill.Constants;
import com.cargill.dto.WebAccountDto;
import com.cargill.dto.request.WebAccountRequest;
import com.cargill.service.DeptService;
import com.cargill.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@Slf4j
@RequestMapping("/webaccount")
public class WebAccountController extends BaseController {

    @Autowired
    UserService userService;

    @Autowired
    DeptService deptService;


    @RequestMapping("")
    public String index(Model model, WebAccountRequest condition, @PageableDefault(value = Constants.DEFAULT_PAGE_SIZE) Pageable pageRequest) {
        WebAccountRequest queryCondition = Optional.ofNullable(condition).orElse(new WebAccountRequest());

        List<String> deptIds = Optional.ofNullable(queryCondition.getDept()).orElse("").isEmpty() ? new ArrayList<>()
                : deptService.getAllDepts().stream()
                .filter(d -> d.getName().contains(queryCondition.getDept()))
                .map(d -> d.getCode())
                .collect(Collectors.toList());

        Page<WebAccountDto> accountDtoList = userService.findByCondition(queryCondition, deptIds, pageRequest)
                .map(user -> {
                    deptService.getAllDepts().stream()
                            .filter(dept -> dept.getCode().equals(user.getDept()))
                            .findFirst()
                            .ifPresent(dept -> user.setDeptName(dept.getName()));

                    return user;
                });

        model.addAttribute("condition", queryCondition);
        model.addAttribute("page", accountDtoList);
        model.addAttribute("accountList", accountDtoList.getContent());
        model.addAttribute("depts", deptService.getAllDepts());
        return "G1";
    }

}
