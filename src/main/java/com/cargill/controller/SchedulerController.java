package com.cargill.controller;

import com.cargill.dto.request.ReportRequest;
import com.cargill.model.CustomerOrder;
import com.cargill.model.SchedulerNotification;
import com.cargill.properties.BatchProperties;
import com.cargill.repository.CustomerOrderRepository;
import com.cargill.repository.CustomerRepository;
import com.cargill.repository.SchedulerNotificationRepository;
import com.cargill.util.DateUtil;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cargill.Constants;
import com.cargill.dto.NotificationCustomerDto;
import com.cargill.service.SchedulerManagerService;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;

@RequestMapping("S")
@Controller
public class SchedulerController {
	
	private SchedulerManagerService schedulerManagerService;

	private CustomerOrderRepository customerOrderRepository;
	private CustomerRepository customerRepository;
	private SchedulerNotificationRepository schedulerNotificationRepository;
	private BatchProperties batchProperties;
	private RestTemplate restTemplate;
	@Autowired
	public void setCustomerOrderRepository(CustomerOrderRepository customerOrderRepository) {
		this.customerOrderRepository = customerOrderRepository;
	}
	@Autowired
	public void setSchedulerNotificationRepository(SchedulerNotificationRepository schedulerNotificationRepository) {
		this.schedulerNotificationRepository = schedulerNotificationRepository;
	}
	@Autowired
	public void setBatchProperties(BatchProperties batchProperties) {
		this.batchProperties = batchProperties;
	}
	@Autowired
	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
	@Autowired
	public void setSchedulerManagerService(SchedulerManagerService schedulerManagerService) {
		this.schedulerManagerService = schedulerManagerService;
	}

	@GetMapping("/1")
	public String getNotificationList(Model model, 
			@PageableDefault(value = Constants.DEFAULT_PAGE_SIZE) Pageable pageRequest) {
		Page<NotificationCustomerDto> notificationList = 
				schedulerManagerService.getNotifictionList(pageRequest);
		model.addAttribute("page", notificationList);
		return "S1";
	}
	
	@PostMapping("/api/cancel")
	@ResponseBody
	public boolean cancelNotification(@RequestBody NotificationCustomerDto dto) {
		return this.schedulerManagerService.cancelNotification(dto.getId());
	}


}
