package com.cargill.controller;

import com.cargill.dto.request.MarketingFileUploadRequest;
import com.cargill.exception.CargillException;
import com.cargill.service.MarketingFileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping("/marketing")
@Slf4j
public class MarketingController extends BaseController {
    @Autowired
    private MarketingFileService marketingFileService;

    @GetMapping("")
    public String index(Model model) {
        model.addAttribute("files", marketingFileService.getAllFiles());

        return "E1";
    }

    @PostMapping("")
    public String uploadFiles(@Valid MarketingFileUploadRequest fileUpload,
                              BindingResult bindingResult,
                              RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {
            bindingResult.getFieldErrors().stream().findFirst().ifPresent(error ->
                    redirectAttributes.addFlashAttribute("message",
                            error.getField()));
            return "redirect:/marketing";
        }

        try {
            marketingFileService.saveFile(fileUpload);
            redirectAttributes.addFlashAttribute("message",
                    "success");
        } catch (CargillException e) {
            redirectAttributes.addFlashAttribute("message",
                    "files");
        }

        return "redirect:/marketing";
    }
}
