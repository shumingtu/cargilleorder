package com.cargill.controller;

import com.cargill.Constants;
import com.cargill.dto.AppAccountDto;
import com.cargill.dto.WebAccountDto;
import com.cargill.dto.request.AppAccountRequest;
import com.cargill.dto.request.WebAccountRequest;
import com.cargill.service.AppUserService;
import com.cargill.service.AppUserTypeService;
import com.cargill.service.SmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/appaccount")
public class AppAccountController extends BaseController{

    @Autowired
    AppUserService appUserService;
    @Autowired
    AppUserTypeService appUserTypeService;

    @RequestMapping("")
    public String index(Model model, AppAccountRequest condition, @PageableDefault(value = Constants.DEFAULT_PAGE_SIZE) Pageable pageRequest) {
        AppAccountRequest queryCondition = Optional.ofNullable(condition).orElse(new AppAccountRequest());

        List<String> typeIds = Optional.ofNullable(queryCondition.getType()).orElse("").isEmpty() ? new ArrayList<>()
                : appUserTypeService.getAllTypes().stream()
                .filter(d -> d.getName().contains(queryCondition.getType()))
                .map(d -> d.getCode())
                .collect(Collectors.toList());
        Page<AppAccountDto> accountDtoList = appUserService.findByCondition(queryCondition, typeIds, pageRequest)
                .map(appuser -> {
                    appUserTypeService.getAllTypes().stream()
                            .filter(appUserType -> appUserType.getCode().equals(appuser.getType()))
                            .findFirst()
                            .ifPresent(appUserType -> {
                                appuser.setTypeName(appUserType.getName());
                            });

                    return appuser;
                });

        model.addAttribute("condition", queryCondition);
        model.addAttribute("page", accountDtoList);
        model.addAttribute("accountList", accountDtoList.getContent());
        model.addAttribute("types", appUserTypeService.getAllTypes());
        return "G3";
    }


}
