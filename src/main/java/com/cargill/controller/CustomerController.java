package com.cargill.controller;

import com.cargill.Constants;
import com.cargill.dto.CustomerDto;
import com.cargill.dto.ImportProcessResultDto;
import com.cargill.dto.request.CustomerFileUploadRequest;
import com.cargill.dto.request.SearchCustomerRequest;
import com.cargill.exception.CargillException;
import com.cargill.service.AbsImportService;
import com.cargill.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping("/B")
@Slf4j
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @Qualifier("customerDataImportServiceImpl")
    @Autowired
    private AbsImportService customerDataImportServiceImpl;

    @Qualifier("customerDataQuotaImportServiceImpl")
    @Autowired
    private AbsImportService customerDataQuotaImportServiceImpl;

    @Qualifier("customerDataFeedingscaleImportServiceImpl")
    @Autowired
    private AbsImportService customerDataFeedingscaleImportServiceImpl;

    @Qualifier("drugInventoryImportServiceImpl")
    @Autowired
    private AbsImportService drugInventoryImportServiceImpl;

    @GetMapping("/1")
    public String index1(Model model) {
        return "B1_empty";
    }

    @RequestMapping("/1")
    public String b1(Model model, SearchCustomerRequest condition, @PageableDefault(value = Constants.DEFAULT_PAGE_SIZE) Pageable pageRequest) {
        SearchCustomerRequest queryCondition = Optional.ofNullable(condition).orElse(new SearchCustomerRequest());
        Page<CustomerDto> customerListDtoList = customerService.findByCondition(condition, pageRequest);
        model.addAttribute("condition", queryCondition);
        model.addAttribute("page", customerListDtoList);
        model.addAttribute("customerList", customerListDtoList.getContent());
        return "B1";
    }

    @GetMapping("/3")
    public String index3(Model model) {

        return "B3";
    }

    @PostMapping("/3")
    public String uploadB3File(@Valid CustomerFileUploadRequest fileUpload,
                               BindingResult bindingResult, RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {
            bindingResult.getFieldErrors().stream().findFirst().ifPresent(error -> {
                ImportProcessResultDto importProcessResult = new ImportProcessResultDto("");
                importProcessResult.setProcessResult(false);
                redirectAttributes.addFlashAttribute("processResult",
                        importProcessResult);
            });
            return "redirect:/B/3";
        }

        ImportProcessResultDto importProcessResult;
        try {
            importProcessResult = customerDataImportServiceImpl.importFile(fileUpload.getFile(), AbsImportService.IMPORT_TYPE.CUSTOMER, fileUpload.getUpdateMode());

        } catch (CargillException e) {
            importProcessResult = new ImportProcessResultDto(fileUpload.getFile().getOriginalFilename());
            importProcessResult.setProcessResult(false);
        }
        redirectAttributes.addFlashAttribute("processResult",
                importProcessResult);
        return "redirect:/B/3";
    }

    @GetMapping("/4")
    public String index4(Model model) {
        return "B4";
    }

    @PostMapping("/4")
    public String uploadB4File(@Valid CustomerFileUploadRequest fileUpload,
                               BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            bindingResult.getFieldErrors().stream().findFirst().ifPresent(error -> {
                ImportProcessResultDto importProcessResult = new ImportProcessResultDto("");
                importProcessResult.setProcessResult(false);
                redirectAttributes.addFlashAttribute("processResult",
                        importProcessResult);
            });
            return "redirect:/B/4";
        }

        ImportProcessResultDto importProcessResult;
        try {
            // QUOTA 或特配
            if (fileUpload.getImportType().equals(AbsImportService.IMPORT_TYPE.CUSTOMER_QUOTA)) {
                importProcessResult = customerDataQuotaImportServiceImpl.importFile(fileUpload.getFile(), AbsImportService.IMPORT_TYPE.CUSTOMER_QUOTA, fileUpload.getUpdateMode());
            } else {
                importProcessResult = customerDataFeedingscaleImportServiceImpl.importFile(fileUpload.getFile(), AbsImportService.IMPORT_TYPE.CUSTOMER_FEEDINGSCALE, fileUpload.getUpdateMode());
            }
        } catch (CargillException e) {
            importProcessResult = new ImportProcessResultDto(fileUpload.getFile().getOriginalFilename());
            importProcessResult.setProcessResult(false);
        }

        redirectAttributes.addFlashAttribute("processResult",
                importProcessResult);
        return "redirect:/B/4";
    }

    @GetMapping("/5")
    public String index5(Model model) {
        return "B5";
    }

    @PostMapping("/5")
    public String uploadB5File(@Valid CustomerFileUploadRequest fileUpload,
                               BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            bindingResult.getFieldErrors().stream().findFirst().ifPresent(error -> {
                ImportProcessResultDto importProcessResult = new ImportProcessResultDto("");
                importProcessResult.setProcessResult(false);
                redirectAttributes.addFlashAttribute("processResult",
                        importProcessResult);
            });
            return "redirect:/B/5";
        }

        ImportProcessResultDto importProcessResult;
        try {

                importProcessResult = drugInventoryImportServiceImpl.importFile(fileUpload.getFile(), AbsImportService.IMPORT_TYPE.DRUG_INVENTORY, fileUpload.getUpdateMode());


        } catch (CargillException e) {
            importProcessResult = new ImportProcessResultDto(fileUpload.getFile().getOriginalFilename());
            importProcessResult.setProcessResult(false);
        }

        redirectAttributes.addFlashAttribute("processResult",
                importProcessResult);
        return "redirect:/B/5";
    }
}
