package com.cargill.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.cargill.dto.request.ReportRequest;
import com.cargill.exception.CargillException;
import com.cargill.exception.ErrorCode;
import com.cargill.repository.CustomerOrderRepository;
import com.cargill.service.ReportService;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/report")
@Slf4j
public class ReportController extends BaseController {

    @Autowired
    private ReportService reportService;
    private CustomerOrderRepository customerOrderRepository;
    @Autowired
	public void setCustomerOrderRepository(CustomerOrderRepository customerOrderRepository) {
		this.customerOrderRepository = customerOrderRepository;
	}

    @GetMapping("")
    public ModelAndView index(ModelAndView modelAndView) {
    	List<String> tmcodes = this.customerOrderRepository.findDistinctTmcodeBy();
    	modelAndView.addObject("tmcodes", tmcodes);
    	modelAndView.setViewName("F1");
        return modelAndView;
    }

    @GetMapping("/download")
    public ResponseEntity<ByteArrayResource> download(ReportRequest request) {
        if (Optional.ofNullable(request).filter(it -> !it.getType().isEmpty()).isPresent()) {
            log.info("=== report: " + request.toString());
        }

        OutputStream os = new ByteArrayOutputStream();
        Optional.ofNullable(request.getType())
                .ifPresent(it -> {
                    try {
                        if ("opt1".equals(it)) {
                            reportService.generateReport1(request).write(os);
                        }

                        if ("opt2".equals(it)) {
                            reportService.generateReport2(request).write(os);
                        }
                        /* <201905> Add: 新增App訂單統計 start */
                        if ("opt3".equals(it)) {
                        	reportService.generateReport3(request).write(os);
                        }
                        /* <201905> Add: 新增App訂單統計 end */
                    } catch (IOException e) {
                        throw new CargillException(ErrorCode.NOT_FOUND);
                    }
                });

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION,
                        "attachment; filename=" + request.getFileByType() + ".xlsx")
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(new ByteArrayResource(((ByteArrayOutputStream) os).toByteArray()));
    }
}
