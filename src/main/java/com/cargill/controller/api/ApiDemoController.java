package com.cargill.controller.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cargill.util.CodeUtil;

@RestController
@RequestMapping(path = "/api/demo")
public class ApiDemoController {

	@GetMapping(path = "/generateOrderNo")
	public String generateOrderNo() {
		return CodeUtil.generateOrderNo();
	}
	
}
