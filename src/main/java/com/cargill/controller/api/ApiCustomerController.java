package com.cargill.controller.api;

import com.cargill.dto.request.CustomerMemoRequest;
import com.cargill.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/customer")
public class ApiCustomerController {

    @Autowired
    private CustomerService customerService;
    @RequestMapping("/saveMemo")
    public ResponseEntity<String> saveMemo(@RequestBody CustomerMemoRequest memoRequest ) {

        try {
            customerService.saveMemo(memoRequest.getCustomerMainId(),memoRequest.getCustomerSubId(),memoRequest.getMemoJson());
            return  ResponseEntity.ok().body("success");
        } catch (Exception e){
            System.out.println("ApiCustomerController ,saveMemo Exception :" +e);
            return ResponseEntity.ok().body("");
        }
    }

}
