package com.cargill.controller.api;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cargill.Constants;
import com.cargill.dto.ProductCUCYDto;
import com.cargill.dto.ProductDto;
import com.cargill.dto.ViewProductTPDrugDto;
import com.cargill.dto.request.QueryProductRequest;
import com.cargill.dto.request.QueryProductTPDrugRequest;
import com.cargill.model.parameter.TPAreaCode;
import com.cargill.model.parameter.TPDrugType;
import com.cargill.service.ProductService;
import com.cargill.transform.ProductCUCYTransform;

@RestController
@RequestMapping("/api/product")
public class ApiProductController {

    @Autowired
    ProductService productService;
    @Autowired
    private ProductCUCYTransform productCUCYTransform;

    @RequestMapping("/query")
    public List<ProductDto> query(QueryProductRequest request) {
        return Optional.ofNullable(request)
                .map((it) -> productService.getProductsByCodeOrName(
                        it.getCode(),
                        it.getName())
                        .stream()
                        .limit(Constants.DEFAULT_PAGE_SIZE)
                        .collect(Collectors.toList())
                ).orElse(Collections.emptyList());
    }

    /**
     * 查詢指定產品 code 的常用抽藥提醒清單
     *
     * @param code
     * @return
     */
    @RequestMapping("/cucy")
    public List<ProductCUCYDto> queryCucy(String code) {
    	String codeTrimSpace = StringUtils.trimToEmpty(code);
        return Optional.ofNullable(
                productService.getProductsCUCY(Collections.singletonList(codeTrimSpace)))
                .map(productCUCYTransform::toDtos).orElse(new ArrayList<>());
    }

    /**
     * 查詢公司藥
     *
     * @param request
     * @return
     */
    @RequestMapping("/query/drugs/company")
    public List<ViewProductTPDrugDto> queryDrugsByComapny(QueryProductTPDrugRequest request) {
        return Optional.ofNullable(request)
                .map((it) -> productService.getDrugs(
                        it.getCode(),
                        it.getName(), TPDrugType.COMPANY)
                        .stream()
                        .filter(d -> d.getAreaType().name().equals(it.getAreaType()))
                        .filter(d -> d.getAreaCode().equals(it.getAreaCodeName()) || d.getAreaCode().equals(TPAreaCode.NULL))
                        .limit(Constants.DEFAULT_PAGE_SIZE)
                        .collect(Collectors.toList())
                ).orElse(Collections.emptyList());
    }

    /**
     * 查詢自備藥
     *
     * @param request
     * @return
     */
    @RequestMapping("/query/drugs/self")
    public List<ViewProductTPDrugDto> queryDrugsBySelf(QueryProductTPDrugRequest request) {
        return Optional.ofNullable(request)
                .map((it) -> productService.getDrugs(
                        it.getCode(),
                        it.getName(), TPDrugType.SELF)
                        .stream()
                        .filter(d -> d.getAreaType().name().equals(it.getAreaType()))
                        .filter(d -> d.getAreaCode().equals(it.getAreaCodeName()) || d.getAreaCode().equals(TPAreaCode.NULL))
                        .limit(Constants.DEFAULT_PAGE_SIZE)
                        .collect(Collectors.toList())
                ).orElse(Collections.emptyList());
    }
}
