package com.cargill.controller.api;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cargill.dto.CustomerOrderDto;
import com.cargill.dto.WebAccountDto;
import com.cargill.dto.request.CustomerOrderDuplicateRequest;
import com.cargill.dto.request.CustomerOrderUpdateRequest;
import com.cargill.exception.CargillApiException;
import com.cargill.exception.CargillException;
import com.cargill.exception.ErrorCode;
import com.cargill.model.CustomerOrder;
import com.cargill.service.CustomerOrderService;
import com.cargill.service.UserService;
import com.cargill.transform.CustomerOrderTransform;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/order")
public class ApiOrderController {

    @Autowired
    private CustomerOrderService customerOrderService;
    @Autowired
    private UserService userService;
    @Autowired
    private CustomerOrderTransform customerOrderTransform;

    private void updateOrderPerson(CustomerOrderUpdateRequest order) {
        String loginUser = Optional.ofNullable(
                userService.getWebAccountById(SecurityContextHolder.getContext().getAuthentication().getName())
        ).map(WebAccountDto::getUserName).orElse("");

        order.setOrderPerson(Optional.ofNullable(order.getOrderPerson()).orElse(loginUser));
    }

    private void updateOrderPerson(CustomerOrder order) {
        String loginUserId = SecurityContextHolder.getContext().getAuthentication().getName();
        String orderPerson = Optional.ofNullable(userService.getWebAccountById(loginUserId))
                .map(WebAccountDto::getUserName).orElse("");
        order.setOrderPerson(Optional.ofNullable(order.getOrderPerson()).orElse(orderPerson));
    }

    @PostMapping("/update")
    public CustomerOrderDto updateOrder(@RequestBody CustomerOrderUpdateRequest order,
                                        BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            bindingResult.getFieldErrors().stream().findFirst().ifPresent(error -> {
                log.error("=== Valid CustomerOrderUpdateRequest failed:" + error.toString());
                throw new CargillApiException(ErrorCode.ERROR_ORDER_DATA);
            });
        }

        String loginUserId = SecurityContextHolder.getContext().getAuthentication().getName();
       String orderPerson = Optional.ofNullable(userService.getWebAccountById(loginUserId))
                .map(WebAccountDto::getUserName).orElse("");

        CustomerOrderDto orderResult = Optional.of(customerOrderService.createOrUpdateOrder(order , orderPerson))
                .orElseThrow(() -> new CargillException(ErrorCode.ERROR_ORDER_DATA));
        
        customerOrderService.afterCreateOrUpdateOrder(orderResult);

        return orderResult;
    }

    @PostMapping("/duplicate")
    public CustomerOrderDto duplicateOrder(@RequestBody CustomerOrderDuplicateRequest request,
                                           BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            bindingResult.getFieldErrors().stream().findFirst().ifPresent(error -> {
                log.error("=== Valid CustomerOrderUpdateRequest failed:" + error.toString());
                throw new CargillApiException(ErrorCode.ERROR_ORDER_DATA);
            });
        }

        CustomerOrder order = Optional.ofNullable(customerOrderService.getOrder(request.getOrderNo()))
                .filter(it -> request.getCustomerSubId().equals(it.getCustomerSubId()))
                .orElseThrow(() -> new CargillException(ErrorCode.INVALID_ORDER_DATA));

        String loginUserId = SecurityContextHolder.getContext().getAuthentication().getName();
        return Optional.of(customerOrderService.duplicateOrder(
                order,
                Optional.ofNullable(userService.getWebAccountById(loginUserId))
                        .map(WebAccountDto::getUserName).orElse("")
        )).orElseThrow(() -> new CargillException(ErrorCode.ERROR_ORDER_DATA));
    }
}
