package com.cargill.controller.api;

import com.cargill.Constants;
import com.cargill.dto.AppAccountDto;
import com.cargill.dto.WebAccountDto;
import com.cargill.dto.request.AppAccountRequest;
import com.cargill.dto.request.TransAppAccountRequest;
import com.cargill.dto.request.TransWebAccountRequest;
import com.cargill.dto.request.WebAccountRequest;
import com.cargill.service.AppUserService;
import com.cargill.service.SmsService;
import com.cargill.util.CodeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigInteger;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

@RestController
@RequestMapping("/api/appaccount")
public class ApiAppAccountController {

    @Autowired
    AppUserService appUserService;

    @Autowired
    SmsService smsService;

    @PostMapping("/getAccount")
    public AppAccountDto getAccount(@RequestBody TransAppAccountRequest transAccount) {
        Boolean isAccountExist = appUserService.isAccountExist(transAccount.getTransUnicode());
        if (isAccountExist) {
            return appUserService.getAppAccountByUnicode(transAccount.getTransUnicode());
        } else {

            return new AppAccountDto();
        }
    }

    @RequestMapping("/deleteAccount")
    public BigInteger deleteAccount(@RequestBody TransAppAccountRequest transAccount) {
        Boolean isAccountExist = appUserService.isAccountExist(transAccount.getTransUnicode());
        if (isAccountExist) {
            return appUserService.deleteByUnicode(transAccount.getTransUnicode());
        } else {

            return BigInteger.ZERO;
        }
    }

    @RequestMapping("/insertAccount")
    public AppAccountDto insertAccount(@RequestBody AppAccountRequest insertAccount) {
        Boolean isAccountExist = appUserService.isAccountExist(insertAccount.getUnicode());
        AppAccountDto appaccount = new AppAccountDto();
        if (isAccountExist) {
            appaccount.setType(insertAccount.getType());
            appaccount.setUnicode(insertAccount.getUnicode());
            appaccount.setUserName(insertAccount.getUserName());
            appaccount.setPhone(insertAccount.getPhone());
            appaccount.setCanAct(insertAccount.getCanAct());
            return appaccount;
        } else {
            appaccount.setType(insertAccount.getType());
            appaccount.setUnicode(insertAccount.getUnicode());
            appaccount.setUserName(insertAccount.getUserName());
            appaccount.setPhone(insertAccount.getPhone());
            appaccount.setVerifyResult("F");
            appaccount.setCanAct(insertAccount.getCanAct());
            return appUserService.insertAccount(appaccount);
        }
    }

    @RequestMapping("/updateAccount")
    public AppAccountDto updateAccount(@RequestBody TransAppAccountRequest updateAccount) {
        AppAccountDto appaccount = appUserService.getAppAccountByUnicode(updateAccount.getTransUnicode());
        appaccount.setType(updateAccount.getTransType());
        appaccount.setUserName(updateAccount.getTransUserName());
        appaccount.setPhone(updateAccount.getTransPhone());
        appaccount.setCanAct(updateAccount.getTransCanAct());
        return appUserService.updateAccount(appaccount);
    }

    @RequestMapping("/sendSms")
    public AppAccountDto sendSms(@RequestBody TransAppAccountRequest updateAccount) {
        AtomicReference<AppAccountDto> account = new AtomicReference<>(new AppAccountDto());

        Optional.ofNullable(appUserService.getAppAccountByUnicode(updateAccount.getTransUnicode()))
                .ifPresent(a -> {
                    String smsCode = CodeUtil.generateSMSCode();

                    // we don't care about the result of this action
                    smsService.sendSms(a.getPhone(), Constants.SMS_DEFAULT_MESSAGE.replaceAll("\\$\\{code}", smsCode));

                    account.set(appUserService.updateAccountBySms(a.getId(), smsCode));
                });

        return account.get();
    }

}
