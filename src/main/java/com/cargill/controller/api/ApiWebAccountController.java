package com.cargill.controller.api;

import com.cargill.dto.DeptDto;
import com.cargill.dto.WebAccountDto;
import com.cargill.dto.request.WebAccountRequest;
import com.cargill.dto.request.TransWebAccountRequest;
import com.cargill.service.DeptService;
import com.cargill.service.UserService;
import com.cargill.util.CodeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;

@RestController
@RequestMapping("/api/webaccount")
public class ApiWebAccountController {
    @Autowired
    UserService userService;

    @Autowired
    DeptService deptService;

    @PostMapping("/actAccount")
    public WebAccountDto actAccount(@RequestBody TransWebAccountRequest transAccount) {
        WebAccountDto webaccount = userService.getWebAccountByAccount(transAccount.getTransAccount());
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(calendar.getTime());
        calendar.add(Calendar.DATE, 1);
        String actCode = CodeUtil.generateRandomCode();
        webaccount.setActCode(actCode);
        webaccount.setLastActCodeTime(calendar);
        return userService.updateAccountActData(webaccount);
    }

    @PostMapping("/getAccount")
    public WebAccountDto getAccount(@RequestBody TransWebAccountRequest transAccount) {
        Boolean isAccountExist = userService.isAccountExist(transAccount.getTransAccount());
        if(isAccountExist){
            return userService.getWebAccountByAccount(transAccount.getTransAccount());
        }else{

            return new WebAccountDto();
        }
    }

    @PostMapping("/getDepts")
    public List<DeptDto> getDepts() {
        List<DeptDto> deptDtoList = deptService.getAllDepts();
        return deptDtoList;
    }

    @RequestMapping("/deleteAccount")
    public BigInteger deleteAccount(@RequestBody TransWebAccountRequest transAccount) {
        Boolean isAccountExist = userService.isAccountExist(transAccount.getTransAccount());
        if(isAccountExist){
            return userService.deleteByAccount(transAccount.getTransAccount());
        }else{

            return BigInteger.ZERO;
        }
    }

    @RequestMapping("/insertAccount")
    public WebAccountDto insertAccount(@RequestBody WebAccountRequest insertAccount) {
        Boolean isAccountExist = userService.isAccountExist(insertAccount.getAccount());
        WebAccountDto webaccount = new WebAccountDto();
        if (isAccountExist) {
            webaccount.setAccount(insertAccount.getAccount());
            webaccount.setUserName(insertAccount.getUserName());
            webaccount.setDept(insertAccount.getDept());
            return webaccount;
        } else {
            webaccount.setAccount(insertAccount.getAccount().toLowerCase());
            webaccount.setUserName(insertAccount.getUserName());
            webaccount.setDept(insertAccount.getDept());
            webaccount.setStatus("F");
            return userService.insertAccount(webaccount);
        }
    }

    @RequestMapping("/updateAccount")
    public WebAccountDto updateAccount(@RequestBody TransWebAccountRequest updateAccount) {
        WebAccountDto webaccount = userService.getWebAccountByAccount(updateAccount.getTransAccount());
        webaccount.setUserName(updateAccount.getTransUserName());
        webaccount.setDept(updateAccount.getTransDept());
        return userService.updateAccount(webaccount);
    }
}
