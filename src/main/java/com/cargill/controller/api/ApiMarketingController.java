package com.cargill.controller.api;

import com.cargill.dto.MarketingFileDto;
import com.cargill.exception.CargillException;
import com.cargill.service.MarketingFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/marketing")
public class ApiMarketingController {

    @Autowired
    private MarketingFileService marketingFileService;


    @GetMapping(value = "/download/{fileId}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileId) {

        MarketingFileDto fileDto = marketingFileService.getAllFiles();

        String fileName = "";
        String fileNameOrig = "";
        switch (fileId) {
            case "1":
                fileName = fileDto.getFileName1();
                fileNameOrig = fileDto.getOriginalName1();
                break;
            case "2":
                fileName = fileDto.getFileName2();
                fileNameOrig = fileDto.getOriginalName2();
                break;
            case "3":
                fileName = fileDto.getFileName3();
                fileNameOrig = fileDto.getOriginalName3();
                break;
            case "4":
                fileName = fileDto.getFileName4();
                fileNameOrig = fileDto.getOriginalName4();
                break;
            case "5":
                fileName = fileDto.getFileName5();
                fileNameOrig = fileDto.getOriginalName5();
                break;
        }

        try {
            Resource resource = marketingFileService.getFile(fileName);
            return ResponseEntity.ok()
                    .contentType(MediaType.IMAGE_JPEG)
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileNameOrig + "\"")
                    .body(resource);
        }catch (CargillException e) {
            return ResponseEntity.notFound().build();
        }
    }
}
