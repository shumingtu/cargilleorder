package com.cargill.controller;

import com.cargill.dto.ImportProcessResultDto;
import com.cargill.dto.request.CustomerFileUploadRequest;
import com.cargill.dto.request.ProductFileUploadRequest;
import com.cargill.exception.CargillException;
import com.cargill.service.AbsImportService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping("/C")
@Slf4j
public class ProductImportController {

    @Qualifier("productDataImportServiceImpl")
    @Autowired
    private AbsImportService productDataImportServiceImpl;

    @Qualifier("productCUCYDataImportServiceImpl")
    @Autowired
    private AbsImportService productCUCYDataImportServiceImpl;

    @Qualifier("productTPDataImportServiceImpl")
    @Autowired
    private AbsImportService productTPDataImportServiceImpl;

    @Qualifier("productTPDrugDataImportServiceImpl")
    @Autowired
    private AbsImportService productTPDrugDataImportServiceImpl;

    @GetMapping("/1")
    public String c1(Model model) {
        return "C1";
    }

    @PostMapping("/1")
    public String uploadC1File(@Valid ProductFileUploadRequest fileUpload,
                               BindingResult bindingResult, RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {
            bindingResult.getFieldErrors().stream().findFirst().ifPresent(error -> {
                ImportProcessResultDto importProcessResult = new ImportProcessResultDto("");
                importProcessResult.setProcessResult(false);
                redirectAttributes.addFlashAttribute("processResult",
                        importProcessResult);
            });
            return "redirect:/C/1";
        }

        ImportProcessResultDto importProcessResult;
        try {
            importProcessResult =
                    fileUpload.getImportType().equals(AbsImportService.IMPORT_TYPE.PRODUCT_CUCY) ?
                            productCUCYDataImportServiceImpl.importFile(fileUpload.getFile(), fileUpload.getImportType(), fileUpload.getUpdateMode()) :
                            productDataImportServiceImpl.importFile(fileUpload.getFile(), fileUpload.getImportType(), fileUpload.getUpdateMode());

        } catch (CargillException e) {
            importProcessResult = new ImportProcessResultDto(fileUpload.getFile().getOriginalFilename());
            importProcessResult.setProcessResult(false);
        }

        redirectAttributes.addFlashAttribute("processResult",
                importProcessResult);
        return "redirect:/C/1";
    }

    @GetMapping("/3")
    public String c3(Model model) {
        return "C3";
    }


    @PostMapping("/3")
    public String uploadC3File(@Valid ProductFileUploadRequest fileUpload,
                               BindingResult bindingResult, RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {
            bindingResult.getFieldErrors().stream().findFirst().ifPresent(error -> {
                ImportProcessResultDto importProcessResult = new ImportProcessResultDto("");
                importProcessResult.setProcessResult(false);
                redirectAttributes.addFlashAttribute("processResult",
                        importProcessResult);
            });
            return "redirect:/C/3";
        }

        ImportProcessResultDto importProcessResult;
        try {
            importProcessResult = productTPDrugDataImportServiceImpl.importFile(fileUpload.getFile(), fileUpload.getImportType(), fileUpload.getUpdateMode());
        } catch (CargillException e) {
            importProcessResult = new ImportProcessResultDto(fileUpload.getFile().getOriginalFilename());
            importProcessResult.setProcessResult(false);
        }

        redirectAttributes.addFlashAttribute("processResult",
                importProcessResult);
        return "redirect:/C/3";
    }



    @GetMapping("/4")
    public String c4(Model model) {
        return "C4";
    }

    @PostMapping("/4")
    public String uploadC4File(@Valid ProductFileUploadRequest fileUpload,
                               BindingResult bindingResult, RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {
            bindingResult.getFieldErrors().stream().findFirst().ifPresent(error -> {
                ImportProcessResultDto importProcessResult = new ImportProcessResultDto("");
                importProcessResult.setProcessResult(false);
                redirectAttributes.addFlashAttribute("processResult",
                        importProcessResult);
            });
            return "redirect:/C/4";
        }

        ImportProcessResultDto importProcessResult;
        try {
            importProcessResult = productTPDrugDataImportServiceImpl.importFile(fileUpload.getFile(), fileUpload.getImportType(), fileUpload.getUpdateMode());
        } catch (CargillException e) {
            importProcessResult = new ImportProcessResultDto(fileUpload.getFile().getOriginalFilename());
            importProcessResult.setProcessResult(false);
        }

        redirectAttributes.addFlashAttribute("processResult",
                importProcessResult);
        return "redirect:/C/4";
    }


    @GetMapping("/5")
    public String c5(Model model) {
        return "C5";
    }

    @PostMapping("/5")
    public String uploadC5File(@Valid ProductFileUploadRequest fileUpload,
                               BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            bindingResult.getFieldErrors().stream().findFirst().ifPresent(error -> {
                ImportProcessResultDto importProcessResult = new ImportProcessResultDto("");
                importProcessResult.setProcessResult(false);
                redirectAttributes.addFlashAttribute("processResult",
                        importProcessResult);
            });
            return "redirect:/C/5";
        }

        ImportProcessResultDto importProcessResult;
        try {
            importProcessResult = productTPDataImportServiceImpl.importFile(fileUpload.getFile(), fileUpload.getImportType(), fileUpload.getUpdateMode());
        } catch (CargillException e) {
            importProcessResult = new ImportProcessResultDto(fileUpload.getFile().getOriginalFilename());
            importProcessResult.setProcessResult(false);
        }

        redirectAttributes.addFlashAttribute("processResult",
                importProcessResult);
        return "redirect:/C/5";
    }
}
