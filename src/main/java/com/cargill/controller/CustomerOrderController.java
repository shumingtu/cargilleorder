package com.cargill.controller;


import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.cargill.dto.*;
import com.cargill.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cargill.Constants;
import com.cargill.dto.request.SearchA2Request;
import com.cargill.dto.request.SearchCustomerOrderRequest;
import com.cargill.dto.request.SearchCustomerRequest;
import com.cargill.exception.CargillException;
import com.cargill.exception.ErrorCode;
import com.cargill.model.CustomerOrder;
import com.cargill.model.parameter.OrderTypeCode;
import com.cargill.service.CustomerOrderService;
import com.cargill.service.CustomerService;
import com.cargill.service.TmcodeGroupService;
import com.cargill.transform.CustomerOrderTransform;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/A")
@Slf4j
public class CustomerOrderController {

    private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyyMMdd HH:mm");
    @Autowired
    private CustomerService customerService;
    @Autowired
    private CustomerOrderService customerOrderService;
    @Autowired
    private CustomerOrderTransform customerOrderTransform;

    @Autowired
    private TmcodeGroupService tmcodeGroupService;

    @Autowired
    private UserService userService;

    @GetMapping("/1")
    public String a1All(Model model, 
    		@PageableDefault(value = Constants.DEFAULT_QUICK_SEARCH_PAGE_SIZE) Pageable pageRequest) {
        Page<CustomerOrderDto> customerListDtoList = customerOrderService.findByAppAndTmcodes(null, pageRequest);
        model.addAttribute("DEFAULT_CAROUSEL_SEC", Constants.DEFAULT_CAROUSEL_SEC);
        model.addAttribute("page", customerListDtoList);
        model.addAttribute("groupName", "");
        model.addAttribute("tmocdes", "");
        model.addAttribute("customerOrderList", customerListDtoList.getContent());
        return "A1";
    }

    @RequestMapping("/1")
    public String a1ByTmocde(Model model, String groupName, 
    		@PageableDefault(value = Constants.DEFAULT_QUICK_SEARCH_PAGE_SIZE) Pageable pageRequest) {
        List<TmcodeGroupDto> tmcodeGroupDtoList = tmcodeGroupService.getTmcodeByGroupName(groupName);
        Page<CustomerOrderDto> customerOrderListDtoList = customerOrderService.findByAppAndTmcodes(groupName.equals("") ? null : tmcodeGroupDtoList, pageRequest);
        String tmocdes = "";
        for (TmcodeGroupDto tmcodeGroupDto : tmcodeGroupDtoList) {
            tmocdes = tmocdes + "、" + tmcodeGroupDto.getTmcodeId();
        }
        if (!tmocdes.equals("")) {
            tmocdes = tmocdes.substring(1);
        }
        model.addAttribute("DEFAULT_CAROUSEL_SEC", Constants.DEFAULT_CAROUSEL_SEC);
        model.addAttribute("page", customerOrderListDtoList);
        model.addAttribute("groupName", groupName);
        model.addAttribute("tmocdes", tmocdes);
        model.addAttribute("customerOrderList", customerOrderListDtoList.getContent());
        return "A1";
    }

    @GetMapping("/2")
    public String a2(Model model, SearchA2Request request) {
        SearchA2Request queryCondition = Optional.ofNullable(request).orElse(new SearchA2Request());
        if (queryCondition.isBlank()) {
            return "A2";
        }

        List<A2Dto> resultList;
        try {
            // 2021/07/06 增加 久未訂購飼料顯示的邏輯
            resultList = customerOrderService.buildA2(queryCondition);
        } catch (Exception e) {
            log.error("=== buildA2 failed: " + e.getMessage());
            e.printStackTrace();

            resultList = Collections.emptyList();
        }
        model.addAttribute("condition", queryCondition);
        model.addAttribute("result", resultList);

        return "A2";
    }

    @GetMapping("/3")
    public String queryCustomerOrder(Model model, SearchCustomerOrderRequest condition, @PageableDefault(value = Constants.DEFAULT_PAGE_SIZE) Pageable pageRequest) {
        SearchCustomerOrderRequest queryCondition = Optional.ofNullable(condition).orElse(new SearchCustomerOrderRequest());
        System.out.println("CustomerOrderController ,queryCustomerOrder 114 :"+condition);
        System.out.println("CustomerOrderController ,queryCustomerOrder 114 :"+pageRequest);
        if (queryCondition.isBlank()) {
            return "A3_empty";
        }


        Page<CustomerOrderDto> customerOrderListDtoList = customerOrderService.findAllCustomerOrder(condition, pageRequest);
        model.addAttribute("condition", queryCondition);
        model.addAttribute("page", customerOrderListDtoList);
        model.addAttribute("customerOrderList", customerOrderListDtoList.getContent());
        return "A3";
    }

    @GetMapping("/4")
    public String a4Empty(Model model) {
        return "A4_empty";
    }

    @RequestMapping("/4")
    public String queryCustomers(Model model, SearchCustomerRequest condition, @PageableDefault(value = Constants.DEFAULT_PAGE_SIZE) Pageable pageRequest) {
        SearchCustomerRequest queryCondition = Optional.ofNullable(condition).orElse(new SearchCustomerRequest());
        Page<CustomerDto> customerDtoList = customerService.findByConditionwWithoutCustomerMainId(condition, pageRequest);
        model.addAttribute("condition", queryCondition);
        model.addAttribute("page", customerDtoList);
        model.addAttribute("customerList", customerDtoList.getContent());

        Optional.ofNullable(condition.getAction())
                .filter(it -> "deleted".equals(condition.getAction()))
                .ifPresent((it) -> model.addAttribute("action", "deleted"));

        return "A4";
    }

    @GetMapping("/5")
    public String a5(Model model, String customer, String order_no) {

        List<CustomerDto> customerDtos = customerService.findBySubId(customer);
        if (customerDtos.isEmpty()) {
            System.out.println("找不到使用者");
            throw new CargillException(ErrorCode.INVALID_CUSTOMER);
        }

        model.addAttribute("customer", customerDtos);
        model.addAttribute("orderTypeCode", OrderTypeCode.WEB.name());

        Optional.ofNullable(order_no).ifPresent((orderNo) -> {
            String customerSubId = customerDtos.get(0).getCustomerSubId();

            CustomerOrderDto orderData = Optional.ofNullable(customerOrderService.getOrder(orderNo.trim()))
                    .filter(it -> customerSubId.equals(it.getCustomerSubId()))
                    .map(customerOrderTransform::toDto)
                    .orElseThrow(() -> new CargillException(ErrorCode.INVALID_ORDER_DATA));

            List<CustomerDto> c2 = customerDtos.stream()
                    .map(it -> {
                        if (orderData.getShipTo().equals(it.getAddressShipTo())) {
                            it.setShipToSelected(true);
                        }
                        return it;
                    }).collect(Collectors.toList());
            model.addAttribute("customer", c2);
            model.addAttribute("orderTypeCode", orderData.getType().getDesc());
            model.addAttribute("orderData", orderData);
        });

        model.addAttribute("feedingScale", customerService.findFeedingScaleBySubId(customer));
        model.addAttribute("customerQuota", customerService.getQuota(customer));

        return "A5";
    }

    @GetMapping("/print")
    public String print(Model model, String customer, String order_no, String detail_id, String type, String descApp) {

        List<CustomerDto> customerDtos = customerService.findBySubId(customer);
        if (customerDtos.isEmpty()) {
            throw new CargillException(ErrorCode.INVALID_CUSTOMER);
        }

        model.addAttribute("customer", customerDtos);
        model.addAttribute("orderTypeCode", OrderTypeCode.WEB.name());
        model.addAttribute("descApp", descApp);

        Optional.ofNullable(order_no).ifPresent((orderNo) -> {
            String customerSubId = customerDtos.get(0).getCustomerSubId();

            CustomerOrderDto orderData = Optional.ofNullable(customerOrderService.getOrder(orderNo.trim()))
                    .filter(it -> customerSubId.equals(it.getCustomerSubId()))
                    .map(customerOrderTransform::toDto)
                    .orElseThrow(() -> new CargillException(ErrorCode.INVALID_ORDER_DATA));

            model.addAttribute("customer", customerDtos.stream().filter(it -> orderData.getShipTo().equals(it.getAddressShipTo()))
                    .peek(it -> it.setShipToSelected(true)).collect(Collectors.toList()));
            model.addAttribute("shipToSelected", customerDtos.stream().filter(it -> orderData.getShipTo().equals(it.getAddressShipTo())).findFirst().get().getAddressShipTo());
            model.addAttribute("orderTypeCode", orderData.getType().name());
            model.addAttribute("orderData", orderData);

            if ("A8".equals(type)) {
            	IntStream.range(0, orderData.getOrderDetailList().size())
                .filter(i -> i == (Integer.parseInt(detail_id) - 2))
                .mapToObj(orderData.getOrderDetailList()::get).findFirst()
                .ifPresent(d -> {
                    d.getDetailSP().getSymptom().stream()
                            .peek(s -> {
                                model.addAttribute("symptom" + s, true);
                            }).collect(Collectors.toList());
                    model.addAttribute("orderDetail", d);
                    model.addAttribute("spDetail", d.getDetailSP());
                });
            } else if ("A10".equals(type)) {
                orderData.getOrderDetailList()
                        .stream()
                        .filter(d -> detail_id.equals(d.getDetailPD().getId())).findFirst()
                        .ifPresent(d -> {
                            model.addAttribute("orderDetail", d);
                            model.addAttribute("pdDetail", d.getDetailPD());
                        });
            }
        });

        model.addAttribute("feedingScale", customerService.findFeedingScaleBySubId(customer));
        model.addAttribute("customerQuota", customerService.getQuota(customer));

        if ("A8".equals(type)) {
            return "A8";
        } else if ("A10".equals(type)) {
            return "A10";
        }
        return "A6";
    }


    @PostMapping("/5/remove")
    public String remove(String customerSubId, String orderNo) {
        /*
        *  20210621 用狀態控制刪除
        * */
        CustomerOrder order = Optional.ofNullable(customerOrderService.getOrder(orderNo + ""))
                .filter(it -> it.getCustomerSubId().equals(customerSubId))
                .orElseThrow(() -> new CargillException(ErrorCode.INVALID_ORDER_DATA));

        String loginUserId = SecurityContextHolder.getContext().getAuthentication().getName();
        String orderPerson = Optional.ofNullable(userService.getWebAccountById(loginUserId))
                .map(WebAccountDto::getUserName).orElse("");
        order.setModifyLogs(order.getModifyLogs() + ","+ SDF.format(new Date()) + " WEB " + orderPerson + " " + "刪除 ,");
        customerOrderService.updateOrderIsDeleteToY(order);
        return "redirect:/A/4?action=deleted";
    }
}
