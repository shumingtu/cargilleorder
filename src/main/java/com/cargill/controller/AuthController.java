package com.cargill.controller;

import com.cargill.dto.request.ActiveUserRequest;
import com.cargill.exception.CargillException;
import com.cargill.service.AuthService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Optional;

@Controller
@Slf4j
public class AuthController {
    @Autowired
    AuthService authService;

    @GetMapping("/login")
    String login(Model model, String error) {
        if (error != null) {
            model.addAttribute("error", "login failed");
        }

        return "Z1";
    }

    @GetMapping("/active")
    String active(Model model, String error) {
        return "Z2";
    }

    @PostMapping("/active")
    String doActive(Model model, ActiveUserRequest request) {
        try {
            model.addAttribute("success", Optional.ofNullable(authService.activeUser(request)).isPresent());
        } catch (CargillException e) {
            log.warn("=== Active User failed: " + e.getErrorCode().getDescription());
            model.addAttribute("error", e.getErrorCode().toString());
        }
        return "Z2";
    }
}
