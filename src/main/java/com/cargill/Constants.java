package com.cargill;

import java.text.SimpleDateFormat;

public class Constants {
    public static final String DEFAULT_ROLE_PREFIX = "ROLE_";

    public static final String DEFAULT_PWD = "a123456";
    public static final int DEFAULT_CODE_LENGTH = 4;
    public static final int DEFAULT_SMS_CODE_LENGTH = 4;
    public static final int DEFAULT_PAGE_SIZE = 20;
    public static final int DEFAULT_QUICK_SEARCH_PAGE_SIZE = 50;

    public static final int DEFAULT_CAROUSEL_SEC = 60;

    public static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    public static final SimpleDateFormat SDF_DATE_ONLY = new SimpleDateFormat("yyyy-MM-dd");
    public static final SimpleDateFormat SDF_TIME_ONLY = new SimpleDateFormat("HH:mm");

    public static final String SMS_DEFAULT_MESSAGE = "您的愛嘉網帳號開通驗證碼是${code}，感謝您的支持，請依照步驟完成操作。";
}
