package com.cargill.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties("cargill.sms")
public class SmsProperties {

    private boolean realExecute = false;

    private String host;
    private String port;
    private String sysid;
    private String address;
    private String phone;
    private String endpointPath;
}
