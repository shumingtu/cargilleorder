package com.cargill.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties("cargill.storage.marketing")
public class StorageMarketingProperties {

    private String uploadPath;
}
