package com.cargill.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

@Data
@ConfigurationProperties("cargill.batch")
public class BatchProperties {

	private String notificationUrl;
	private String notificationTitle;
	private String notificationMessage;
	private Boolean realExecute; 
	private Boolean disablePush;
	private Boolean controlList;
	@Value("${cargill.batch.batch-enabled:true}")
	private Boolean batchEnabled;
	
}
