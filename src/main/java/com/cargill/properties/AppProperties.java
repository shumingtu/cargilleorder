package com.cargill.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

@Data
@ConfigurationProperties("cargill.app")
public class AppProperties {

	private String orderUrl;
	
}
