package com.cargill.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties("cargill.website")
public class CargillProperties {

    private String mainUrl;
    private Integer maxUploadSize;
}
