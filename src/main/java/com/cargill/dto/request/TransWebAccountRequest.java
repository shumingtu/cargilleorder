package com.cargill.dto.request;

import com.cargill.Constants;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TransWebAccountRequest {

    @NotBlank
    @Length(max = 50)
    private String transId;

    @NotBlank
    @Length(max = 50)
    private String transAccount;

    @NotBlank
    @Length(max = 50)
    private String transUserName;

    @NotBlank
    @Length(max = 50)
    private String transDept;
}
