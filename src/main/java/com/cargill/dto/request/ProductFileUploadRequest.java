package com.cargill.dto.request;

import com.cargill.service.AbsImportService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductFileUploadRequest {

    @NotNull
    private MultipartFile file;
    @NotEmpty
    private String fileType;
    @NotEmpty
    private String updateType;

    public AbsImportService.UPDATE_MODE getUpdateMode() {
        if ("2".equals(updateType)) {
            return AbsImportService.UPDATE_MODE.DELETE_THEN_INSERT;
        }

        return AbsImportService.UPDATE_MODE.OVERWRITE_OR_APPEND;
    }

    public AbsImportService.IMPORT_TYPE getImportType() {
        if ("1".equals(fileType)) {
            return AbsImportService.IMPORT_TYPE.PRODUCT;
        }else if ("2".equals(fileType)) {
            return AbsImportService.IMPORT_TYPE.PRODUCT_CUCY;
        }else if ("3".equals(fileType)) {
            return AbsImportService.IMPORT_TYPE.PRODUCT_TP_TC_COMPANY_TWENTY;
        }else if ("4".equals(fileType)) {
            return AbsImportService.IMPORT_TYPE.PRODUCT_TP_TC_SELF_TWENTY;
        }else if ("5".equals(fileType)) {
            return AbsImportService.IMPORT_TYPE.PRODUCT_TP_KS_COMPANY_NULL;
        }else if ("6".equals(fileType)) {
            return AbsImportService.IMPORT_TYPE.PRODUCT_TP_KS_SELF_40;
        }else if ("7".equals(fileType)) {
            return AbsImportService.IMPORT_TYPE.PRODUCT_TP_KS_SELF_50;
        }else if ("8".equals(fileType)) {
            return AbsImportService.IMPORT_TYPE.PRODUCT_TP_KS_SELF_60;
        }else if ("9".equals(fileType)){
            return AbsImportService.IMPORT_TYPE.PRODUCT_TP_DRUG_PRICE;
        }else{
            return AbsImportService.IMPORT_TYPE.PRODUCT;
        }
    }
}
