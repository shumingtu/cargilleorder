package com.cargill.dto.request;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.cargill.dto.OrderDetailDto;
import com.cargill.model.parameter.OrderTypeCode;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CustomerOrderUpdateRequest {
    private BigInteger id;
    private String orderNo;

    // 訂單人
    private String orderPerson;

    // 出貨日期 (日期+時間的組合: YYYYMMDD HH:mm)
    @NotNull
    private String deliveryDate;

    private String tmcode;

    // 客戶編號
    @NotNull
    private String customerSubId;
    // 客戶名稱
    @NotNull
    private String customerSubName;
    // 客戶電話
    @NotNull
    private String customerPhone;

    // 送貨地點
    @NotNull
    private String shipTo;


    private String descWeb;

    private String descApp;

    // 訂單狀態
    @NotNull
    private OrderTypeCode type = OrderTypeCode.WEB;
    @NotNull
    private String poCode;

    // 訂單內容，含特配單與生產小單
    @NotNull
    private List<OrderDetailDto> orderDetailList = Collections.emptyList();
    
    private Calendar modifyTime;
}
