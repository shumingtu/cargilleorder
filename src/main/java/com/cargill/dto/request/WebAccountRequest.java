package com.cargill.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WebAccountRequest {


    @NotEmpty
    @Length(max = 10)
    private String account = "";

    @NotEmpty
    @Length(max = 10)
    private String userName = "";

    @NotEmpty
    @Length(max = 10)
    private String dept = "";
}
