package com.cargill.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SearchCustomerRequest {

    @NotBlank
    @Length(max = 8)
    private String customerId = "";

    @NotBlank
    @Length(max = 20)
    private String customerName = "";

    @NotBlank
    @Length(max = 5)
    private String tmcode = "";

    private String action = "";
}
