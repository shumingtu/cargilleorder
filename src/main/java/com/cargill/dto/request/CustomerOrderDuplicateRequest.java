package com.cargill.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CustomerOrderDuplicateRequest {
    private String orderNo;

    // 客戶編號
    @NotNull
    private String customerSubId;
}
