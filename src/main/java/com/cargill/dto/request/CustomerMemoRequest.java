package com.cargill.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CustomerMemoRequest {

    private String customerMainId;
    private String customerSubId;
    private String memoJson;

}
