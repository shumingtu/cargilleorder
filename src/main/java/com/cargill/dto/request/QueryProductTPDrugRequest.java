package com.cargill.dto.request;

import com.cargill.model.parameter.TPAreaCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import java.util.Optional;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class QueryProductTPDrugRequest {

    @Length(max = 50)
    private String code;

    @Length(max = 50)
    private String name;

    @Length(max = 10)
    private String areaCode;

    @Length(max = 10)
    private String areaType;

    public TPAreaCode getAreaCodeName() {
        return Optional.ofNullable(areaCode).map(it -> {
            switch (it) {
                case "20":
                    return TPAreaCode.TWENTY;
                case "40":
                    return TPAreaCode.FOURTY;
                case "50":
                    return TPAreaCode.FIFTY;
                case "60":
                    return TPAreaCode.SIXTY;
            }
            return TPAreaCode.NULL;
        }).orElse(TPAreaCode.NULL);
    }
}
