package com.cargill.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppAccountRequest {

    @NotBlank
    @Length(max = 50)
    private String type = "";

    @NotBlank
    @Length(max = 50)
    private String unicode = "";

    @NotBlank
    @Length(max = 50)
    private String userName = "";

    @NotBlank
    @Length(max = 50)
    private String phone = "";

    @NotBlank
    @Length(max = 50)
    private String canAct = "T";
}
