package com.cargill.dto.request;

import com.cargill.Constants;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ActiveUserRequest {

    @NotBlank
    @Length(max = 50)
    private String username;
    @NotBlank
    @Length(max = 50)
    private String password;
    @NotBlank
    @Length(max = 50)
    private String password2;
    @NotBlank
    @Length(max = Constants.DEFAULT_CODE_LENGTH)
    private String activeCode;
}
