package com.cargill.dto.request;

import java.util.Calendar;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.joda.time.DateTime;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReportRequest {

    @NotBlank
    @Length(max = 10)
    private String type = "";
    @NotBlank
    @Length(max = 2099, min = 2018)
    private Integer year = DateTime.now().getYear();
    @NotBlank
    @Length(max = 12, min = 1)
    private Integer month = DateTime.now().getMonthOfYear();
    @NotBlank
    private String orderType;
    private String tmcode;
    private String sscode;
    private String livestock;

    public String getFileByType() {
        return year + "_" + month + "_" + type;
    }

    public Calendar getStartDate() {
        return DateTime.now().withYear(year).withMonthOfYear(month).withDayOfMonth(1).millisOfDay().withMinimumValue().toGregorianCalendar();
    }

    public Calendar getEndDate() {
        return DateTime.now().withYear(year).withMonthOfYear(month).plusMonths(1).withDayOfMonth(1).minusDays(1).millisOfDay().withMaximumValue().toGregorianCalendar();
    }
}
