package com.cargill.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class QueryProductRequest {

    @Length(max = 50)
    private String code;

    @Length(max = 50)
    private String name;
}
