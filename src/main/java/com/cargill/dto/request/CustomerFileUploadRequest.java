package com.cargill.dto.request;

import com.cargill.service.AbsImportService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CustomerFileUploadRequest {

    @NotNull
    private MultipartFile file;
    @NotEmpty
    private String type;
    @NotEmpty
    private String mode;

    public AbsImportService.UPDATE_MODE getUpdateMode() {
        if ("2".equals(mode)) {
            return AbsImportService.UPDATE_MODE.DELETE_THEN_INSERT;
        }

        return AbsImportService.UPDATE_MODE.OVERWRITE_OR_APPEND;
    }

    public AbsImportService.IMPORT_TYPE getImportType() {
        if ("1".equals(type)) {
            return AbsImportService.IMPORT_TYPE.CUSTOMER_QUOTA;
        }else if("2".equals(type)){
            return AbsImportService.IMPORT_TYPE.CUSTOMER_FEEDINGSCALE;
        }
        return AbsImportService.IMPORT_TYPE.CUSTOMER_QUOTA;
    }
}
