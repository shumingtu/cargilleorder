package com.cargill.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TransAppAccountRequest {

    @NotBlank
    @Length(max = 50)
    private String transId;

    @NotBlank
    @Length(max = 50)
    private String transType;

    @NotBlank
    @Length(max = 50)
    private String transUnicode;

    @NotBlank
    @Length(max = 50)
    private String transUserName;


    @NotBlank
    @Length(max = 50)
    private String transPhone;

    @NotBlank
    @Length(max = 50)
    private String transCanAct;


}
