package com.cargill.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SearchCustomerOrderRequest {

    @NotBlank
    @Length(max = 20)
    private String orderNo = "";

    @NotBlank
    @Length(max = 8)
    private String customerId = "";

    @NotBlank
    @Length(max = 10)
    private String orderType = "";

    @NotBlank
    @Length(max = 20)
    private String customerName = "";

    @NotBlank
    @Length(max = 10)
    private String deliveryStartDate = "";

    @NotBlank
    @Length(max = 10)
    private String deliveryEndDate = "";

    @NotBlank
    @Length(max = 15)
    private String poCode = "";

    @NotBlank
    @Length(max = 5)
    private String tmCode = "";


    public boolean isBlank() {
        return orderNo.isEmpty() && customerId.isEmpty() &&
                orderType.isEmpty() && customerName.isEmpty() &&
                deliveryStartDate.isEmpty() && deliveryEndDate.isEmpty() &&
                poCode.isEmpty() && tmCode.isEmpty();
    }
}
