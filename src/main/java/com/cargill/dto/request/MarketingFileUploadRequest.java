package com.cargill.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MarketingFileUploadRequest {

    @NotEmpty
    private List<MultipartFile> files;
    @NotEmpty
    private List<String> doUploads;
    @NotEmpty
    private List<String> fileLinks;
    @NotEmpty
    private String carouselSec;
}
