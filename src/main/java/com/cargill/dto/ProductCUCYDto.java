package com.cargill.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductCUCYDto {

    private String code;
    private String drugCode;

    private String name;
    private String drugName;
    private BigDecimal amount = BigDecimal.ZERO;
}
