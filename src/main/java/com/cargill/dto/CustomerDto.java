package com.cargill.dto;

import com.cargill.model.CustomerFeedingscale;
import com.cargill.model.CustomerQuota;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CustomerDto {

    private String customerSubId;
    private String customerMainId;
    private String addressShipTo;
    private Boolean shipToSelected = false;
    private String customerSubName;
    private String customerMainName;
    private String phone;
    private String tmcode;
    private String address;

    private CustomerQuotaDto customerQuota;
    private CustomerFeedingscaleDto customerFeedingscale;

    // 20210628 增加memo欄位及功能
    private String memo;

}
