package com.cargill.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigInteger;
import java.util.Calendar;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WebAccountDto {

    private BigInteger id;
    private String account;
    private String userName;
    private String dept;
    private String actCode;
    private Calendar lastActCodeTime;
    private String status;
    private Calendar lastLoginTime;
    private String lastActCodeTimeDisplay;
    private String deptName;
}
