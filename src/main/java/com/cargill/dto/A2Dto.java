package com.cargill.dto;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.cargill.model.parameter.OrderTypeCode;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class A2Dto {

    private BigInteger id;
    private String orderNo;
    private Calendar createTime;
    private String customerSubId;
    private String customerSubName;
    private String customerPhone;
    private String shipTo;
    private String descWeb;
    private String descApp;
    private OrderTypeCode type;
    private String poCode;
    private String createTimeDisplay;
    private String customerQuota;

    private String orderPerson;
    private String tmcode;

    private List<A2RowDto> rowList = new ArrayList<>();
    
    private Calendar deliveryDate;
}
