package com.cargill.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductDto {

    private String code;
    private String name;
    private String phase;

    private BigDecimal priceX = BigDecimal.ZERO;
    private BigDecimal price3kg3K = BigDecimal.ZERO;
    private BigDecimal price10kg1 = BigDecimal.ZERO;
    private BigDecimal price20kg2 = BigDecimal.ZERO;
    private BigDecimal price25kg2point5C = BigDecimal.ZERO;
    private BigDecimal price30kg3 = BigDecimal.ZERO;
    private BigDecimal price30kg3C = BigDecimal.ZERO;
    private BigDecimal price50kg5 = BigDecimal.ZERO;
    private BigDecimal price1000kgJ = BigDecimal.ZERO;

    private BigDecimal price500kgJ5 = BigDecimal.ZERO;
}
