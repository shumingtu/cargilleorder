package com.cargill.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigInteger;
import java.util.Calendar;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppAccountDto {

    private BigInteger id;
    private String type;
    private String unicode;
    private String userName;
    private String email;
    private String phone;
    private String canAct;
    private String typeName;
    private String verifyCode;
    private String verifyResult;
    private Integer verifyCounts;
    private Integer verifySendCounts;
    private String verifyLasttimeDisplay;
    private String verifyCreatetimeDisplay;
}
