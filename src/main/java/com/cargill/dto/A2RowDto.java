package com.cargill.dto;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class A2RowDto {
    private String code = "";
    @JsonIgnore
    private String name = "";
    private List<A2CellDto> cellList = Arrays.asList(new A2CellDto[32]);
}
