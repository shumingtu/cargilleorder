package com.cargill.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DetailPdDto {
    private String id;
    private Boolean exist = false;
    private String createDate;
    private String note;
}
