package com.cargill.dto;

import com.cargill.model.parameter.TPAreaCode;
import com.cargill.model.parameter.TPAreaType;
import com.cargill.model.parameter.TPDrugType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductTPDrugDto {

    private String code;
    private String name;

    private TPAreaCode areaCode;
    private TPAreaType areaType;
    private TPDrugType drugType;
    private String recommendedAmount;
    private String maxAmount;
    private String drugWithdrawal;
    private String note;
}
