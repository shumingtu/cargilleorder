package com.cargill.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DetailDrugSpDto {
    private String code;
    private String name;
    private Double count1kg;
    private Double count2kg;
    private String recommend;
    private Double totalCount;
    private String days;
    private String note;

    // 飼料扣藥重的錢
    private BigDecimal price;
}
