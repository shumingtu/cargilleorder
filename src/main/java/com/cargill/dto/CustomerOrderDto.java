package com.cargill.dto;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import com.cargill.model.parameter.OrderTypeCode;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CustomerOrderDto {

    private BigInteger id;
    private String orderNo;
    private Calendar createTime;
    private String customerSubId;
    private String customerSubName;
    private String customerPhone;
    private String deliveryDateDisplay;
    private String deliveryTimeDisplay;
    private String shipTo;
    private String descWeb;
    private String descApp;
    private OrderTypeCode type;
    private String poCode;
    private String createTimeDisplay;

    private String orderPerson;
    private String tmcode;
    
    private Calendar modifyTime;

    private List<OrderDetailDto> orderDetailList = Collections.emptyList();
    private List<String> modifyLogList = Collections.emptyList();
}
