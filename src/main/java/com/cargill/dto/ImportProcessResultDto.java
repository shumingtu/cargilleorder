package com.cargill.dto;

import lombok.Data;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Data
public class ImportProcessResultDto {

    private String processFileName;
    private boolean processResult = false;
    private Long originalRowsCount = 0L;
    private Long successRowsCount = 0L;
    private Long failedRowsCount = 0L;
    private List<Long> failedRows = new ArrayList<>();

    public ImportProcessResultDto(String file_name) {
        this.processFileName = file_name;
    }

    public String getFailedRowsDetail() {
        if (failedRows.isEmpty()) {
            return "無";
        }
        return failedRows.stream().map(String::valueOf).collect(Collectors.joining(", "));
    }
}
