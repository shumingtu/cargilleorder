package com.cargill.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class F1Dto {

	private String tmcode;
	private String name;
}
