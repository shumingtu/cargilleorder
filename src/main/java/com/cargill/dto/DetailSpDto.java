package com.cargill.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collections;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DetailSpDto {
    private String id;
    private Boolean exist = false;
    private Boolean update = false;

    private String value;

    private String createDate;

    private List<String> symptom;

    private String deliveryDate;
    private String deliveryDateDisplay;
    private String deliveryTimeDisplay;
    private String productLineNote;
    private String productNote;
    private String productSpec;

    private String noteA;
    private String noteB;
    private String noteC;

    List<DetailDrugSpDto> drugCompanyList;
    List<DetailDrugSpDto> drugSelfList;

    private Integer totalPrice;
    private Double codeWeight;
    private Double spPrice;

    private List<String> modifyLogs = Collections.emptyList();
}
