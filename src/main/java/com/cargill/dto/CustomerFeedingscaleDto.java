package com.cargill.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CustomerFeedingscaleDto {
    
    private String customerId;
    private String customerName;
    private String feedingScale;
    private String size;
    private String note;
}
