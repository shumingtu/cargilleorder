package com.cargill.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderDetailDto {
    private String code;
    private String name;
    private String unit;
    private Double count;
    private String spec;
    private Double weight;
    private String phase;
    private Double price;
    private String vehicleType;
    private String vehicleName;

    private DetailSpDto detailSP;
    private String jsonDetailSP;
    private DetailPdDto detailPD;
    private String jsonDetailPD;
}
