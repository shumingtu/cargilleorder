package com.cargill.dto;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;

import com.cargill.util.DateUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NotificationCustomerDto {

	private BigInteger id;
	private Calendar createTime;
	private Calendar modifyTime;
	private Date notificationDate;
	private String tmcode;
	private String customerSubName;
	private String customerSubId;
	private String message;
	private String owner;
	private boolean finished;
	private boolean cancelled;
	
	@Override
	@JsonIgnore
	public String toString() {
		String date = DateUtil.DashDateFormatter.format(DateUtil.convertToLocalDate(notificationDate));
		return "NotificationCustomerDto [notificationDate=" + date + ", tmcode=" + tmcode
				+ ", customerSubName=" + customerSubName + ", customerSubId=" + customerSubId + ", message="
				+ message + ", owner=" + owner + ", finished=" + finished + ", cancelled=" + cancelled + "]";
	}

}
