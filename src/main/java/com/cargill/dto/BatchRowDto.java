package com.cargill.dto;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BatchRowDto {
    private String code = "";
    @JsonIgnore
    private String name = "";
    private List<Calendar> cellList = new ArrayList<>();
}
