package com.cargill.dto;

import com.cargill.model.parameter.TPAreaCode;
import com.cargill.model.parameter.TPAreaType;
import com.cargill.model.parameter.TPDrugType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ViewProductTPDrugDto {

    private String code;
    private String name;

    private BigDecimal price;

    private TPAreaCode areaCode;
    private TPAreaType areaType;
    private TPDrugType drugType;
    private String recommendedAmount;
    private String maxAmount;
    private String drugWithdrawal;
    private String note;

    private String tpCode;
    private String tpName;
}
